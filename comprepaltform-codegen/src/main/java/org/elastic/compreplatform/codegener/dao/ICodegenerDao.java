package org.elastic.compreplatform.codegener.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.elastic.compreplatform.codegener.model.SysConfig;
import org.elastic.compreplatform.common.dao.BaseDao;

/**
 * ClassName: ICodegenerDao 
 * @Description: 数据库配置操作接口定义
 * @author JornTang
 * @date 2018年2月2日
 */
public interface ICodegenerDao extends BaseDao<SysConfig>{
	/**
	 * @Description: saveOrUpdateDbconf
	 * @param SysConfig
	 * @return   
	 * @return int  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月2日
	 */
	int saveDbconf(SysConfig sysConfig);
	/**
	 * @Description: 查询dbconf集合
	 * @param SysConfig
	 * @param page
	 * @return   
	 * @return List<SysConfig>  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月2日
	 */
	List<SysConfig> searchDbconflist(@Param("sysConfig")SysConfig SysConfig, @Param("sortColumns")String sortColumns);
	/**
	 * @Description: 删除数据库配置
	 * @param SysConfig
	 * @return   
	 * @return int  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月3日
	 */
	int deleteDbconf(SysConfig sysConfig);
	/**
	 * @Description: 根据ID查询数据库配置
	 * @param id
	 * @return   
	 * @return SysConfig  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月4日
	 */
	SysConfig searchDbconfById(@Param("sysConfig")SysConfig sysConfig);
	/**
	 * @Description: 修改数据库配置
	 * @param SysConfig
	 * @return   
	 * @return int  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月4日
	 */
	int updateDbconf(SysConfig sysConfig);
	/**
	 * @Description: 获取所有的db配置
	 * @return   
	 * @return List<SysConfig>  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月6日
	 */
	List<SysConfig> searchAllDbconf();
	
//	int deleteById(java.lang.Long id);
//
//    int insert(SysConfig sysConfig);
//
//    int insertSelective(SysConfig sysConfig);
//
//    SysConfig selectById(java.lang.Long id);
//
//    int updateSelective(SysConfig sysConfig);
//
//    int update(SysConfig sysConfig);
	

}
