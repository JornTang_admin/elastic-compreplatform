package org.elastic.compreplatform.codegener.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.elastic.compreplatform.codegener.model.SysConfig;
import org.elastic.compreplatform.common.model.PageHelper;
import org.elastic.compreplatform.common.model.RespMsg;

/**
 * ClassName: ICodegenerService 
 * @Description: 代码生成服务接口定义
 * @author JornTang
 * @date 2018年2月2日
 */
public interface ICodegenerService {
	/**
	 * @Description: 添加或修改db配置
	 * @param request
	 * @param SysConfig
	 * @return   
	 * @return RespMsg  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月2日
	 */
	RespMsg saveOrUpdateDbconf(HttpServletRequest request, SysConfig sysConfig);
	/**
	 * @Description: 查询dbconf集合
	 * @param SysConfig
	 * @param page
	 * @return   
	 * @return List<SysConfig>  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月2日
	 */
	List<SysConfig> searchDbconflist(SysConfig SysConfig, PageHelper page);
	/**
	 * @Description: 删除数据库配置
	 * @param SysConfig
	 * @return   
	 * @return RespMsg  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月3日
	 */
	RespMsg deleteDbconf(SysConfig sysConfig);
	/**
	 * @Description: 根据ID查询数据库配置
	 * @param id
	 * @return   
	 * @return SysConfig  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月4日
	 */
	SysConfig searchDbconfById(SysConfig sysConfig);
	/**
	 * @Description: 获取所有的db配置
	 * @return   
	 * @return List<SysConfig>  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月6日
	 */
	List<SysConfig> searchAllDbconf();

}
