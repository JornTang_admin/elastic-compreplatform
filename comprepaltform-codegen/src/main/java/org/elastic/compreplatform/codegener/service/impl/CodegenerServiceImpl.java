package org.elastic.compreplatform.codegener.service.impl;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.elastic.compreplatform.codegener.dao.ICodegenerDao;
import org.elastic.compreplatform.codegener.model.SysConfig;
import org.elastic.compreplatform.codegener.service.ICodegenerService;
import org.elastic.compreplatform.common.constant.RespEnum;
import org.elastic.compreplatform.common.model.RespMsg;
import org.elastic.compreplatform.common.util.RespMsgUtil;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;

/**
 * ClassName: CodegenerService 
 * @Description: 代码生成服务实现类
 * @author JornTang
 * @date 2018年2月2日
 */
@Service
public class CodegenerServiceImpl implements ICodegenerService{
	@Resource
	private ICodegenerDao codegenerDao;
	/**
	 * @Description: 添加或修改db配置
	 * @param request
	 * @param SysConfig
	 * @return   
	 * @return RespMsg  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月2日
	 */
	@Override
	public RespMsg saveOrUpdateDbconf(HttpServletRequest request,
			SysConfig sysConfig) {
		int result = 0;
		SysConfig config = codegenerDao.searchDbconfById(sysConfig);
		if(config!= null){
			result = codegenerDao.updateDbconf(sysConfig);
		}else{
			result = codegenerDao.saveDbconf(sysConfig);
		}
		return result>0?RespMsgUtil.returnMsg(RespEnum.SUCCESS, null): RespMsgUtil.returnMsg(RespEnum.ERROR, null);
	}
	/**
	 * @Description: 查询dbconf集合
	 * @param SysConfig
	 * @param page
	 * @return   
	 * @return List<SysConfig>  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月2日
	 */
	@Override
	public List<SysConfig> searchDbconflist(SysConfig SysConfig, org.elastic.compreplatform.common.model.PageHelper page) {
		PageHelper.startPage(page.getPage(), page.getLimit());
		List<SysConfig> list = codegenerDao.searchDbconflist(SysConfig, page.getSortColumns());
		return list;
	}
	/**
	 * @Description: 删除数据库配置
	 * @param SysConfig
	 * @return   
	 * @return RespMsg  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月3日
	 */
	@Override
	public RespMsg deleteDbconf(SysConfig sysConfig) {
		int result = codegenerDao.deleteDbconf(sysConfig);
		return result>0?RespMsgUtil.returnMsg(RespEnum.SUCCESS): RespMsgUtil.returnMsg(RespEnum.ERROR);
	}
	/**
	 * @Description: 根据ID查询数据库配置
	 * @param id
	 * @return   
	 * @return SysConfig  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月4日
	 */
	@Override
	public SysConfig searchDbconfById(SysConfig sysConfig) {
		return codegenerDao.searchDbconfById(sysConfig);
	}
	/**
	 * @Description: 获取所有的db配置
	 * @return   
	 * @return List<SysConfig>  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月6日
	 */
	@Override
	public List<SysConfig> searchAllDbconf() {
		return codegenerDao.searchAllDbconf();
	}
	
}
