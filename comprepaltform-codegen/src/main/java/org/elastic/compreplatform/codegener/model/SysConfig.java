package org.elastic.compreplatform.codegener.model;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

/**
 * ClassName: 
 * @Description: 
 * @author JornTang
 * @date 
 */


public class SysConfig implements java.io.Serializable{
	private static final long serialVersionUID = 5454155825314635342L;
	
	//alias
	public static final String TABLE_ALIAS = "DbConfig";
	public static final String ALIAS_ID = "主键id";
	public static final String ALIAS_DESCRIBE = "描述";
	public static final String ALIAS_DB_TYPE = "数据库类型";
	public static final String ALIAS_DB_URL = "ip地址";
	public static final String ALIAS_DB_DRIVER = "数据库驱动";
	public static final String ALIAS_SCHEMA = "oracle需要配置scheme";
	public static final String ALIAS_USERNAME = "用户名";
	public static final String ALIAS_PASSWORD = "密码";
	
	//date formats
	public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	
	//Can be used directly: @Length (max=50, message= "username length can not be greater than 50") display error messages
	//columns START
    /**
     * 主键id       db_column: id 
     */	
	
	private Long id;
    /**
     * 描述       db_column: describe 
     */	
	@NotBlank @Length(max=50)
	private String dbDesc;
    /**
     * 数据库类型       db_column: db_type 
     */	
	@NotBlank @Length(max=50)
	private String dbType;
    /**
     * ip地址       db_column: db_url 
     */	
	@NotBlank @Length(max=50)
	private String dbUrl;
	/**
     * 数据库驱动       db_column: db_driver 
     */	
	@NotBlank @Length(max=100)
	private String dbDriver;
    /**
     * oracle需要配置scheme       db_column: schema 
     */	
	@Length(max=50)
	private String dbSchema;
    /**
     * 用户名       db_column: username 
     */	
	@NotBlank @Length(max=50)
	private String username;
    /**
     * 密码       db_column: password 
     */	
	@NotBlank @Length(max=50)
	private String password;
	
	private String tableName;
	//columns END
	
	public SysConfig(){
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public SysConfig(Long id){
		this.id = id;
	}
	
	public String getDbDriver() {
		return dbDriver;
	}

	public void setDbDriver(String dbDriver) {
		this.dbDriver = dbDriver;
	}

	public String getDbDesc() {
		return dbDesc;
	}

	public void setDbDesc(String dbDesc) {
		this.dbDesc = dbDesc;
	}

	public String getDbSchema() {
		return dbSchema;
	}

	public void setDbSchema(String dbSchema) {
		this.dbSchema = dbSchema;
	}

	public void setId(Long value) {
		this.id = value;
	}
	
	public Long getId() {
		return this.id;
	}
	public void setDbType(String value) {
		this.dbType = value;
	}
	
	public String getDbType() {
		return this.dbType;
	}
	public void setDbUrl(String value) {
		this.dbUrl = value;
	}
	
	public String getDbUrl() {
		return this.dbUrl;
	}
	public void setUsername(String value) {
		this.username = value;
	}
	
	public String getUsername() {
		return this.username;
	}
	public void setPassword(String value) {
		this.password = value;
	}
	
	public String getPassword() {
		return this.password;
	}

	public String toString() {
		return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
			.append("Id",getId())
			.append("Describe",getDbDesc())
			.append("DbType",getDbType())
			.append("DbUrl",getDbUrl())
			.append("DbDriver",getDbDriver())
			.append("Schema",getDbSchema())
			.append("Username",getUsername())
			.append("Password",getPassword())
			.toString();
	}
	
	public int hashCode() {
		return new HashCodeBuilder()
			.append(getId())
			.toHashCode();
	}
	
	public boolean equals(Object obj) {
		if(obj instanceof SysConfig == false) return false;
		if(this == obj) return true;
		SysConfig other = (SysConfig)obj;
		return new EqualsBuilder()
			.append(getId(),other.getId())
			.isEquals();
	}
}

