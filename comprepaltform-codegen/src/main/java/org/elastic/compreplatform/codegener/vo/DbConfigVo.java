package org.elastic.compreplatform.codegener.vo;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * ClassName: DbConfigVo 
 * @Description: 数据库配置VO
 * @author JornTang
 * @date 2018年2月2日
 */
public class DbConfigVo implements Serializable {
    private static final long serialVersionUID = 3148176768559230877L;
    

	/** 主键id */
	private java.lang.Long id;
	/** 描述 */
	private java.lang.String describe;
	/** 数据库类型 */
	private java.lang.String dbType;
	/** ip地址 */
	private java.lang.String dbUrl;
	/** 端口 */
	private java.lang.Integer dbPort;
	/** 数据库实例 */
	private java.lang.String instance;
	/** oracle需要配置scheme */
	private java.lang.String schema;
	/** 用户名 */
	private java.lang.String username;
	/** 密码 */
	private java.lang.String password;

	public java.lang.Long getId() {
		return this.id;
	}
	
	public void setId(java.lang.Long value) {
		this.id = value;
	}
	
	public java.lang.String getDescribe() {
		return this.describe;
	}
	
	public void setDescribe(java.lang.String value) {
		this.describe = value;
	}
	
	public java.lang.String getDbType() {
		return this.dbType;
	}
	
	public void setDbType(java.lang.String value) {
		this.dbType = value;
	}
	
	public java.lang.String getDbUrl() {
		return this.dbUrl;
	}
	
	public void setDbUrl(java.lang.String value) {
		this.dbUrl = value;
	}
	
	public java.lang.Integer getDbPort() {
		return this.dbPort;
	}
	
	public void setDbPort(java.lang.Integer value) {
		this.dbPort = value;
	}
	
	public java.lang.String getInstance() {
		return this.instance;
	}
	
	public void setInstance(java.lang.String value) {
		this.instance = value;
	}
	
	public java.lang.String getSchema() {
		return this.schema;
	}
	
	public void setSchema(java.lang.String value) {
		this.schema = value;
	}
	
	public java.lang.String getUsername() {
		return this.username;
	}
	
	public void setUsername(java.lang.String value) {
		this.username = value;
	}
	
	public java.lang.String getPassword() {
		return this.password;
	}
	
	public void setPassword(java.lang.String value) {
		this.password = value;
	}
	

	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.MULTI_LINE_STYLE);
	}
	
}

