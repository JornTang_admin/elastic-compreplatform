<#include "/macro.include"/>
<#include "/custom.include"/>  
<#assign className = table.className>   
<#assign classNameLower = className?uncap_first> 
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/commons/taglibs.jsp" %>


<codegen:override name="head">
	<title><%=${className}.TABLE_ALIAS%>编辑</title>
</codegen:override>

<codegen:override name="content">
	<form action="<@jspEl 'ctx'/>${actionBasePath}/update.do" method="post">
		<div style="width:100px;white-space:nowrap;" ><input id="submitButton" name="submitButton" type="submit" value="提交" />
		<input type="button" value="返回列表" onclick="window.location='<@jspEl 'ctx'/>${actionBasePath}/list.do'"/>
		<input type="button" value="后退" onclick="history.back();"/></div>
		
		<table class="formTable">
		<%@ include file="form_include.jsp" %>
		</table>
	</form>
	
	<script>
		
	$.validator.setDefaults({
		submitHandler: function(form) {  form.submit();}
	});

	$().ready(function() {
		// validate the comment form when it is submitted
		$(document.forms[0]).validate();
	});
	</script>
</codegen:override>

<%-- jsp模板继承, jsp_extends --%>
<%@ include file="base.jsp" %>