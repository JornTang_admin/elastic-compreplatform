<#assign className = table.className>   
<#assign classNameLower = className?uncap_first>   
package ${basepackage}.dao;

<#include "/java_imports.include">

public interface I${className}Dao {
	
	int deleteById(${table.idColumn.javaType} ${table.idColumn});

    int insert(${className} ${classNameLower});
    
    Page<${className}> pageList(${className}Query query);

    int insertSelective(${className} ${classNameLower});

    ${className} selectById(${table.idColumn.javaType} ${table.idColumn});

    int updateSelective(${className} ${className});

    int update(${className} ${classNameLower});
	
	<#list table.columns as column>
	<#if column.unique && !column.pk>
	public ${className} getBy${column.columnName}(${column.javaType} v) {
		return (${className})getSqlSessionTemplate().selectOne("${className}.getBy${column.columnName}",v);
	}	
	
	</#if>
	</#list>

}
