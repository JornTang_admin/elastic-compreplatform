<#include "/custom.include">
<#include "/java_copyright.include">
<#assign className = table.className>   
<#assign classNameLower = className?uncap_first>   
package ${basepackage}.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.org.codegenframework.web.scope.Flash;

import org.elastic.compreplatform.admin.manager.base.controller.BaseController;
import org.elastic.compreplatform.admin.manager.codegener.model.DbConfig;
import org.elastic.compreplatform.admin.util.PageHelperUtil;
import org.elastic.compreplatform.admin.manager.base.model.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import com.github.pagehelper.Page;

import cn.org.codegenframework.beanutils.BeanUtils;

<#include "/java_imports.include">

@Controller
@RequestMapping("/"+${classNameLower})
public class ${className}Controller extends BaseSpringController{
	private static Logger log = LoggerFactory.getLogger(${className}Controller.class); 
	/**
	 * 注解注入
	 */
	@Resource
	private ${className}Service ${classNameLower}Service;
	
	public ${className}Controller() {
	}
	
	/** 
	 * 分页查询
	 **/
	@RequestMapping("/pagelist")
	public PageHelper pagelist(HttpServletRequest request, HttpServletResponse response, ${className}Query query, PageHelper page) {
		Page<${className}> list = (Page<${className}>) ${classNameLower}Service.pagelist(query, page);
        return PageHelperUtil.doHandlePage(list);
	}
	
	/** 
	 * 查看对象
	 **/
	public ModelAndView view(HttpServletRequest request,HttpServletResponse response) throws Exception {
		<@generateIdParameter/>
		${className} ${classNameLower} = (${className})${classNameLower}Manager.getById(id);
		return new ModelAndView("/${className}/show","${classNameLower}",${classNameLower});
	}
	
	/** 
	 * 进入新增页面
	 **/
	public ModelAndView create(HttpServletRequest request,HttpServletResponse response,${className} ${classNameLower}) throws Exception {
		return new ModelAndView("/${className}/create","${classNameLower}",${classNameLower});
	}
	
	/** 
	 * 保存新增对象
	 **/
	public ModelAndView save(HttpServletRequest request,HttpServletResponse response,${className} ${classNameLower}) throws Exception {
		${classNameLower}Manager.save(${classNameLower});
		Flash.current().success(CREATED_SUCCESS); //存放在Flash中的数据,在下一次http请求中仍然可以读取数据,error()用于显示错误消息
		return new ModelAndView(LIST_ACTION);
	}
	
	/**
	 * 进入更新页面
	 **/
	public ModelAndView edit(HttpServletRequest request,HttpServletResponse response) throws Exception {
		<@generateIdParameter/>
		${className} ${classNameLower} = (${className})${classNameLower}Manager.getById(id);
		return new ModelAndView("/${className}/edit","${classNameLower}",${classNameLower});
	}
	
	/**
	 * 保存更新对象
	 **/
	public ModelAndView update(HttpServletRequest request,HttpServletResponse response) throws Exception {
		<@generateIdParameter/>
		
		${className} ${classNameLower} = (${className})${classNameLower}Manager.getById(id);
		bind(request,${classNameLower});
		${classNameLower}Manager.update(${classNameLower});
		Flash.current().success(UPDATE_SUCCESS);
		return new ModelAndView(LIST_ACTION);
	}
	
	/**
	 *删除对象
	 **/
	public ModelAndView delete(HttpServletRequest request,HttpServletResponse response) {
		String[] items = request.getParameterValues("items");
		for(int i = 0; i < items.length; i++) {
			Hashtable params = HttpUtils.parseQueryString(items[i]);
			
			<#if table.compositeId>
			${className}Id id = (${className}Id)copyProperties(${className}Id.class,params);
			<#else>
				<#list table.compositeIdColumns as column>
			${column.javaType} id = new ${column.javaType}((String)params.get("${column.columnNameLower}"));
				</#list>
			</#if>
			
			${classNameLower}Manager.removeById(id);
		}
		Flash.current().success(DELETE_SUCCESS);
		return new ModelAndView(LIST_ACTION);
	}
	
}

<#macro generateIdParameter>
	<#if table.compositeId>
		${className}Id id = new ${className}Id();
		bind(request, id);
	<#else>
		<#list table.compositeIdColumns as column>
		${column.javaType} id = new ${column.javaType}(request.getParameter("${column.columnNameLower}"));
		</#list>
	</#if>
</#macro>