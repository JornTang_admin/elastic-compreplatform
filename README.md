
	# 前言 #
	本项目是本人在空闲时间写的，其最主要目的：
	1、学习新技术
	2、回馈开源社区
	3、公司项目技术升级
	由于是开源各位老司机注意不得以商业为目的。欢迎大家加QQ群：708970773学习交流

	# 特别感谢以下开源项目： #
	1)许雪里 分布式任务调度平台XXL-JOB http://www.xuxueli.com/xxl-job/
	2)baomidou/mybatis-plus https://gitee.com/baomidou/mybatis-plus
	3)abel533 / Mybatis_PageHelper  https://gitee.com/free/Mybatis_PageHelper
	4)Looly / hutool https://gitee.com/loolly/hutool
	5)闲心/layui http://www.layui.com/

	# 项目介绍 #
	基于Spring + SpringMVC+Mybatis技术架构，并集成了mybatis-plus,mybatis-pageHelper。简化CURD操作。
	核心模块：公共服务、配置中心、分布式任务调度、Redis管理、连接池监控（Druid）、代码生成工具（敏捷开发）、
	爬虫管理（成功爬取www.ip181.com、www.xicidaili.com、全球最大成人网站www.xvideos.com几千部视频 ps：老司机谨慎开车）
![](https://gitee.com/uploads/images/2018/0323/090826_5e49c03a_1445072.png "项目结构")

	# 项目特点 #
	1、开箱即用。详细注释，方便学习
	2、Redis在线管理，实时监控（非单机）
	3、可自定义模板生成前后端代码。省去95%单表操作代码
	4、分布式任务管理、配置
	5、爬虫学习，有成功爬取3个网站实例
	6、分布式ID高效产生，可以使用hutool => Snowflake相关API

	技术选型
	后端技术
	技术项	            名称	                      官网连接
	SpringFrameWork	    容器框架	http://projects.spring.io/spring-framework/
	SpringMVC	        MVC框架	http://docs.spring.io/spring/docs/current/spring-framework-reference/htmlsingle/#mvc
	FreeMarker	        模板引擎	https://freemarker.apache.org/
	Apache Shiro	    安全控制框架	http://shiro.apache.org/
	MyBatis	            ORM持久层框架	http://www.mybatis.org/mybatis-3/zh/index.html
	PageHelper	        Mybatis分页控件	http://git.oschina.net/free/Mybatis_PageHelper
	Mybatis-plus	    mybatis 增强工具包https://gitee.com/baomidou/mybatis-plus
	Druid	            数据库连接池	https://github.com/alibaba/druid
	ZooKeeper	        分布式协调服务	http://zookeeper.apache.org/
	Redis	            Key-value内存缓存数据库	https://redis.io/
	Elasticsearch5.6.4	搜索引擎，实时分析技术	https://www.elastic.co/guide/en/elasticsearch/reference/5.6/index.html
	Quartz	            任务调度框架	http://www.quartz-scheduler.org/
	Maven	            项目构建、管理工具	http://maven.apache.org/
	hutool	            Java工具集,分布式ID生成	https://gitee.com/loolly/hutool
	slf4j、log4j12	    日志框架	https://www.slf4j.org/、http://logging.apache.org/log4j/2.x/download.html
	jsoup	            HTML解析器	https://jsoup.org/
	htmlunit	        页面分析工具	http://htmlunit.sourceforge.net/
	fastjson	        JSON 解析器、生成器 https://gitee.com/wenshao/fastjson
	XXL-JOB	            分布式调度框架	https://gitee.com/xuxueli0323/xxl-job
	XXL-conf	        分布式配置中心	https://gitee.com/xuxueli0323/xxl-conf
	
	
	前端技术
	技术项	            名称	                    官网连接
	Jquery	            JavaScript框架	https://jquery.com/
	Bootstrap	        前端框架	https://getbootstrap.com/
	layui	            前端UI框架	http://www.layui.com/
	echarts	            报表控件、图表库	http://echarts.baidu.com/
	particles	        粒子（画布）的轻量级JavaScript库	https://codepen.io/#
	Jsp	                Java服务器页面	http://www.jsp.com/
	Css	                层叠样式表	https://www.w3.org/
	
	快速开始
	环境搭建
	1、Jdk8 + eclipse
	2、Maven3x
	3、Mysql5.5+
	4、Redis
	5、Zookeeper
	6、Elasticsearch5.6.4 + kibana5.6.4 + elasticsearch-sql
	7、Tomcat8
	
	资源下载连接
	Jdk8 http://www.oracle.com/technetwork/java/javase/downloads
	Mysql5.5 https://dev.mysql.com/downloads/mysql/
	Redis https://redis.io/download
	Zookeeper http://zookeeper.apache.org/releases.html#download
	Elasticsearch5.6.4 https://www.elastic.co/downloads/past-releases
	Elasticsearch-sql https://github.com/NLPchina/elasticsearch-sql
	kibana5.6.4 https://www.elastic.co/downloads/past-releases
	Tomcat8 https://tomcat.apache.org/download-80.cgi
	
	PS：如果不使用compreplatform-crawler模块无须安装Elasticsearch5.6.4 + kibana5.6.4 + elasticsearch-sql
	
	集成指南
	第一步：在mysql中创建elastic_compre库。找到comprepaltform-parent/doc/db/
	elastic-compre.sql执行此数据库脚本.然后在comprepaltform-admin 找到jdbc.properties配置数据库
	
	第二步：使用zkcli.cmd 创建zookeeper node. 操作命令：create /elastic_compre elastic_compre_data。并启动zookeeper
	
	第三步：在compreplatform-redis子模块中找到redis.properties,配置redis地址与端口。并启动redis server
	
	第四步：编译compreplatform-admin web模块并发布至tomcat 。访问地址http://host:port/compreplatform-admin/
	 
	登录用户名与密码请加Q群：708970773获取
	
	体验过程中有什么好的建议欢迎提 issues
	
	项目效果图
![登录页面](https://gitee.com/uploads/images/2018/0323/091645_16a1cabf_1445072.png "登录页面.png")
![项目登录首页](https://gitee.com/uploads/images/2018/0323/091323_d3924cff_1445072.png "项目登录首页.png")
![基本操作CURD](https://gitee.com/uploads/images/2018/0323/091709_2396ac22_1445072.png "基本操作CURD.png")
![监控](https://gitee.com/uploads/images/2018/0323/091735_c78bcf5c_1445072.png "监控.png")
	
	特别警告
	本开源项目不得以非法盈利为目的，如果触碰法律，一切后果自己承担。切记！切记！切记！重要的事情说3遍。