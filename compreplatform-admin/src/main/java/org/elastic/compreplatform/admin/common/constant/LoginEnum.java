package org.elastic.compreplatform.admin.common.constant;

/**
 * ClassName: LoginEnum 
 * @Description: 登录相关常量定义
 * @author JornTang
 * @date 2018年1月29日
 */
public enum LoginEnum {
	// 登录验证码标识
	CAPTCHA_CODE("CAPTCHA_CODE","登录验证码标识"),
	// 登录session key
	LOGIN_SESSION_KEY("LOGIN_SESSION_KEY" ,"登录session key");
	
	LoginEnum(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}
	private String code;

	private String msg;
	

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
}
