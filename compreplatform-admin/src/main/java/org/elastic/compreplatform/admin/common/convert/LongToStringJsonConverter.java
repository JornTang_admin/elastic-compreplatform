package org.elastic.compreplatform.admin.common.convert;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
/**
 * ClassName: LongToStringJsonConverter 
 * @Description: Long转字符串（避免序列成json时在页面丢失精度）
 * @author JornTang
 * @date 2018年2月3日
 */
public class LongToStringJsonConverter extends ObjectMapper {

    /**
	 * 
	 */
	private static final long serialVersionUID = -2118573271661706063L;

	public LongToStringJsonConverter() {
        super();
        SimpleModule simpleModule = new SimpleModule();
        simpleModule.addSerializer(Long.class, ToStringSerializer.instance);
        simpleModule.addSerializer(Long.TYPE, ToStringSerializer.instance);
        registerModule(simpleModule);
    }
}