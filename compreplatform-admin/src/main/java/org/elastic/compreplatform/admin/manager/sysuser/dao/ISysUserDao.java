package org.elastic.compreplatform.admin.manager.sysuser.dao;

import org.apache.ibatis.annotations.Param;
import org.elastic.compreplatform.admin.manager.sysuser.model.SysUser;
import org.elastic.compreplatform.common.dao.BaseDao;

/**
 * ClassName: ISysUserDao 
 * @Description: 用户持久化操作接口定义
 * @author JornTang
 * @date 2018年1月28日
 */
public interface ISysUserDao extends BaseDao<SysUser>{
	/**
	 * @Description: 用户登录查询验证
	 * @param username
	 * @param pswd
	 * @return   
	 * @return SysUser  
	 * @throws
	 * @author JornTang
	 * @date 2018年1月28日
	 */
	SysUser login(@Param("email")String username, @Param("pswd")String pswd);
	/**
	 * @Description: 更新用户登录时间
	 * @param user   
	 * @return 
	 * @return void  
	 * @throws
	 * @author JornTang
	 * @date 2018年1月28日
	 */
	 void updateByPrimaryKeys(SysUser user);

}
