package org.elastic.compreplatform.admin.manager.sysuser.service.impl;

import java.util.Set;

import javax.annotation.Resource;

import org.elastic.compreplatform.admin.manager.sysuser.dao.IPermissionDao;
import org.elastic.compreplatform.admin.manager.sysuser.service.IPermissionService;
import org.springframework.stereotype.Service;

/**
 * ClassName: IPermissionService 
 * @Description: 用户角色权限操作接口定义
 * @author JornTang
 * @date 2018年1月28日
 */
@Service("permissionService")
public class PermissionServiceImpl implements IPermissionService {
	@Resource
	private IPermissionDao permissionDao;
	
	/**
	 * @Description: 根据用户ID查询权限
	 * @param userId
	 * @return   
	 * @return Set<String>  
	 * @throws
	 * @author JornTang
	 * @date 2018年1月28日
	 */
	@Override
	public Set<String> findPermissionByUserId(Long userId) {
		return permissionDao.findPermissionByUserId(userId);
	}
}
