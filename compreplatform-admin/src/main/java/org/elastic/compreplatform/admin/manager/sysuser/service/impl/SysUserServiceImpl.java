package org.elastic.compreplatform.admin.manager.sysuser.service.impl;

import javax.annotation.Resource;

import org.elastic.compreplatform.admin.manager.sysuser.dao.ISysUserDao;
import org.elastic.compreplatform.admin.manager.sysuser.model.SysUser;
import org.elastic.compreplatform.admin.manager.sysuser.service.ISysUserService;
import org.elastic.compreplatform.common.service.impl.BaseServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * ClassName: ISysUserService 
 * @Description: 用户操作接口实现类
 * @author JornTang
 * @date 2018年1月28日
 */
@Service("sysUserService")
public class SysUserServiceImpl extends BaseServiceImpl<SysUser> implements ISysUserService {
	private static Logger log = LoggerFactory.getLogger(SysUserServiceImpl.class);
	@Resource
	private ISysUserDao sysUserDao;
	/**
	 * @Description: 用户登录查询验证
	 * @param username
	 * @param pswd
	 * @return   
	 * @return SysUser  
	 * @throws
	 * @author JornTang
	 * @date 2018年1月28日
	 */
	@Override
	public SysUser login(String username, String pswd) {
		return sysUserDao.login(username, pswd);
	}
	/**
	 * @Description: 更新用户登录时间
	 * @param user   
	 * @return void  
	 * @throws
	 * @author JornTang
	 * @date 2018年1月28日
	 */
	@Override
	public void updateByPrimaryKeySelective(SysUser user) {
		sysUserDao.updateByPrimaryKeys(user);
	}
}