package org.elastic.compreplatform.admin.core.shiro.cache;

import org.apache.shiro.cache.Cache;

/**
 * ClassName: ShiroCacheManager 
 * @Description: shiro cache manager 接口
 * @author JornTang
 * @date 2018年1月27日
 */
public interface ShiroCacheManager {

    <K, V> Cache<K, V> getCache(String name);

    void destroy();

}
