package org.elastic.compreplatform.admin.core.shiro.filter;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;

/**
 * ClassName: ShiroFilterUtils 
 * @Description: Shiro Filter 工具类
 * @author JornTang
 * @date 2018年1月30日
 */
public class ShiroFilterUtils {
	private static Logger log = LoggerFactory.getLogger(ShiroFilterUtils.class);
	//登录页面
	static final String LOGIN_URL = "/login.jsp";
	//踢出登录提示
	final static String KICKED_OUT = "/user/toKickout.do";
	//未知授权提示
	final static String UNAUTHORIZED = "/user/toUnauthorized.do";
	/**
	 * 是否是Ajax请求
	 * @param request
	 * @return
	 */
	public static boolean isAjax(ServletRequest request){
		return "XMLHttpRequest".equalsIgnoreCase(((HttpServletRequest) request).getHeader("X-Requested-With"));
	}
	
	/**
	 * response 输出JSON
	 * @param hresponse
	 * @param resultMap
	 * @throws IOException
	 */
	public static void out(ServletResponse response, Map<String, String> resultMap){
		
		PrintWriter out = null;
		try {
			response.setCharacterEncoding("UTF-8");
			out = response.getWriter();
			out.println(JSON.toJSONString(resultMap));
		} catch (Exception e) {
			log.error("输出JSON报错。", e);
		}finally{
			if(null != out){
				out.flush();
				out.close();
			}
		}
	}
}
