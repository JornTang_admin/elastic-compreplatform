package org.elastic.compreplatform.admin.manager.sysuser.service;

import org.elastic.compreplatform.admin.manager.sysuser.model.SysUser;
import org.elastic.compreplatform.common.service.IBaseService;

/**
 * ClassName: ISysUserService 
 * @Description: 用户操作接口定义
 * @author JornTang
 * @date 2018年1月28日
 */
public interface ISysUserService extends IBaseService<SysUser> {
	/**
	 * @Description: 用户登录查询验证
	 * @param username
	 * @param pswd
	 * @return   
	 * @return SysUser  
	 * @throws
	 * @author JornTang
	 * @date 2018年1月28日
	 */
	SysUser login(String username, String pswd);
	/**
	 * @Description: 更新用户登录时间
	 * @param user   
	 * @return void  
	 * @throws
	 * @author JornTang
	 * @date 2018年1月28日
	 */
	void updateByPrimaryKeySelective(SysUser user);
}