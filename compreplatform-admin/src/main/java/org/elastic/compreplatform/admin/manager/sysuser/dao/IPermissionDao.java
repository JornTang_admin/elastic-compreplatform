package org.elastic.compreplatform.admin.manager.sysuser.dao;

import java.util.Set;

import org.apache.ibatis.annotations.Param;
import org.elastic.compreplatform.admin.manager.sysuser.model.Permission;
import org.elastic.compreplatform.common.dao.BaseDao;

/**
 * ClassName: IPermissionDao 
 * @Description: 用户角色权限操作接口定义
 * @author JornTang
 * @date 2018年1月28日
 */
public interface IPermissionDao extends BaseDao<Permission>{
	/**
	 * @Description: 根据用户ID查询权限
	 * @param userId
	 * @return   
	 * @return Set<String>  
	 * @throws
	 * @author JornTang
	 * @date 2018年1月28日
	 */
	Set<String> findPermissionByUserId(@Param("userId")Long userId);

}
