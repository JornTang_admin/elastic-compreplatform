package org.elastic.compreplatform.admin.core.shiro.cache.impl;

import org.apache.shiro.cache.Cache;
import org.elastic.compreplatform.admin.core.shiro.cache.JedisShiroCache;
import org.elastic.compreplatform.admin.core.shiro.cache.ShiroCacheManager;

/**
 * ClassName: JedisShiroCacheManager 
 * @Description: JRedis管理
 * @author JornTang
 * @date 2018年1月30日
 */
public class JedisShiroCacheManager implements ShiroCacheManager {

    @Override
    public <K, V> Cache<K, V> getCache(String name) {
        return new JedisShiroCache<K, V>(name);
    }

    @Override
    public void destroy() {
    	//如果和其他系统，或者应用在一起就不能关闭
    	//getJedisManager().getJedis().shutdown();
    }
}
