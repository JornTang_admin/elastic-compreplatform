package org.elastic.compreplatform.admin.manager.sysuser.service;

import java.util.Set;

/**
 * ClassName: IRoleService 
 * @Description: 用户角色操作接口定义
 * @author JornTang
 * @date 2018年1月28日
 */
public interface IRoleService {
	/**
	 * @Description: 根据用户ID查询角色
	 * @param userId
	 * @return   
	 * @return Set<String>  
	 * @throws
	 * @author JornTang
	 * @date 2018年1月28日
	 */
	Set<String> findRoleByUserId(Long userId);

}
