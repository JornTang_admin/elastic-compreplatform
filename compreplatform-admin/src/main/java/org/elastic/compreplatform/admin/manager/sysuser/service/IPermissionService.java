package org.elastic.compreplatform.admin.manager.sysuser.service;

import java.util.Set;

/**
 * ClassName: IPermissionService 
 * @Description: 用户角色权限操作接口定义
 * @author JornTang
 * @date 2018年1月28日
 */
public interface IPermissionService {
	/**
	 * @Description: 根据用户ID查询权限
	 * @param userId
	 * @return   
	 * @return Set<String>  
	 * @throws
	 * @author JornTang
	 * @date 2018年1月28日
	 */
	Set<String> findPermissionByUserId(Long userId);

}
