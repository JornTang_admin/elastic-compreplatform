package org.elastic.compreplatform.admin.core.shiro.cache;

import java.util.Collection;
import java.util.Set;

import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.elastic.compreplatform.common.util.SerializationUtil;
import org.elastic.compreplatform.redis.util.JedisUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * ClassName: JedisShiroCache 
 * @Description: 缓存获取Manager
 * @author JornTang
 * @date 2018年1月27日
 */
public class JedisShiroCache<K, V> implements Cache<K, V> {
	private static Logger log = LoggerFactory.getLogger(JedisShiroCache.class);
	/**
	 * 为了不和其他的缓存混淆，采用追加前缀方式以作区分
	 */
    private static final String REDIS_SHIRO_CACHE = "shiro-demo-cache:";
    
    private String name;
    
	static final Class<JedisShiroCache> SELF = JedisShiroCache.class;
	public JedisShiroCache(String name){
		this.name = name;
	}
    /**
     * 自定义relm中的授权/认证的类名加上授权/认证英文名字
     */
    public String getName() {
        if (name == null)
            return "";
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public V get(K key) throws CacheException {
        byte[] byteKey = SerializationUtil.serialize(buildCacheKey(key));
        byte[] byteValue = new byte[0];
        try {
            byteValue = JedisUtil.getValueByKey(JedisUtil.DB_INDEX, byteKey);
            if(byteValue == null){
            	return null;
            }
        } catch (Exception e) {
            log.error("get value by cache throw exception",e);
        }
        return (V) SerializationUtil.deserialize(byteValue);
    }

    @Override
    public V put(K key, V value) throws CacheException {
        V previos = get(key);
        try {
        	JedisUtil.saveValueByKey(JedisUtil.DB_INDEX, SerializationUtil.serialize(buildCacheKey(key)),
                    SerializationUtil.serialize(value), -1);
        } catch (Exception e) {
        	 log.error("put cache throw exception",e);
        }
        return previos;
    }

    @Override
    public V remove(K key) throws CacheException {
        V previos = get(key);
        try {
        	JedisUtil.deleteByKey(JedisUtil.DB_INDEX, SerializationUtil.serialize(buildCacheKey(key)));
        } catch (Exception e) {
            log.error("remove cache  throw exception");
        }
        return previos;
    }

    @Override
    public void clear() throws CacheException {
        //TODO--
    }

    @Override
    public int size() {
        if (keys() == null)
            return 0;
        return keys().size();
    }

    @Override
    public Set<K> keys() {
        //TODO
        return null;
    }

    @Override
    public Collection<V> values() {
        //TODO
        return null;
    }

    private String buildCacheKey(Object key) {
        return REDIS_SHIRO_CACHE + getName() + ":" + key;
    }

}
