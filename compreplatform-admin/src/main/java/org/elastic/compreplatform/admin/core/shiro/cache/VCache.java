package org.elastic.compreplatform.admin.core.shiro.cache;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.elastic.compreplatform.common.util.SerializationUtil;
import org.elastic.compreplatform.redis.core.factory.JedisConnectionFactory;
import org.elastic.compreplatform.redis.util.JedisUtil;

import redis.clients.jedis.Jedis;
/**
 * ClassName: VCache 
 * @Description: 简单封装的Cache
 * @author JornTang
 * @date 2018年1月27日
 */
public class VCache {
	private VCache() {}
	/**
	 * 简单的Get
	 * @param <T>
	 * @param key
	 * @param requiredType
	 * @return
	 */
	public static <T> T get(String key , Class<T>...requiredType){
		Jedis jds = null;
		try {
			jds = JedisConnectionFactory.getJedis();
			jds.select(0);
			byte[] skey = key.getBytes();//SerializationUtil.serialize(key);
			return SerializationUtil.deserialize(jds.get(skey),requiredType);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JedisUtil.closeJedis(jds);
        }
		return null;
	}
	/**
	 * 简单的set
	 * @param key
	 * @param value
	 */
	public static void set(String key ,Object value){
		Jedis jds = null;
		try {
			jds = JedisConnectionFactory.getJedis();
			jds.select(0);
			byte[] skey = key.getBytes();//SerializationUtil.serialize(key);
			byte[] svalue = SerializationUtil.serialize(value);
			jds.set(skey, svalue);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JedisUtil.closeJedis(jds);
        }
	}
	/**
	 * 过期时间的
	 * @param key
	 * @param value
	 * @param timer （秒）
	 */
	public static void setex(String key, Object value, int timer) {
		Jedis jds = null;
		try {
			jds = JedisConnectionFactory.getJedis();
			jds.select(0);
			byte[] skey = key.getBytes();//SerializationUtil.serialize(key);
			byte[] svalue = SerializationUtil.serialize(value);
			jds.setex(skey, timer, svalue);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JedisUtil.closeJedis(jds);
        }
	}
	/**
	 * 
	 * @param <T>
	 * @param mapkey map
	 * @param key	 map里的key
	 * @param requiredType value的泛型类型
	 * @return
	 */
	public static <T> T getVByMap(String mapkey,String key , Class<T> requiredType){
		Jedis jds = null;
		try {
			jds = JedisConnectionFactory.getJedis();
			jds.select(0);
			byte[] mkey = SerializationUtil.serialize(mapkey);
			byte[] skey = SerializationUtil.serialize(key);
			List<byte[]> result = jds.hmget(mkey, skey);
			if(null != result && result.size() > 0 ){
				byte[] x = result.get(0);
				T resultObj = SerializationUtil.deserialize(x, requiredType);
				return resultObj;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			JedisUtil.closeJedis(jds);
		}
		return null;
	}
	/**
	 * 
	 * @param mapkey map
	 * @param key	 map里的key
	 * @param value   map里的value
	 */
	public static void setVByMap(String mapkey,String key ,Object value){
		Jedis jds = null;
		try {
			jds = JedisConnectionFactory.getJedis();
			jds.select(0);
			byte[] mkey = SerializationUtil.serialize(mapkey);
			byte[] skey = SerializationUtil.serialize(key);
			byte[] svalue = SerializationUtil.serialize(value);
			jds.hset(mkey, skey,svalue);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JedisUtil.closeJedis(jds);
        }
		
	}
	/**
	 * 删除Map里的值
	 * @param mapKey
	 * @param dkey
	 * @return
	 */
	public static Object delByMapKey(String mapKey ,String...dkey){
		Jedis jds = null;
		try {
			jds = JedisConnectionFactory.getJedis();
			jds.select(0);
			byte[][] dx = new byte[dkey.length][];
			for (int i = 0; i < dkey.length; i++) {
				dx[i] = SerializationUtil.serialize(dkey[i]);
			}
			byte[] mkey = SerializationUtil.serialize(mapKey);
			Long result = jds.hdel(mkey, dx);
			return result;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			JedisUtil.closeJedis(jds);
		}
		return new Long(0);
	}
	
	/**
	 * 往redis里取set整个集合
	 * 
	 * @param <T>
	 * @param setKey
	 * @param start
	 * @param end
	 * @param requiredType
	 * @return
	 */
	public static <T> Set<T> getVByList(String setKey,Class<T> requiredType){
		Jedis jds = null;
		try {
			jds = JedisConnectionFactory.getJedis();
			jds.select(0);
			byte[] lkey = SerializationUtil.serialize(setKey);
			Set<T> set = new TreeSet<T>();
			Set<byte[]> xx = jds.smembers(lkey);
			for (byte[] bs : xx) {
				T t = SerializationUtil.deserialize(bs, requiredType);
				set.add(t);
			}
			return set;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			JedisUtil.closeJedis(jds);
		}
		return null;
	}
	/**
	 * 获取Set长度
	 * @param setKey
	 * @return
	 */
	public static Long getLenBySet(String setKey){
		Jedis jds = null;
		try {
			jds = JedisConnectionFactory.getJedis();
			jds.select(0);
			Long result = jds.scard(setKey);
			return result;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			JedisUtil.closeJedis(jds);
		}
		return null;
	}
	/**
	 * 删除Set
	 * @param dkey
	 * @return
	 */
	public static Long delSetByKey(String key,String...dkey){
		Jedis jds = null;
		try {
			jds = JedisConnectionFactory.getJedis();
			jds.select(0);
			Long result = 0L;
			if(null == dkey){
				result = jds.srem(key);
			}else{
				result = jds.del(key);
			}
			return result;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			JedisUtil.closeJedis(jds);
		}
		return new Long(0);
	}
	/**
	 * 随机 Set 中的一个值
	 * @param key
	 * @return
	 */
	public static String srandmember(String key){
		Jedis jds = null;
		try {
			jds = JedisConnectionFactory.getJedis();
			jds.select(0);
			String result = jds.srandmember(key);
			return result;
		} catch (Exception e){ 
			e.printStackTrace();
		} finally {
			JedisUtil.closeJedis(jds);
		}
		return null;
	}
	/**
	 * 往redis里存Set
	 * @param setKey
	 * @param value
	 */
	public static void setVBySet(String setKey,String value){
		Jedis jds = null;
		try {
			jds = JedisConnectionFactory.getJedis();
			jds.select(0);
			jds.sadd(setKey, value);
		} catch (Exception e) {
            e.printStackTrace();
        } finally {
            JedisUtil.closeJedis(jds);
        }
	}
	/**
	 * 取set 
	 * @param key
	 * @return
	 */
	public static Set<String> getSetByKey(String key){
		Jedis jds = null;
		try {
			jds = JedisConnectionFactory.getJedis();
			jds.select(0);
			Set<String> result = jds.smembers(key);
			return result;
		} catch (Exception e) {
            e.printStackTrace();
        } finally {
            JedisUtil.closeJedis(jds);
        }
        return null;
		 
	}
	
	
	/**
	 * 往redis里存List
	 * @param listKey
	 * @param value
	 */
	public static void setVByList(String listKey,Object value){
		Jedis jds = null;
		try {
			jds = JedisConnectionFactory.getJedis();
			jds.select(0);
			byte[] lkey = SerializationUtil.serialize(listKey);
			byte[] svalue = SerializationUtil.serialize(value);
			jds.rpush(lkey, svalue);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			JedisUtil.closeJedis(jds);
		}
	}
	/**
	 * 往redis里取list
	 * 
	 * @param <T>
	 * @param listKey
	 * @param start
	 * @param end
	 * @param requiredType
	 * @return
	 */
	public static <T> List<T> getVByList(String listKey,int start,int end,Class<T> requiredType){
		Jedis jds = null;
		try {
			jds = JedisConnectionFactory.getJedis();
			jds.select(0);
			byte[] lkey = SerializationUtil.serialize(listKey);
			List<T> list = new ArrayList<T>();
			List<byte[]> xx = jds.lrange(lkey,start,end);
			for (byte[] bs : xx) {
				T t = SerializationUtil.deserialize(bs, requiredType);
				list.add(t);
			}
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			JedisUtil.closeJedis(jds);
		}
		return null;
	}
	/**
	 * 获取list长度
	 * @param listKey
	 * @return
	 */
	public static Long getLenByList(String listKey){
		Jedis jds = null;
		try {
			jds = JedisConnectionFactory.getJedis();
			jds.select(0);
			byte[] lkey = SerializationUtil.serialize(listKey);
			Long result = jds.llen(lkey);
			return result;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			JedisUtil.closeJedis(jds);
		}
		return null;
	}
	/**
	 * 删除
	 * @param dkey
	 * @return
	 */
	public static Long delByKey(String...dkey){
		Jedis jds = null;
		try {
			jds = JedisConnectionFactory.getJedis();
			jds.select(0);
			byte[][] dx = new byte[dkey.length][];
			for (int i = 0; i < dkey.length; i++) {
				dx[i] = SerializationUtil.serialize(dkey[i]);
			}
			Long result = jds.del(dx);
			return result;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			JedisUtil.closeJedis(jds);
		}
		return new Long(0);
	}
	/**
	 * 判断是否存在
	 * @param existskey
	 * @return
	 */
	public static boolean exists(String existskey){
		Jedis jds = null;
		try {
			jds = JedisConnectionFactory.getJedis();
			jds.select(0);
			byte[] lkey = SerializationUtil.serialize(existskey);
			return jds.exists(lkey);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			JedisUtil.closeJedis(jds);
		}
		return false;
	}
}
