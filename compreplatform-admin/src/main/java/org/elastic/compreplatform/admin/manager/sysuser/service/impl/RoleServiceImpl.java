package org.elastic.compreplatform.admin.manager.sysuser.service.impl;

import java.util.Set;

import javax.annotation.Resource;

import org.elastic.compreplatform.admin.manager.sysuser.dao.IRoleDao;
import org.elastic.compreplatform.admin.manager.sysuser.service.IRoleService;
import org.springframework.stereotype.Service;

/**
 * ClassName: IRoleService 
 * @Description: 用户角色操作实现类
 * @author JornTang
 * @date 2018年1月28日
 */
@Service("roleService")
public class RoleServiceImpl implements IRoleService {
	@Resource
	private IRoleDao roleDao;
	/**
	 * @Description: 根据用户ID查询角色
	 * @param userId
	 * @return   
	 * @return Set<String>  
	 * @throws
	 * @author JornTang
	 * @date 2018年1月28日
	 */
	@Override
	public Set<String> findRoleByUserId(Long userId) {
		return roleDao.findRoleByUserId(userId);
	}

}
