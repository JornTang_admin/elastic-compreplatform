<%@ tag  pageEncoding="UTF-8"%>
<%@tag import="java.util.Date"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ attribute name="headerType" type="java.lang.String" required="true" rtexprvalue="true"%>
<%
	String path = request.getContextPath();
	String serverUrl = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+"/";
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
	request.setAttribute("serverUrl",serverUrl);
	request.setAttribute("basePath",basePath);
%>
<c:choose>
	<c:when test='${headerType eq "base"}'>
		<link rel="shortcut icon" href="${basePath}/resource/common/ico/compre.ico" type="image/x-icon" />
	    <link rel="stylesheet" href="${basePath}resource/plugin/layui/css/layui.css">
		<link rel="stylesheet" href="${basePath}resource/bootstrap-3.3.6/css/bootstrap.min.css"/>
	    <link rel="stylesheet" href="${basePath}resource/common/common.css">
	    <script src="${basePath}resource/jquery/jquery-1.8.3.js"></script>
	    <script src="${basePath}resource/plugin/layui/layui.js"></script>
	</c:when>
	<c:when test='${headerType eq "particles"}'>
		<link rel="stylesheet" href="${basePath}resource/plugin/particles/style.css">
	    <script src="${basePath}resource/plugin/particles/particles.min.js"></script>
	    <script src="${basePath}resource/plugin/particles/app.js"></script>
	</c:when>
	<c:when test='${headerType eq "util"}'>
	    <script src="${basePath}resource/plugin/layui/layerUtil.js"></script>
	</c:when>
	<c:when test='${headerType eq "echarts"}'>
	    <script src="${basePath}resource/plugin/echarts/echarts.min.js"></script>
	    <script src="${basePath}resource/plugin/echarts/dark.js"></script>
	</c:when>
</c:choose>
