<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>${headTitle}</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <!-- 为兼容火狐此引用必须放在这里 -->
 <tag:header headerType="base"/>
</head>
<body>  
<form class="layui-form" action="" method='POST' style="width:100%;">
  <input name="id" type="hidden" value="${jobInfo.id}"/>
  <div class="compre-content">
  <div class="layui-form-item">
    <label class="layui-form-label"><i class='layui-must'>*</i>执行器</label>
    <div class="layui-input-inline">
      <select name="jobGroup" lay-verify="required">
      	<option value=""></option>
        <c:forEach items="${groupList}" var="group">
        	<option value="${group.id}" <c:if test="${group.id eq jobInfo.jobGroup}">selected="true"</c:if>>${group.registName}</option>
        </c:forEach>
      </select>
    </div>
    
    <label class="layui-form-label"><i class='layui-must'>*</i>任务描述</label>
    <div class="layui-input-inline">
      <input type="text" name="jobDesc" value="${jobInfo.jobDesc}" lay-verify="required" autocomplete="off" placeholder="请输入任务描述" class="layui-input">
    </div>
  </div>
  
  <div class="layui-form-item">
    <label class="layui-form-label"><i class='layui-must'>*</i>路由策略</label>
    <div class="layui-input-inline">
      <select name="executorRouteStrategy" lay-verify="required">
        <c:forEach items="${ExecutorRouteStrategyEnum}" var="stra">
        	<option value="${stra}" <c:if test="${stra eq jobInfo.executorRouteStrategy}">selected="true"</c:if>>${stra.title}</option>
        </c:forEach>
      </select>
    </div>
    
    <label class="layui-form-label"><i class='layui-must'>*</i>Cron</label>
    <div class="layui-input-inline">
      <input type="text" name="jobCron" value="${jobInfo.jobCron}" lay-verify="required" autocomplete="off" placeholder="请输入Cron" class="layui-input">
    </div>
  </div>
  
  <div class="layui-form-item">
    <label class="layui-form-label"><i class='layui-must'>*</i>运行模式</label>
    <div class="layui-input-inline">
      <select name="glueType" lay-verify="required">
        <c:forEach items="${GlueTypeEnum}" var="glueType">
        	<option value="${glueType}" <c:if test="${glueType eq jobInfo.glueType}">selected="true"</c:if>>${glueType.desc}</option>
        </c:forEach>
      </select>
    </div>
    
    <label class="layui-form-label"><i class='layui-must'>*</i>JobHandler</label>
    <div class="layui-input-inline">
      <input type="text" name="executorHandler" value="${jobInfo.executorHandler}" lay-verify="required" autocomplete="off" placeholder="请输入JobHandler" class="layui-input">
    </div>
  </div>
  
  <div class="layui-form-item">
    <label class="layui-form-label"><i class='layui-must'>*</i>失败处理策略</label>
    <div class="layui-input-inline">
      <select name="executorFailStrategy" lay-verify="required">
        <c:forEach items="${ExecutorFailStrategyEnum}" var="failStra">
        	<option value="${failStra}" <c:if test="${failStra eq jobInfo.executorFailStrategy}">selected="true"</c:if>>${failStra.title}</option>
        </c:forEach>
      </select>
    </div>
    
    <label class="layui-form-label">子任务Key</label>
    <div class="layui-input-inline">
      <input type="text" name="childJobKey" value="${jobInfo.childJobKey}" lay-verify="" autocomplete="off" placeholder="请输入子任务Key" class="layui-input">
    </div>
  </div>
  
  <div class="layui-form-item">
    <label class="layui-form-label"><i class='layui-must'>*</i>阻塞处理策略</label>
    <div class="layui-input-inline">
      <select name="executorBlockStrategy" lay-verify="required">
        <c:forEach items="${ExecutorBlockStrategyEnum}" var="blockStra">
        	<option value="${blockStra}" <c:if test="${blockStra eq jobInfo.executorBlockStrategy}">selected="true"</c:if>>${blockStra.title}</option>
        </c:forEach>
      </select>
    </div>
    
    <label class="layui-form-label">执行参数</label>
    <div class="layui-input-inline">
      <input type="text" name="executorParam" value="${jobInfo.executorParam}" lay-verify="" autocomplete="off" placeholder="请输入执行参数" class="layui-input">
    </div>
  </div>
  
  
  <div class="layui-form-item">
    <label class="layui-form-label">报警邮件</label>
    <div class="layui-input-inline">
      <input type="text" name="alarmEmail" value="${jobInfo.alarmEmail}" lay-verify="" autocomplete="off" placeholder="请输入报警邮件" class="layui-input">
    </div>
    
    <label class="layui-form-label"><i class='layui-must'>*</i>负责人</label>
    <div class="layui-input-inline">
      <input type="text" name="author" value="${jobInfo.author}" lay-verify="required" autocomplete="off" placeholder="请输入负责人" class="layui-input">
    </div>
  </div>
  
  </div>
   <!-- layui-footer layui-fixbar -->
  <div class="layui-form-item">
  	<div class="layui-inline compre-footer">
      <label class="layui-form-label"></label>
      <button class="layui-btn compre-btn" lay-submit="" lay-filter="form-submit">提交</button>
      <button type="reset" class="layui-btn compre-btn compre-btn-reset layui-btn-primary">重置</button>
    </div>
  </div>
</form>
</body>
<script>
layui.use('form', function(){
	  var form = layui.form,
	  		layer = layui.layer,
	  		$ = layui.$;
	  
	  
	  //监听提交
	  form.on('submit(form-submit)', function(data){
	  	var url = '${basePath}jobinfo/add.do';
	  	if(data.field.id != ''){
	  		url = '${basePath}jobinfo/reschedule.do';
	  	}
	  	$.post(url,data.field,function(result){
	  		if(result.code != 200){
	  			window.top.layer.msg(result.msg);
	  		}else{
	  			window.top.layer.closeAll();
	  			window.top.layer.msg(result.msg);
	  		}
	  	});
	    return false;
	  });
	  //注册方式监听事件
	  form.on('radio(compre-radio)', function(data){
	  	if(data.value == 0){
	  		$('input[name="addressList"]').attr('disabled','disabled').val("");
	  	}else{
	  		$('input[name="addressList"]').removeAttr('disabled').attr('lay-verify','required');
	  	};
	  });
	  if('${group.addressType}' == 1){
	  	$('input[name="addressList"]').removeAttr('disabled');
	  }
});
</script>
</html>