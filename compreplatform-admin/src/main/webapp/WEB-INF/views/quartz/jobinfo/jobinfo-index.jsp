<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>layui</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <!-- 为兼容火狐此引用必须放在这里 -->
  <tag:header headerType="base"/>
</head>
<body>
	<div class="compre-search">
	 	<div class="layui-btn-group layui-btn-left compreTable">
	    	<button class="layui-btn layui-btn-primary" data-type="addJobinfo"><i class="layui-icon"></i>添加</button>
	  	</div>
	  	<div class="layui-right-search">
	  		<form class="layui-form layui-form-left" action="">
			    <select name="searchGroup" id="searchGroup" lay-search="">
			      <option value="">选择或搜索执行器</option>
			      <c:forEach items="${JobGroupList}" var="group">
			      	<option value="${group.id}">${group.registName}</option>
			      </c:forEach>
			    </select>
		    </form>
	  		<input type="text" name="" id="jobHandler" placeholder="根据JobHandler搜索" autocomplete="off" class="layui-input layui-input-right">  
	  		<button class="layui-btn layui-btn-primary layui-btn-left" id="searchTable"><i class="layui-icon">&#xe615;</i>搜索</button>
	  	</div> 
  	</div>
	<table class="layui-table" lay-data="{height: 'full-120', cellMinWidth: 50, limit:15}" lay-filter="compreTable">
  	<thead>
	    <tr>
	      <th lay-data="{type:'checkbox', fixed: 'left'}"></th>
	      <th lay-data="{field:'id',templet:'#jobKey'}">JobKey</th>
	      <th lay-data="{field:'controlFlag',templet:'#controlFlag'}">控制权限</th>
	      <th lay-data="{field:'jobDesc'}">描述</th>
	      <th lay-data="{field:'glueType',templet:'#glueType'}">运行模式</th>
	      <th lay-data="{field:'jobCron'}">Cron</th>
	      <th lay-data="{field:'author'}">负责人</th>
	      <th lay-data="{field:'jobStatus',templet:'#jobStatus'}">状态</th>
	      <th lay-data="{fixed: 'right', width:238, align:'center', toolbar: '#barcompreTable'}">操作</th>
	    </tr>
  </thead>
</table>
<!-- layui模板控制 --> 
<script type="text/html" id="glueType">
	{{d.glueType}}:{{d.executorHandler}}
</script>
<script type="text/html" id="jobKey">
	{{d.jobGroup}}_{{d.id}}
</script>
<script type="text/html" id="controlFlag">
  	{{#  
		if(d.controlFlag=='0'){ }}
			<span class="layui-badge layui-bg-blue">综合支撑平台</span>
		{{#  } 
		else if(d.controlFlag=='1') { }}
    		<span class="layui-badge layui-bg-gray">客户端</span>
		{{#  }
	}}
</script>
<script type="text/html" id="jobStatus">
  	{{#  
		if(d.jobStatus=='NORMAL'){ }}
			<span class="layui-badge layui-bg-green"><i class="layui-icon"></i>NORMAL</span>
		{{#  } 
		else if(d.jobStatus=='PAUSED') { }}
    		<span class="layui-badge layui-bg-orange"><i class="layui-icon"></i>PAUSED</span>
		{{#  }
		else { }}
    		<span class="layui-badge layui-bg-orange">暂停</span>
		{{#  }
	}}
</script>
<script type="text/html" id="barcompreTable">
  <a class="layui-btn layui-btn-xs" lay-event="trigger">执行</a>
  <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="pause">暂停</a>
<a class="layui-btn layui-btn-green layui-btn-xs" lay-event="resume">恢复</a>
  <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
  <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
</script>
<script>
layui.use('table', function(){
  var table = layui.table,
  		$ = layui.$;
  var tableOptions = {
        url: '${basePath}jobinfo/pageList.do', //请求地址
        method: 'POST', //方式
        id: 'listReload', //生成 Layui table 的标识 id，必须提供，用于后文刷新操作，笔者该处出过问题
        page: true, //是否分页
        where: { type: "all" }, //请求后端接口的条件，该处就是条件错误点，按照官方给出的代码示例，原先写成了 where: { key : { type: "all" } }，结果并不是我想的那样，如此写，key 将是后端的一个类作为参数，里面有 type 属性，如果误以为 key 是 Layui 提供的格式，那就大错特错了
        response: { //定义后端 json 格式，详细参见官方文档
            statusName: 'code', //状态字段名称
            statusCode: '200', //状态字段成功值
            msgName: 'msg', //消息字段
        }
    };
    table.init('compreTable', tableOptions);
  //条件搜索	
  $('#searchTable').on('click',function(){
  		table.reload("listReload", {
             where: {
                     jobGroup: $('#searchGroup').find('option:selected').val(),
                     executorHandler: $('#jobHandler').val()
                     
             }
        });
  });
  //监听表格复选框选择
  table.on('checkbox(compreTable)', function(obj){
    console.log(obj)
  });
  //监听工具条
  table.on('tool(compreTable)', function(obj){
    var data = obj.data;
    if(obj.event === 'del'){
      top.layer.confirm('确定删除?', function(index){
      	$.post('${basePath}jobinfo/remove.do',{id: data.id},function(result){
      		top.layer.msg(result.msg);
      		table.reload('listReload');
        	top.layer.close(index);
      	});
      });
    } else if(obj.event === 'edit'){
      	window.top.$.layerFrame({callBack:function(){table.reload('listReload');},content: ['${basePath}jobinfo/toAddOrUpdate.do?id=' + data.id,'no'],title: '修改任务',});
    }else if(obj.event === 'trigger'){
    	$.post('${basePath}jobinfo/trigger.do',{id: data.id},function(result){
      		top.layer.msg(result.msg);
      		table.reload('listReload');
        	top.layer.close(index);
      	});
    }else if(obj.event === 'pause'){
    	$.post('${basePath}jobinfo/pause.do',{id: data.id},function(result){
      		top.layer.msg(result.msg);
      		table.reload('listReload');
        	top.layer.close(index);
      	});
    }else if(obj.event === 'resume'){
    	$.post('${basePath}jobinfo/resume.do',{id: data.id},function(result){
      		top.layer.msg(result.msg);
      		table.reload('listReload');
        	top.layer.close(index);
      	});
    }
  });
  
  var $ = layui.$, active = {
  	//数据库配置
  	addJobinfo: function(opt){
  		window.top.$.layerFrame({callBack:function(){table.reload('listReload');},content: ['${basePath}jobinfo/toAddOrUpdate.do','no'], area:[ '850px', '550px' ],title: '添加任务',});
  	}
  };
  
  $('.compreTable .layui-btn').on('click', function(){
    var type = $(this).data('type');
    active[type] ? active[type].call(this) : '';
  });
});
</script>

</body>
</html>