<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>layui</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <!-- 为兼容火狐此引用必须放在这里 -->
  <tag:header headerType="base"/>
</head>
<body>
	<div class="compre-search">
	  	<div class="layui-right-search">
	  		<input type="text" name="" id="executorClient" placeholder="根据授权客户端搜索" autocomplete="off" class="layui-input layui-input-right"> 
	  		<input type="text" name="" id="clientName" placeholder="根据客户端名称搜索" autocomplete="off" class="layui-input layui-input-right"> 
	  		<button class="layui-btn layui-btn-primary layui-btn-left" id="searchTable"><i class="layui-icon">&#xe615;</i>搜索</button>
	  	</div> 
  	</div>
	<table class="layui-table" lay-data="{height: 'full-120', cellMinWidth: 50, limit:15}" lay-filter="compreTable">
  	<thead>
	    <tr>
	      <th lay-data="{type:'checkbox', fixed: 'left'}"></th>
	      <th lay-data="{field:'registryGroup'}">注册分组</th>
	      <th lay-data="{field:'registryValue'}">注册值</th>
	      <th lay-data="{field:'executorClient'}">授权客户端</th>
	      <th lay-data="{field:'clientName'}">客户端名称</th>
	      <th lay-data="{field:'ifGrant',templet:'#ifGrantTemp'}">是否授权</th>
	      <th lay-data="{field:'crtTime'}">创建时间</th>
	      <th lay-data="{field:'updateTime'}">修改时间</th>
	      <th lay-data="{fixed: 'right', width:178, align:'center', toolbar: '#barcompreTable'}">操作</th>
	    </tr>
  </thead>
</table>
<script type="text/html" id="ifGrantTemp">
  	{{#  
		if(d.ifGrant=='0'){ }}
    		<span style="color:red;">未授权</span>
		{{#  } 
		else if(d.ifGrant=='1') { }}
    		<span style="color:green;">已授权</span>
		{{#  }
	}}
</script>  
<script type="text/html" id="barcompreTable">
  <a class="layui-btn layui-btn-xs" lay-event="grant">授权</a>
<a class="layui-btn layui-btn-xs" lay-event="notgrant">取消授权</a>
</script>
<script>
layui.use('table', function(){
  var table = layui.table,
  		$ = layui.$;
  var tableOptions = {
        url: '${basePath}jobregistry/pageList.do', //请求地址
        method: 'POST', //方式
        id: 'listReload', //生成 Layui table 的标识 id，必须提供，用于后文刷新操作，笔者该处出过问题
        page: true, //是否分页
        where: { type: "all" }, //请求后端接口的条件，该处就是条件错误点，按照官方给出的代码示例，原先写成了 where: { key : { type: "all" } }，结果并不是我想的那样，如此写，key 将是后端的一个类作为参数，里面有 type 属性，如果误以为 key 是 Layui 提供的格式，那就大错特错了
        response: { //定义后端 json 格式，详细参见官方文档
            statusName: 'code', //状态字段名称
            statusCode: '200', //状态字段成功值
            msgName: 'msg', //消息字段
        }
    };
    table.init('compreTable', tableOptions);
  //条件搜索	
  $('#searchTable').on('click',function(){
  		table.reload("listReload", {
             where: {
                     executorClient: $('#executorClient').val(),
                     clientName: $('#clientName').val()
             }
        });
  });
  //监听表格复选框选择
  table.on('checkbox(compreTable)', function(obj){
    console.log(obj)
  });
  //监听工具条
  table.on('tool(compreTable)', function(obj){
    var data = obj.data;
    if(obj.event === 'grant'){
      top.layer.confirm('确定授权?', function(index){
      	$.post('${basePath}jobregistry/updateRegistGrant.do',{id: data.id,ifGrant: 1},function(result){
      		top.layer.msg(result.msg);
      		table.reload('listReload');
        	top.layer.close(index);
      	});
      });
    }else if(obj.event === 'notgrant'){
      top.layer.confirm('确定取消授权?', function(index){
      	$.post('${basePath}jobregistry/updateRegistGrant.do',{id: data.id,ifGrant: 0},function(result){
      		top.layer.msg(result.msg);
      		table.reload('listReload');
        	top.layer.close(index);
      	});
      });
    }
  });
  
  var $ = layui.$, active = {
  	//数据库配置
  	dbConfig: function(opt){
  		window.top.$.layerFrame({callBack:function(){table.reload('listReload');},content: ['${basePath}codegener/toDbconfAddOrUpdate.do','no'],title: '添加数据库配置',});
  	}
  };
  
  $('.compreTable .layui-btn').on('click', function(){
    var type = $(this).data('type');
    active[type] ? active[type].call(this) : '';
  });
});
</script>

</body>
</html>