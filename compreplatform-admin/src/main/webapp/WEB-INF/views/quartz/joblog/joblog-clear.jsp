<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>${headTitle}</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <!-- 为兼容火狐此引用必须放在这里 -->
 <tag:header headerType="base"/>
</head>
<body>  
<form class="layui-form" action="" method='POST' style="width:100%;">
  <div class="compre-content" style="height: 345px;">
  <div class="layui-form-item">
    <label class="layui-form-label">执行器</label>
    <div class="layui-input-inline layui-input-70">
      	<input type="text" disabled="disabled" data-meta='${group.id eq null?0: group.id}' id="jobGroup" value="${group.id eq 0 or group.id eq null?'全部': group.title}" lay-verify="required" autocomplete="off" class="layui-input">
    </div>
  </div>
  <div class="layui-form-item">
    <label class="layui-form-label">任务</label>
    <div class="layui-input-inline layui-input-70">
      <input type="text" disabled="disabled" data-meta='${jobInfo.id eq null?0: jobInfo.id}' id="jobId" value="${jobInfo.id eq 0 or jobInfo.id eq null?'全部': jobInfo.jobDesc}" lay-verify="required" autocomplete="off" class="layui-input">
    </div>
  </div>
  
  <div class="layui-form-item">
    <div class="layui-inline">
      <label class="layui-form-label">清理类型</label>
      <div class="layui-input-inline layui-input-70">
      	 <select id="clearType" lay-verify="required" lay-filter="dbType">
	       	<option value="1">清理一个月之前日志数据</option>
            <option value="2">清理三个月之前日志数据</option>
            <option value="3">清理六个月之前日志数据</option>
            <option value="4">清理一年之前日志数据</option>
            <option value="5">清理一千条以前日志数据</option>
            <option value="6">清理一万条以前日志数据</option>
            <option value="7">清理三万条以前日志数据</option>
            <option value="8">清理十万条以前日志数据</option>
            <option value="9">清理所用日志数据</option>
	     </select>
      </div>
    </div>
  </div>
  </div>
    <!-- layui-footer layui-fixbar -->
  <div class="layui-form-item">
  	<div class="layui-inline compre-footer">
      <label class="layui-form-label"></label>
      <button class="layui-btn compre-btn" lay-submit="" lay-filter="form-submit">提交</button>
      <button type="reset" class="layui-btn compre-btn compre-btn-reset layui-btn-primary">重置</button>
    </div>
  </div>
</form>
</body>
<script>
layui.use('form', function(){
	  var form = layui.form,
	  		layer = layui.layer,
	  		$ = layui.$;
	  
	  //监听提交
	  form.on('submit(form-submit)', function(data){
	  	var url = '${basePath}joblog/clearLog.do';
	  	var jobGroup = $('#jobGroup').attr('data-meta');
	  	var jobId = $('#jobId').attr('data-meta');
	  	var clearType = $('#clearType').find('option:selected').val();
	  	$.post(url,{jobGroup: jobGroup, jobId: jobId, type: clearType},function(result){
	  		if(result.code != 200){
	  			window.top.layer.msg(result.msg);
	  		}else{
	  			window.top.layer.closeAll();
	  			window.top.layer.msg(result.msg);
	  		}
	  	});
	    return false;
	  });
});
</script>
</html>