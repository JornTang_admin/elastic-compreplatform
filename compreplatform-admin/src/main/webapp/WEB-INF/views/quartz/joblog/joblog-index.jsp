<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>layui</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <!-- 为兼容火狐此引用必须放在这里 -->
  <tag:header headerType="base"/>
</head>
<body>
	<div class="compre-search">
	 	<div class="layui-btn-group layui-btn-left compreTable">
	    	<button class="layui-btn layui-btn-primary" data-type="clear"><i class="layui-icon"></i>清理</button>
	  	</div>
	  	<div class="layui-right-search">
	  		<form class="layui-form layui-form-left" action="">
			    <select name="jobGroup" id="searchGroup" lay-search="" lay-filter="jobGroup">
			      <option value="">选择或搜索执行器</option>
			      <c:forEach items="${JobGroupList}" var="group">
			      	<option value="${group.id}">${group.registName}</option>
			      </c:forEach>
			    </select>
		    </form>
	  		<form class="layui-form layui-form-left" action="">
			    <select name="jobId" id="searchJob" lay-search="">
			      <option value="">选择或搜索任务</option>
			    </select>
		    </form> 
		    <form class="layui-form layui-form-left" action="">
			    <select name="logStatus" id="searchLogStatus">
			      <option value="-1">全部</option>
                  <option value="1">成功</option>
                  <option value="2">失败</option>
                  <option value="3">进行中</option>
			    </select>
		    </form>
		    <form class="layui-form layui-form-left" action="">
			    <select name="filterType" id="searchFilterTime" lay-search="">
			      <option value="-1">全部</option>
			      <option value="1">最近1小时</option>
			      <option value="2">今日</option>
			      <option value="3">昨日</option>
			      <option value="4">最近7日</option>
			      <option value="5">最近30日</option>
			      <option value="6">本月</option>
			      <option value="7">上个月</option>
			    </select>
		    </form> 
	  		<button class="layui-btn layui-btn-primary layui-btn-left" id="searchTable"><i class="layui-icon">&#xe615;</i>搜索</button>
	  	</div> 
  	</div>
	<table class="layui-table" lay-data="{height: 'full-120', cellMinWidth: 50, limit:15}" lay-filter="compreTable">
  	<thead>
	    <tr>
	      <th lay-data="{field:'id',templet:'#jobKey'}">JobKey</th>
	      <th lay-data="{field:'triggerTime'}">调度时间</th>
	      <th lay-data="{field:'triggerCode',templet:'#triggerCode'}">调度结果</th>
	      <th lay-data="{field:'triggerMsg',event: 'triggerMsg', style:'cursor: pointer;'}">调度备注</th>
	      <th lay-data="{field:'handleTime'}">执行时间</th>
	      <th lay-data="{field:'handleCode',templet:'#handleCode'}">执行结果</th>
	      <th lay-data="{field:'handleMsg',event: 'handleMsg', style:'cursor: pointer;'}">执行备注</th>
	    </tr>
  </thead>
</table>
<!-- layui模板控制 --> 
<script type="text/html" id="triggerCode">
  	{{#  
		if(d.triggerCode=='200'){ }}
			<span class="layui-badge layui-bg-green">成功</span>
		{{#  } 
		else { }}
    		<span class="layui-badge">失败</span>
		{{#  }
	}}
</script>
<script type="text/html" id="handleCode">
  	{{#  
		if(d.handleCode=='200'){ }}
			<span class="layui-badge layui-bg-green">成功</span>
		{{#  } 
		else { }}
    		<span class="layui-badge">失败</span>
		{{#  }
	}}
</script>
<script>
layui.use(['table','form'], function(){
  var table = layui.table,
  		form = layui.form,
  		$ = layui.$;
  var tableOptions = {
        url: '${basePath}joblog/pageList.do', //请求地址
        method: 'POST', //方式
        id: 'listReload', //生成 Layui table 的标识 id，必须提供，用于后文刷新操作，笔者该处出过问题
        page: true, //是否分页
        where: { type: "all" }, //请求后端接口的条件，该处就是条件错误点，按照官方给出的代码示例，原先写成了 where: { key : { type: "all" } }，结果并不是我想的那样，如此写，key 将是后端的一个类作为参数，里面有 type 属性，如果误以为 key 是 Layui 提供的格式，那就大错特错了
        response: { //定义后端 json 格式，详细参见官方文档
            statusName: 'code', //状态字段名称
            statusCode: '200', //状态字段成功值
            msgName: 'msg', //消息字段
        }
    };
    table.init('compreTable', tableOptions);
  //条件搜索	
  $('#searchTable').on('click',function(){
  		table.reload("listReload", {
             where: {
                     jobGroup: $('#searchGroup').find('option:selected').val(),
                     jobId: $('#searchJob').val(),
                     logStatus: $('#searchLogStatus').val(),
                     filterType: $('#searchFilterTime').val()
                     
             }
        });
  });
  //监听表格复选框选择
  table.on('checkbox(compreTable)', function(obj){
    console.log(obj)
  });
  //监听工具条
  table.on('tool(compreTable)', function(obj){
    var data = obj.data;
    //删除
    if(obj.event === 'del'){
      top.layer.confirm('确定删除?', function(index){
      	$.post('${basePath}jobinfo/remove.do',{id: data.id},function(result){
      		top.layer.msg(result.msg);
      		table.reload('listReload');
        	top.layer.close(index);
      	});
      });
    //编辑
    } else if(obj.event === 'edit'){
      	window.top.$.layerFrame({callBack:function(){table.reload('listReload');},content: ['${basePath}jobinfo/toAddOrUpdate.do?id=' + data.id,'no'],title: '修改任务',});
    //执行
    }else if(obj.event === 'trigger'){
    	$.post('${basePath}jobinfo/trigger.do',{id: data.id},function(result){
      		top.layer.msg(result.msg);
      		table.reload('listReload');
        	top.layer.close(index);
      	});
    //暂停
    }else if(obj.event === 'pause'){
    	$.post('${basePath}jobinfo/pause.do',{id: data.id},function(result){
      		top.layer.msg(result.msg);
      		table.reload('listReload');
        	top.layer.close(index);
      	});
    //恢复
    }else if(obj.event === 'resume'){
    	$.post('${basePath}jobinfo/resume.do',{id: data.id},function(result){
      		top.layer.msg(result.msg);
      		table.reload('listReload');
        	top.layer.close(index);
      	});
    //查看调度备注  	
    }else if(obj.event === 'triggerMsg'){
    //查看执行备注
    }else if(obj.event === 'handleMsg'){
    
    }
  });
  
  var $ = layui.$, active = {
  	//清理日志
  	clear: function(opt){
  		var jobGroup = $('#searchGroup').find('option:selected').val(),
  			groupName = $('#searchGroup').find('option:selected').text();
  		if(jobGroup == ''){
  			jobGroup = 0;
  		}
  		var jobId = $('#searchJob').find('option:selected').val()
  			jobName = $('#searchJob').find('option:selected').text();
  		if(jobId == ''){
  			jobId = 0;
  		}
  		var url = '${basePath}joblog/toClearlog.do?jobGroup=' + jobGroup + '&jobId='+jobId;
  		window.top.$.layerFrame({callBack:function(){table.reload('listReload');},content: [url,'no'], area:[ '550px', '450px' ],title: '清理日志',});
  	}
  };
  
  $('.compreTable .layui-btn').on('click', function(){
    var type = $(this).data('type');
    active[type] ? active[type].call(this) : '';
  });
  
  form.on('select(jobGroup)', function(data){
  		$.post('${basePath}joblog/getJobsByGroup.do',{jobGroup: data.value},function(result){
  			var _data = result.data,jobhtml='<option value="">选择或搜索任务</option>'; 
  			for(var x=0; x< _data.length; x++){
  				var jobInfo = _data[x];
  				jobhtml += '<option value="'+jobInfo.id+'">'+jobInfo.jobDesc+'</option>';
  			}
  			$('#searchJob').html(jobhtml);
  			//刷新下拉列表
  			form.render('select');
      	});
  });
});
</script>

</body>
</html>