<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>${headTitle}</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <!-- 为兼容火狐此引用必须放在这里 -->
 <tag:header headerType="base"/>
 <tag:header headerType="echarts"/>
 <style type="text/css">
 	.float-e-margins{
 		padding-left: 10px;
 	}
 	.col-sm-4{
 		border-right: solid 3px;
 	}
 </style>
</head>
<body>
	<div style="background: #333;color:#fff;margin-bottom: 10px;height:80px;">
	    <div class="row">
	    	<div class="col-sm-4">
	            <div class="ibox float-e-margins">
	                <div class="ibox-title">
	                	<h5>任务数量</h5>
	                	<span class="lable-span pull-right" style="margin-top: -30px;">系统中配置的任务数量</span>
	                </div>
	                <div class="ibox-content" style="line-height: 1;">
	                	<i class="layui-icon" style="font-size: 35px;"></i>
	                	<span class="pull-right">
		                    <h2 class="no-margins" style="margin-top: 0px;" id="jobTotal">0</h2>
	                    </span>
	                </div>
	            </div>
	        </div>
	        <div class="col-sm-4">
	            <div class="ibox float-e-margins">
	                <div class="ibox-title">
	                	<h5>调度次数</h5>
	                	<span class="lable-span pull-right" style="margin-top: -30px;">调度中心触发的调度次数</span>
	                </div>
	                <div class="ibox-content" style="line-height: 1;">
	                	<i class="layui-icon" style="font-size: 35px;">&#xe637;</i>
	                	<span class="pull-right">
		                    <h2 class="no-margins" style="margin-top: 0px;" id="triggerTotal">0</h2>
	                    </span>
	                </div>
	            </div>
	        </div>
	        <div class="col-sm-4">
	            <div class="ibox float-e-margins">
	                <div class="ibox-title">
	                    <h5>执行器数量</h5>
	                    <span class="lable-span pull-right" style="margin-top: -30px;">心跳检测成功的执行器机器数量</span>
	                </div>
	                <div class="ibox-content" style="line-height: 1;">
	                	<i class="layui-icon" style="font-size: 35px;">&#xe628;</i>
	                	<span class="pull-right">
		                    <h2 class="no-margins" style="margin-top: 0px;" id="groupTotal">0</h2>
	                    </span>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
	<div id="jobChart" style="width: 100%; height: 400px; -moz-user-select: none; position: relative; background: transparent none repeat scroll 0% 0%;" _echarts_instance_="ec_1515575675805"><div style="position: relative; overflow: hidden; width: 783px; height: 380px; padding: 0px; margin: 0px; border-width: 0px; cursor: default;"><canvas style="position: absolute; left: 0px; top: 0px; width: 783px; height: 380px; -moz-user-select: none; padding: 0px; margin: 0px; border-width: 0px;" width="783" height="380" data-zr-dom-id="zr_0"></canvas></div><div style="position: absolute; display: none; border-style: solid; white-space: nowrap; z-index: 9999999; transition: left 0.4s cubic-bezier(0.23, 1, 0.32, 1) 0s, top 0.4s cubic-bezier(0.23, 1, 0.32, 1) 0s; background-color: rgba(50, 50, 50, 0.7); border-width: 0px; border-color: rgb(51, 51, 51); border-radius: 4px; color: rgb(255, 255, 255); font: normal normal normal normal 14px/21px Microsoft YaHei; padding: 5px; left: 320px; top: 240px;">利用方式 <br>婚姻凭证 : 0 (0%)</div></div>
</body>
<script type="text/javascript">
	//JavaScript代码区域
	layui.use(['layer','jquery','element'], function(){
		var $ = layui.jquery,
      		element = layui.element,
      		layer = layui.layer;
    //获取统计信息
    $.post('${basePath}joblog/dashboardInfo.do',{},function(result){
    	var data = result.data;
    	$('#jobTotal').text(data.jobInfoCount);
    	$('#triggerTotal').text(data.jobLogCount);
    	$('#groupTotal').text(data.executorCount);
    },'json');
	//任务调度
    var jobChartOption = {
		    title: {
		        text: '按日期调度分布',
		        left: 'left',
		        top: 0,
		        textStyle: {
		            color: '#fff'
		        }
		    },
    	    tooltip : {
    	        trigger: 'axis',
    	        axisPointer: {
    	            type: 'cross',
    	            label: {
    	                backgroundColor: '#6a7985'
    	            }
    	        }
    	    },
    	    legend: {
    	        data:['成功调度次数','失败调度次数']
    	    },
    	    grid: {
    	        left: '1%',
    	        right: '35%',
    	        bottom: '3%',
    	        containLabel: true
    	    },
    	    xAxis : [
    	        {
    	            type : 'category',
    	            boundaryGap : false,
    	            data : ['2014-01-01','2015-01-01','2016-01-01','2017-01-01']
    	        }
    	    ],
    	    yAxis : [
    	        {
    	            type : 'value'
    	        }
    	    ],
    	    series : [
    	        {
    	            name:'成功调度次数',
    	            type:'line',
    	            stack: '总量',
    	            areaStyle: {normal: {}},
    	            data:[0, 0, 0, 0]
    	        },
    	        {
    	            name:'失败调度次数',
    	            type:'line',
    	            stack: '总量',
    	            areaStyle: {normal: {}},
    	            data:[0, 0, 0, 0]
    	        },
    	        {
	                name:'调度比例图',
	                type:'pie',
		            center: ['80%', '45%'],
	                radius : '45%',
	                label: {
	                    normal: {
	                        position: 'outside',
	                        formatter: '{b}:【占比{d}%】',
	                        itemStyle: {
	    		                emphasis: {
	    		                    shadowBlur: 10,
	    		                    shadowOffsetX: 0,
	    		                    shadowColor: 'rgba(0, 0, 0, 0.5)'
	    		                }
	    		            }
	                    }
	                    
	                },
	                data:[
	                    {value:0, name:'成功调度次数'},
	                    {value:0, name:'失败调度次数'}
	                ]
            	}
    	    ]
    	};
  	//初始化echarts实例
    var jobCharts = echarts.init(document.getElementById('jobChart'),'dark');
 	//使用刚指定的配置项和数据显示图表。
    jobCharts.setOption(jobChartOption);
    //异步加载
	$.ajax({
			url:"${basePath}joblog/triggerChart.do",
            async:true,
        	type:"post",
        	cache:false,
			timeout : 30000, //超时时间设置，单位毫秒
        	dataType:"json",
        	data:{},
        	success: function(result){
        		var data = result.data;
				//折线图
				var triggerDayList = data.triggerDayList;
				var triggerDayCountSucList = data.triggerDayCountSucList;
				var triggerDayCountFailList = data.triggerDayCountFailList;
				jobChartOption.xAxis[0].data= triggerDayList;
				jobChartOption.series[0].data= triggerDayCountSucList;
				jobChartOption.series[1].data= triggerDayCountFailList;
				//饼状图
				var triggerCountSucTotal = data.triggerCountSucTotal;
				var triggerCountFailTotal = data.triggerCountFailTotal;
				jobChartOption.series[2].data[0].value= triggerCountSucTotal;
				jobChartOption.series[2].data[1].value= triggerCountFailTotal;
				jobCharts.setOption(jobChartOption);
			}
		});
	});
</script>
</html>