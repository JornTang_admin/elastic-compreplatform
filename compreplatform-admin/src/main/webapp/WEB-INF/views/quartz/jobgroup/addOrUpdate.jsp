<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>${headTitle}</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <!-- 为兼容火狐此引用必须放在这里 -->
 <tag:header headerType="base"/>
</head>
<body>  
<form class="layui-form" action="" method='POST' style="width:100%;">
  <input name="id" type="hidden" value="${group.id}"/>
  <div class="compre-content">
  <div class="layui-form-item">
    <label class="layui-form-label">注册授权</label>
    <div class="layui-input-inline layui-input-70">
      <select name="registId" lay-verify="required" lay-filter="dbType">
        <option value=""></option>
        <c:forEach items="${regists}" var="rt">
        	<option value="${rt.id}" <c:if test="${rt.id eq group.registId}">selected="true"</c:if>>${rt.clientName}</option>
        </c:forEach>
      </select>
    </div>
  </div>
  <div class="layui-form-item">
    <label class="layui-form-label">AppName</label>
    <div class="layui-input-inline layui-input-70">
      <input type="text" name="appName" value="${group.appName}" lay-verify="required" autocomplete="off" placeholder="请输入AppName" class="layui-input">
    </div>
  </div>
  
  <div class="layui-form-item">
    <div class="layui-inline">
      <label class="layui-form-label">名称</label>
      <div class="layui-input-inline layui-input-70">
        <input type="tel" name="title" value="${group.title}" lay-verify="required" autocomplete="off" placeholder="请输入名称" class="layui-input">
      </div>
    </div>
  </div>
  
  <div class="layui-form-item">
    <div class="layui-inline">
      <label class="layui-form-label">排序</label>
      <div class="layui-input-inline layui-input-70">
        <input type="text" name="reorder" value="${group.reorder}" lay-verify="required|number" placeholder="请输入排序号" autocomplete="off" class="layui-input">
      </div>
    </div>
  </div>
  
  <div class="layui-form-item">
    <div class="layui-inline">
      <label class="layui-form-label">注册方式</label>
      	<div class="layui-input-block">
	      <input type="radio" name="addressType" lay-filter="compre-radio" value="0" title="自动注册" <c:if test="${group.addressType eq 0}">checked="checked"</c:if>>
	      <input type="radio" name="addressType" lay-filter="compre-radio" value="1" title="手动录入" <c:if test="${group.addressType eq 1}">checked="checked"</c:if>>
	    </div>
    </div>
  </div>
  
  <div class="layui-form-item">
    <div class="layui-inline">
      <label class="layui-form-label">机器地址</label>
      <div class="layui-input-inline layui-input-70">
        <input type="text" disabled="disabled" name="addressList" value="${group.addressList}" lay-verify="" autocomplete="off" placeholder="注册方式为手动录入时，才能录入。机器地址列表，以逗号分隔" class="layui-input">
      </div>
    </div>
  </div>
  
  </div>
   <!-- layui-footer layui-fixbar -->
  <div class="layui-form-item">
  	<div class="layui-inline compre-footer">
      <label class="layui-form-label"></label>
      <button class="layui-btn compre-btn" lay-submit="" lay-filter="form-submit">提交</button>
      <button type="reset" class="layui-btn compre-btn compre-btn-reset layui-btn-primary">重置</button>
    </div>
  </div>
</form>
</body>
<script>
layui.use('form', function(){
	  var form = layui.form,
	  		layer = layui.layer,
	  		$ = layui.$;
	  
	  
	  //监听提交
	  form.on('submit(form-submit)', function(data){
	  	var url = '${basePath}jobgroup/save.do';
	  	if(data.field.id != ''){
	  		url = '${basePath}jobgroup/update.do';
	  	}
	  	$.post(url,data.field,function(result){
	  		if(result.code != 200){
	  			window.top.layer.msg(result.msg);
	  		}else{
	  			window.top.layer.closeAll();
	  			window.top.layer.msg(result.msg);
	  		}
	  	});
	    return false;
	  });
	  //注册方式监听事件
	  form.on('radio(compre-radio)', function(data){
	  	if(data.value == 0){
	  		$('input[name="addressList"]').attr('disabled','disabled').val("");
	  	}else{
	  		$('input[name="addressList"]').removeAttr('disabled').attr('lay-verify','required');
	  	};
	  });
	  if('${group.addressType}' == 1){
	  	$('input[name="addressList"]').removeAttr('disabled');
	  }
});
</script>
</html>