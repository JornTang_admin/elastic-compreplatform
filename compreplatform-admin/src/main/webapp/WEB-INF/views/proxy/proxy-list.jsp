<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>layui</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <!-- 为兼容火狐此引用必须放在这里 -->
  <tag:header headerType="base"/>
</head>
<body>
	<div class="compre-search">
	 	<div class="layui-btn-group layui-btn-left compreTable">
<!-- 	    	<button class="layui-btn layui-btn-primary" data-type="addJobinfo"><i class="layui-icon"></i>添加</button> -->
	  	</div>
	  	<div class="layui-right-search">
	  		<input type="text" name="" id="proxyIp" placeholder="根据ip地址搜索" autocomplete="off" class="layui-input layui-input-right">  
	  		<button class="layui-btn layui-btn-primary layui-btn-left" id="searchTable"><i class="layui-icon">&#xe615;</i>搜索</button>
	  	</div> 
  	</div>
	<table class="layui-table" lay-data="{height: 'full-120', cellMinWidth: 50, limit:15}" lay-filter="compreTable">
  	<thead>
	    <tr>
	      <th lay-data="{type:'checkbox', fixed: 'left'}"></th>
	      <th lay-data="{field:'proxyIp'}">ip</th>
	      <th lay-data="{field:'proxyPort'}">端口</th>
	      <th lay-data="{field:'crypLevel'}">级别</th>
	      <th lay-data="{field:'proxyType'}">类型</th>
	      <th lay-data="{field:'respTime'}">响应时间/秒</th>
	      <th lay-data="{field:'cityAddress'}">地理位置</th>
	      <th lay-data="{field:'proxyFrom'}">代理来源</th>
	      <th lay-data="{field: 'verifyLastTime'}">最后验证时间</th>
	    </tr>
  </thead>
</table>
<!-- layui模板控制 --> 
<script>
layui.use('table', function(){
  var table = layui.table,
  		$ = layui.$;
  var tableOptions = {
        url: '${basePath}craIpProxy/proxyPageList.do', //请求地址
        method: 'POST', //方式
        nums: 10,
        id: 'listReload', //生成 Layui table 的标识 id，必须提供，用于后文刷新操作，笔者该处出过问题
        page: true, //是否分页
        where: { type: "all" }, //请求后端接口的条件，该处就是条件错误点，按照官方给出的代码示例，原先写成了 where: { key : { type: "all" } }，结果并不是我想的那样，如此写，key 将是后端的一个类作为参数，里面有 type 属性，如果误以为 key 是 Layui 提供的格式，那就大错特错了
        response: { //定义后端 json 格式，详细参见官方文档
            statusName: 'code', //状态字段名称
            statusCode: '200', //状态字段成功值
            msgName: 'msg', //消息字段
        }
    };
    table.init('compreTable', tableOptions);
  //条件搜索	
  $('#searchTable').on('click',function(){
  		table.reload("listReload", {
             where: {
                     proxyIp: $('#proxyIp').val()
                     
             }
        });
  });
});
</script>

</body>
</html>