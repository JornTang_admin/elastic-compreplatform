<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>layuiTitle</title>
		<meta name="renderer" content="webkit">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<!-- 为兼容火狐此引用必须放在这里 -->
		<tag:header headerType="base"/>
	</head>
	<body>
		<div class="compre-search">
		 	<div class="layui-btn-group layui-btn-left compreTable">
		    	<button class="layui-btn layui-btn-primary" data-type="add"><i class="layui-icon"></i>添加</button>
		  	</div>
		  	<div class="layui-right-search">
		  		<input type="text" name="" id="dbDesc" required lay-verify="required" placeholder="根据描述搜索" autocomplete="off" class="layui-input layui-input-right">  
		  		<button class="layui-btn layui-btn-primary layui-btn-left" id="searchTable"><i class="layui-icon">&#xe615;</i>搜索</button>
		  	</div> 
	  	</div>
		<table class="layui-table" lay-data="{height: 'full-120', cellMinWidth: 50, limit:15}" lay-filter="compreTable">
	  	<thead>
		    <tr>
		      	<th lay-data="{type:'checkbox', fixed: 'left'}"></th>
			<th lay-data="{field:'busiId'}">业务ID</th>
			<th lay-data="{field:'fileFnam'}">附件全局唯一名称（UUID+后缀）</th>
			<th lay-data="{field:'fileFprimalnam'}">附件原名称</th>
			<th lay-data="{field:'fileType'}">附件类型</th>
			<th lay-data="{field:'fileFormat'}">附件后缀</th>
			<th lay-data="{field:'fileSize'}">附件大小</th>
			<th lay-data="{field:'filePath'}">存储路径</th>
			<th lay-data="{field:'status'}">状态</th>
			<th lay-data="{field:'createUser'}">上传者</th>
			<th lay-data="{field:'createTime'}">上传时间</th>
			<th lay-data="{field:'updateTime'}">修改时间</th>
		      	<th lay-data="{fixed: 'right', width:178, align:'center', toolbar: '#barTable'}">操作</th>
		    </tr>
	  </thead>
	</table>
	
	<!-- 操作绑定 -->
	<script type="text/html" id="barTable">
  		<a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
  		<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
	</script>
	               
	<script>
		layui.use('table', function(){
			var table = layui.table,
		  		$ = layui.$;
		  	var tableOptions = {
		        url: '${basePath}sysFile/pageList', //请求地址
		        method: 'POST', //方式
		        id: 'listReload', //生成 Layui table 的标识 id，必须提供，用于后文刷新操作，笔者该处出过问题
		        page: true, //是否分页
		        where: { type: "all" }, //请求后端接口的条件，该处就是条件错误点，按照官方给出的代码示例，原先写成了 where: { key : { type: "all" } }，结果并不是我想的那样，如此写，key 将是后端的一个类作为参数，里面有 type 属性，如果误以为 key 是 Layui 提供的格式，那就大错特错了
		        response: { //定义后端 json 格式，详细参见官方文档
		            statusName: 'code', //状态字段名称
		            statusCode: '200', //状态字段成功值
		            msgName: 'msg', //消息字段
		        }
		    };
		    table.init('compreTable', tableOptions);
		  	//条件搜索	
		  	$('#searchTable').on('click',function(){
		  		table.reload("listReload", {
		             where: {
		            	 //条件放置
		             }
		        });
		  	});
		  	//监听表格复选框选择
		  	table.on('checkbox(compreTable)', function(obj){
		    	console.log(obj)
		  	});
		  	//监听工具条
		  	table.on('tool(compreTable)', function(obj){
			    var data = obj.data;
			    if(obj.event === 'del'){
				    top.layer.confirm('确定删除?', function(index){
				      	$.post('${basePath}sysFile/delete',{id: data.id},function(result){
				      		top.layer.msg(result.msg);
				      		table.reload('listReload');
				        	top.layer.close(index);
				      	});
				    });
			    } else if(obj.event === 'edit'){
			      	window.top.$.layerFrame({callBack:function(){table.reload('listReload');},content: ['${basePath}sysFile/toUpdate?id=' + data.id,'no'],title: '修改',});
			    }
		  	});
		  	
		  	//操作按钮事件绑定
		  	var $ = layui.$, active = {
	  			add: function(opt){
			  		window.top.$.layerFrame({callBack:function(){table.reload('listReload');},content: ['${basePath}sysFile/toAdd','no'],title: '添加',});
			  	}
			};
		  
		  	$('.compreTable .layui-btn').on('click', function(){
		    	var type = $(this).data('type');
		    	active[type] ? active[type].call(this) : '';
		  	});
		});
	</script>
	</body>
</html>