<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags"%>
<!DOCTYPE html>
<html>
	<head>
	  	<meta charset="utf-8">
	  	<title>layuiTile</title>
	  	<meta name="renderer" content="webkit">
	  	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	  	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	  	<!-- 为兼容火狐此引用必须放在这里 -->
	 	<tag:header headerType="base"/>
	</head>
	<body>  
		<form class="layui-form" action="" method='POST' style="width:100%;">
				<input type="hidden" name="id" value="${entity.id}"/>
		  	<div class="compre-content">
			<div class="layui-form-item">
		    	<div class="layui-inline">
			      	<label class="layui-form-label"><i class='layui-must'>*</i>业务ID
			      	</label>
			      	<div class="layui-input-inline layui-input-70">
			        	<input type="text" name="busiId" value="${entity.busiId}" lay-verify="required" autocomplete="off" placeholder="请输入业务ID" class="layui-input">
			      	</div>
		    	</div>
		  	</div>
			<div class="layui-form-item">
		    	<div class="layui-inline">
			      	<label class="layui-form-label"><i class='layui-must'>*</i>附件全局唯一名称（UUID+后缀）
			      	</label>
			      	<div class="layui-input-inline layui-input-70">
			        	<input type="text" name="fileFnam" value="${entity.fileFnam}" lay-verify="required" autocomplete="off" placeholder="请输入附件全局唯一名称（UUID+后缀）" class="layui-input">
			      	</div>
		    	</div>
		  	</div>
			<div class="layui-form-item">
		    	<div class="layui-inline">
			      	<label class="layui-form-label"><i class='layui-must'>*</i>附件原名称
			      	</label>
			      	<div class="layui-input-inline layui-input-70">
			        	<input type="text" name="fileFprimalnam" value="${entity.fileFprimalnam}" lay-verify="required" autocomplete="off" placeholder="请输入附件原名称" class="layui-input">
			      	</div>
		    	</div>
		  	</div>
			<div class="layui-form-item">
		    	<div class="layui-inline">
			      	<label class="layui-form-label">附件类型
			      	</label>
			      	<div class="layui-input-inline layui-input-70">
			        	<input type="text" name="fileType" value="${entity.fileType}" lay-verify="" autocomplete="off" placeholder="请输入附件类型" class="layui-input">
			      	</div>
		    	</div>
		  	</div>
			<div class="layui-form-item">
		    	<div class="layui-inline">
			      	<label class="layui-form-label"><i class='layui-must'>*</i>附件后缀
			      	</label>
			      	<div class="layui-input-inline layui-input-70">
			        	<input type="text" name="fileFormat" value="${entity.fileFormat}" lay-verify="required" autocomplete="off" placeholder="请输入附件后缀" class="layui-input">
			      	</div>
		    	</div>
		  	</div>
			<div class="layui-form-item">
		    	<div class="layui-inline">
			      	<label class="layui-form-label"><i class='layui-must'>*</i>附件大小
			      	</label>
			      	<div class="layui-input-inline layui-input-70">
			        	<input type="text" name="fileSize" value="${entity.fileSize}" lay-verify="required" autocomplete="off" placeholder="请输入附件大小" class="layui-input">
			      	</div>
		    	</div>
		  	</div>
			<div class="layui-form-item">
		    	<div class="layui-inline">
			      	<label class="layui-form-label">存储路径
			      	</label>
			      	<div class="layui-input-inline layui-input-70">
			        	<input type="text" name="filePath" value="${entity.filePath}" lay-verify="" autocomplete="off" placeholder="请输入存储路径" class="layui-input">
			      	</div>
		    	</div>
		  	</div>
			<div class="layui-form-item">
		    	<div class="layui-inline">
			      	<label class="layui-form-label"><i class='layui-must'>*</i>状态
			      	</label>
			      	<div class="layui-input-inline layui-input-70">
			        	<input type="text" name="status" value="${entity.status}" lay-verify="required" autocomplete="off" placeholder="请输入状态" class="layui-input">
			      	</div>
		    	</div>
		  	</div>
			<div class="layui-form-item">
		    	<div class="layui-inline">
			      	<label class="layui-form-label"><i class='layui-must'>*</i>上传者
			      	</label>
			      	<div class="layui-input-inline layui-input-70">
			        	<input type="text" name="createUser" value="${entity.createUser}" lay-verify="required" autocomplete="off" placeholder="请输入上传者" class="layui-input">
			      	</div>
		    	</div>
		  	</div>
			<div class="layui-form-item">
		    	<div class="layui-inline">
			      	<label class="layui-form-label"><i class='layui-must'>*</i>上传时间
			      	</label>
			      	<div class="layui-input-inline layui-input-70">
			        	<input type="text" name="createTime" value="${entity.createTime}" lay-verify="required" autocomplete="off" placeholder="请输入上传时间" class="layui-input">
			      	</div>
		    	</div>
		  	</div>
			<div class="layui-form-item">
		    	<div class="layui-inline">
			      	<label class="layui-form-label">修改时间
			      	</label>
			      	<div class="layui-input-inline layui-input-70">
			        	<input type="text" name="updateTime" value="${entity.updateTime}" lay-verify="" autocomplete="off" placeholder="请输入修改时间" class="layui-input">
			      	</div>
		    	</div>
		  	</div>
		  	</div>
		   	<!-- layui-footer layui-fixbar -->
		  	<div class="layui-form-item">
		  		<div class="layui-inline compre-footer">
			      	<label class="layui-form-label"></label>
			      	<button class="layui-btn compre-btn" lay-submit="" lay-filter="form-submit">提交</button>
			      	<button type="reset" class="layui-btn compre-btn compre-btn-reset layui-btn-primary">重置</button>
			    </div>
		  	</div>
		</form>
	</body>
	<script>
	layui.use('form', function(){
		  var form = layui.form,
		  		layer = layui.layer,
		  		$ = layui.$;
		  //监听提交
		  form.on('submit(form-submit)', function(data){
		  	$.post('${basePath}sysFile/update',data.field,function(result){
		  		if(result.code != 200){
		  			top.layer.msg(result.msg);
		  		}else{
		  			top.layer.closeAll();
		  			top.layer.msg(result.msg);
		  		}
		  	});
		    return false;
		  });
	});
	</script>
</html>