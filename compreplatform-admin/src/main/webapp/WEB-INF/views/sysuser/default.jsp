<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <title>综合支撑管理平台</title>
  <!-- 为兼容火狐此引用必须放在这里 -->
  <tag:header headerType="base"/>
  <tag:header headerType="util"/>
  <style type="text/css">
  	.layui-nav-child >dd >a {
  		color: #000;
  	}
  </style>
</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">
  <div class="layui-header" style="background-color: #145CCD;">
    <div class="layui-logo"><h3 style="color:#fff;">综合支撑管理平台</h3></div>
    <ul class="layui-nav layui-layout-right">
      <li class="layui-nav-item"><a href="javascript:void(0);">在线用户：${userTotal}</a></li>
      <li class="layui-nav-item">
        <a href="javascript:;">
          <img src="${basePath}/resource/common/ico/compre.ico" class="layui-nav-img">
          ${asd}
        </a>
        <dl class="layui-nav-child">
          <dd><a href="" style="color: #000;">基本资料</a></dd>
          <dd><a href="" style="color: #000;">安全设置</a></dd>
        </dl>
      </li>
      <li class="layui-nav-item"><a href="javascript:void(0);" id="loginout">注销</a></li>
    </ul>
  </div>
  
  <div class="layui-side layui-bg-black">
    <div class="layui-side-scroll" style="background-color: #1454b7;">
      <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
      <ul class="layui-nav layui-nav-tree" lay-filter="compre-left-nav" style="background-color: #1454b7;">
      	<li class="layui-nav-item">
          <a href="javascript:;">公共服务</a>
          <dl class="layui-nav-child">
            <dd><a href="javascript:;">短信管理</a></dd>
            <dd><a href="javascript:;">邮件管理</a></dd>
            <dd><a href="javascript:;">文件管理</a></dd>
            <dd><a href="javascript:;">字典管理</a></dd>
          </dl>
        </li>
        <li class="layui-nav-item layui-nav-itemed">
          <a class="" href="javascript:;"><span class="layui-icon" style="margin-right: 5px;">&#xe857;</span>配置中心</a>
          <dl class="layui-nav-child">
            <dd><a href="javascript:void(0);" class="menuTab" data-id="15" data-url="${basePath}zookProject/toList"><span class="layui-icon" style="margin-right: 5px;">&#xe60f;</span>项目管理</a></dd>
          	<dd><a href="javascript:void(0);" class="menuTab" data-id="14" data-url="${basePath}zookNode/toList"><span class="layui-icon" style="margin-right: 5px;">&#xe641;</span>节点操作</a></dd>
          </dl>
        </li>
        <li class="layui-nav-item">
          <a href="javascript:;"><span class="layui-icon" style="margin-right: 5px;"></span>任务调度</a>
          <dl class="layui-nav-child">
            <dd><a href="javascript:void(0);" class="menuTab" data-id="1" data-url="${basePath}jobregistry/index.do"><span class="layui-icon" style="margin-right: 5px;">&#xe642;</span>客户端注册管理</a></dd>
            <dd><a href="javascript:void(0);" class="menuTab" data-id="4" data-url="${basePath}jobgroup/index.do"><span class="layui-icon" style="margin-right: 5px;">&#xe633;</span>执行器管理</a></dd>
            <dd><a href="javascript:void(0);" class="menuTab" data-id="5" data-url="${basePath}jobinfo/index.do"><span class="layui-icon" style="margin-right: 5px;">&#xe608;</span>任务管理</a></dd>
            <dd><a href="javascript:void(0);" class="menuTab" data-id="6" data-url="${basePath}joblog/index.do"><span class="layui-icon" style="margin-right: 5px;">&#xe637;</span>调度日志</a></dd>
            <dd><a href="javascript:void(0);" class="menuTab" data-id="7" data-url="${basePath}joblog/toTriggerChartDate.do"><span class="layui-icon" style="margin-right: 5px;">&#xe62c;</span>运行报表</a></dd>
          </dl>
        </li>
        <li class="layui-nav-item">
          <a href="javascript:;"><span class="layui-icon" style="margin-right: 5px;">&#xe628;</span>Redis管理</a>
          <dl class="layui-nav-child">
            <dd><a href="javascript:void(0);" class="menuTab" data-id="10" data-url="${basePath}redis/toList"><span class="layui-icon" style="margin-right: 5px;">&#xe631;</span>Redis配置</a></dd>
            <dd><a href="javascript:void(0);" class="menuTab" data-id="12" data-url="${basePath}redis/toCache"><span class="layui-icon" style="margin-right: 5px;">&#xe857;</span>缓存操作</a></dd>
            <dd><a href="javascript:void(0);" class="menuTab" data-id="11" data-url="${basePath}redis/toStat"><span class="layui-icon" style="margin-right: 5px;">&#xe629;</span>统计信息</a></dd>
            <dd><a href="javascript:void(0);" class="menuTab" data-id="13" data-url="${basePath}redis/toCacheChart"><span class="layui-icon" style="margin-right: 5px;">&#xe62c;</span>实时监控</a></dd>
<%--            <dd><a href="javascript:void(0);" class="menuTab" data-id="14" data-url="${basePath}redis/toBackupDatabase"><span class="layui-icon" style="margin-right: 5px;">&#xe624;</span>备份/还原</a></dd> --%>
          </dl>
        </li>
        
        <li class="layui-nav-item"><a href="${basePath}druid/" target="_bank"><span class="layui-icon" style="margin-right: 5px;">&#xe62c;</span>连接池监控</a></li>
        <li class="layui-nav-item">
          <a href="javascript:;"><span class="layui-icon" style="margin-right: 5px;">&#xe64e;</span>代码生成工具</a>
          <dl class="layui-nav-child">
            <dd><a href="javascript:void(0);" class="menuTab" data-id="2" data-url="${basePath}codegener/toCodegenerDbconf.do"><span class="layui-icon" style="margin-right: 5px;">&#xe631;</span>数据库配置</a></dd>
            <dd><a href="javascript:void(0);" class="menuTab" data-id="3" data-url="${basePath}codegener/toCodeBuild.do"><span class="layui-icon" style="margin-right: 5px;">&#xe635;</span>代码生成</a></dd>
          </dl>
        </li>
        
        <li class="layui-nav-item">
          <a href="javascript:;"><span class="layui-icon" style="margin-right: 5px;">&#xe609;</span>爬虫管理</a>
          <dl class="layui-nav-child">
            <dd><a href="javascript:void(0);" class="menuTab" data-id="9" data-url="${basePath}craIpProxy/toProxyList.do"><span class="layui-icon" style="margin-right: 5px;">&#xe644;</span>IP代理列表</a></dd>
          </dl>
        </li>
      </ul>
    </div>
  </div>
  
  <div class="layui-body" style="overflow-y:hidden;">
    <!-- 内容主体区域 -->
    <div class="layui-tab layui-tab-card layui-tab" lay-filter="compre-tab" lay-allowclose="true" style="margin:0px;">
	  <ul class="layui-tab-title layui-compre-tab-title">
	  </ul>
	  <div class="layui-tab-content" id="compre-tab-content">
	  </div>
	</div>
  </div>
  
  <div class="layui-footer">
    <!-- 底部固定区域 -->
    © JornTang 作者全权拥有 - 本项目免费开源不得以商业为目的，不然追究法律责任
  </div>
</div>
<script>
	function changeFrameHeight(iframeId){
        var iframe= document.getElementById(iframeId);
        var div_content = document.getElementById('compre-tab-content');
		iframe.height = div_content.offsetHeight;
    }
    window.onresize=function(){ changeFrameHeight();}
    $(function(){changeFrameHeight();});
	//JavaScript代码区域
	layui.use(['layer','jquery','element'], function(){
		var $ = layui.jquery,
      		element = layui.element,
      		layer = layui.layer;
		element.on('tab(compre-tab)', function(elem){
			//alert('tab');
		});
      	//首页
      	var contentHtml = '<h1>欢迎使用elastic-compreplatform</h1>' +
      					'<h3>开源地址：<a href="https://gitee.com/JornTang/elastic-compreplatform">https://gitee.com/JornTang/elastic-compreplatform<a/></h3></br>' + 
      					'<h3>QQ群：708970773</h3></br>' + 
      					'<h3>关于作者</h3>' +
      					'<h3>英文名： JornTang</h3>' +
      					'<h3>电子邮箱： 957707261@qq.com</h3>';
      	element.tabAdd('compre-tab', {title: '<span class="layui-icon" style="margin-right: 5px;font-size: 14px;">&#xe68e;</span>首页',content: contentHtml,id: -1});
	  	$('li[lay-id="-1"]').trigger('click');
	  	$('li[lay-id="-1"]').find('i').remove();
	  	$("#loginout").on("click", function(){
	  		//询问框
			layer.confirm(
				"确定注销?",
				{
					btn : [ "确定", "取消" ], //按钮
					title : "提示信息框",
					area : [ '300px', '150px' ],
				},
				function() {
					window.location.href="${basePath}user/loginout.do";
				}, function() {
			});
	  	});
	  	//动态tab增、删、切换
	  	$("a.menuTab").on('click',function(){
	  		var title = $(this).html();
	  		var id = $(this).attr('data-id');
	  		var url = $(this).attr('data-url');
	  		var content = '<iframe id="iframe-'+id+'" data-frame-id="'+id+'" src="' + url + '" width="100%" height="550px" scrolling="no" frameborder="no"/>';
	      	//新增一个Tab项 判断是否已经存在，存在则切换
	      	var ulTabli = $("ul.layui-tab-title").find("li");
	      	if(ulTabli.length> 0){
	      		var ifExits = false;
	      		ulTabli.each(function(){
		      		var layId = $(this).attr('lay-id');
		      		if(layId == id){
		      			ifExits = true;
		      		}
		      	});
		      	if(ifExits){
		      		element.tabChange('compre-tab', id); //切换tab
		      	}else{
		      		element.tabAdd('compre-tab', {title: title,content: content,id: id});
		      		element.tabChange('compre-tab', id); //切换tab
		      	}
	      	}else{
	      		element.tabAdd('compre-tab', {title: title,content: content,id: id});
		      	element.tabChange('compre-tab', id); //切换tab
	      	}
	      	changeFrameHeight('iframe-'+id);
	  	});
	  	
	});
</script>
</body>
</html>