<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags"%>
<!DOCTYPE html>
<html>
	<head>
	  	<meta charset="utf-8">
	  	<title>layuiTile</title>
	  	<meta name="renderer" content="webkit">
	  	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	  	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	  	<!-- 为兼容火狐此引用必须放在这里 -->
	 	<tag:header headerType="base"/>
	</head>
	<body>  
		<form class="layui-form" action="" method='POST' style="width:100%;">
		  	<div class="compre-content">
			<div class="layui-form-item">
		    	<div class="layui-inline">
			      	<label class="layui-form-label">描述
			      	</label>
			      	<div class="layui-input-inline layui-input-70">
			        	<input type="text" name="dbDesc" value="${entity.dbDesc}" lay-verify="required" autocomplete="off" placeholder="请输入描述" readonly="readonly" class="layui-input">
			      	</div>
		    	</div>
		  	</div>
			<div class="layui-form-item">
		    	<div class="layui-inline">
			      	<label class="layui-form-label">数据库类型
			      	</label>
			      	<div class="layui-input-inline layui-input-70">
			        	<input type="text" name="dbType" value="${entity.dbType}" lay-verify="required" autocomplete="off" placeholder="请输入数据库类型" readonly="readonly" class="layui-input">
			      	</div>
		    	</div>
		  	</div>
			<div class="layui-form-item">
		    	<div class="layui-inline">
			      	<label class="layui-form-label">ip地址
			      	</label>
			      	<div class="layui-input-inline layui-input-70">
			        	<input type="text" name="dbUrl" value="${entity.dbUrl}" lay-verify="required" autocomplete="off" placeholder="请输入ip地址" readonly="readonly" class="layui-input">
			      	</div>
		    	</div>
		  	</div>
			<div class="layui-form-item">
		    	<div class="layui-inline">
			      	<label class="layui-form-label">oracle需要配置scheme
			      	</label>
			      	<div class="layui-input-inline layui-input-70">
			        	<input type="text" name="dbSchema" value="${entity.dbSchema}" lay-verify="" autocomplete="off" placeholder="请输入oracle需要配置scheme" readonly="readonly" class="layui-input">
			      	</div>
		    	</div>
		  	</div>
			<div class="layui-form-item">
		    	<div class="layui-inline">
			      	<label class="layui-form-label">用户名
			      	</label>
			      	<div class="layui-input-inline layui-input-70">
			        	<input type="text" name="username" value="${entity.username}" lay-verify="required" autocomplete="off" placeholder="请输入用户名" readonly="readonly" class="layui-input">
			      	</div>
		    	</div>
		  	</div>
			<div class="layui-form-item">
		    	<div class="layui-inline">
			      	<label class="layui-form-label">密码
			      	</label>
			      	<div class="layui-input-inline layui-input-70">
			        	<input type="text" name="password" value="${entity.password}" lay-verify="required" autocomplete="off" placeholder="请输入密码" readonly="readonly" class="layui-input">
			      	</div>
		    	</div>
		  	</div>
			<div class="layui-form-item">
		    	<div class="layui-inline">
			      	<label class="layui-form-label">数据库驱动
			      	</label>
			      	<div class="layui-input-inline layui-input-70">
			        	<input type="text" name="dbDriver" value="${entity.dbDriver}" lay-verify="required" autocomplete="off" placeholder="请输入数据库驱动" readonly="readonly" class="layui-input">
			      	</div>
		    	</div>
		  	</div>
		  	</div>
		</form>
	</body>
	<script>
	layui.use('form', function(){
		  var form = layui.form,
		  		layer = layui.layer,
		  		$ = layui.$;
	});
	</script>
</html>