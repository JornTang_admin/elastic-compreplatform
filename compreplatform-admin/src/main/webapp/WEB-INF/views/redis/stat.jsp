<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>layuiTitle</title>
		<meta name="renderer" content="webkit">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<!-- 为兼容火狐此引用必须放在这里 -->
		<tag:header headerType="base"/>
	</head>
	<body>
		<div class="compre-search">
		 	<div class="layui-btn-group layui-btn-left compreTable">
		    	<button class="layui-btn layui-btn-primary" data-type="clearInfo"><i class="layui-icon"></i>重置统计信息</button>
		  	</div>
		  	<div class="layui-right-search">
		  		<form class="layui-form layui-form-left" action="">
				    <select name="dbId" id="dbId" lay-search="">
				      <c:forEach items="${list}" var="ls">
				      	<option value="${ls.id}">${ls.dbDesc}</option>
				      </c:forEach>
				    </select>
			    </form>
		  		<button class="layui-btn layui-btn-primary layui-btn-left" id="searchTable"><i class="layui-icon">&#xe615;</i>搜索</button>
		  	</div> 
	  	</div>
		<table class="layui-table" lay-data="{height: 'full-120', cellMinWidth: 50, limit:74}" lay-filter="compreTable">
	  	<thead>
		    <tr>
				<th lay-data="{field:'key'}">属性</th>
				<th lay-data="{field:'value'}">值</th>
				<th lay-data="{field:'content'}">说明</th>
		    </tr>
	  </thead>
	</table>
	
	<script>
		layui.use('table', function(){
			var table = layui.table,
		  		$ = layui.$;
		  	var tableOptions = {
		        url: '${basePath}redis/stat?dbId=' + $('#dbId').find('option:first').val(), //请求地址
		        method: 'POST', //方式
		        id: 'listReload', //生成 Layui table 的标识 id，必须提供，用于后文刷新操作，笔者该处出过问题
		        page: true, //是否分页
		        where: { type: "all" }, //请求后端接口的条件，该处就是条件错误点，按照官方给出的代码示例，原先写成了 where: { key : { type: "all" } }，结果并不是我想的那样，如此写，key 将是后端的一个类作为参数，里面有 type 属性，如果误以为 key 是 Layui 提供的格式，那就大错特错了
		        response: { //定义后端 json 格式，详细参见官方文档
		            statusName: 'code', //状态字段名称
		            statusCode: '200', //状态字段成功值
		            msgName: 'msg', //消息字段
		        }
		    };
		    table.init('compreTable', tableOptions);
		  	//条件搜索	
		  	$('#searchTable').on('click',function(){
		  		table.reload("listReload", {
		             where: {
		            	 //条件放置
		            	 dbId: $('#dbId').find('option:first').val()
		             }
		        });
		  	});
		  //操作按钮事件绑定
		  	var $ = layui.$, active = {
			  	clearInfo: function(opt){
			  		var dbId = $('#dbId').find('option:selected').val();
			  		top.layer.confirm('确定重置统计信息?', function(index){
				      	$.post('${basePath}redis/clearInfo', {dbId: dbId}, function(result){
				      		
				      	});
				      	table.reload('listReload');
			        	top.layer.closeAll();
				    });
			  	}
			};
		  
		  	$('.compreTable .layui-btn').on('click', function(){
		    	var type = $(this).data('type');
		    	active[type] ? active[type].call(this) : '';
		  	});
		});
	</script>
	</body>
</html>