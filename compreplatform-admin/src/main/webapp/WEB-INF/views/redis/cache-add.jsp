<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags"%>
<!DOCTYPE html>
<html>
	<head>
	  	<meta charset="utf-8">
	  	<title>layuiTile</title>
	  	<meta name="renderer" content="webkit">
	  	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	  	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	  	<!-- 为兼容火狐此引用必须放在这里 -->
	 	<tag:header headerType="base"/>
	</head>
	<body>  
		<form class="layui-form" action="" method='POST' style="width:100%;">
			<input type="hidden" name="dbId" value="${cache.dbId}"/>
			<input type="hidden" name="dbIndex" value="${cache.dbIndex}"/>
		  	<div class="compre-content">
			<div class="layui-form-item">
		    	<div class="layui-inline">
			      	<label class="layui-form-label"><i class='layui-must'>*</i>key
			      	</label>
			      	<div class="layui-input-inline layui-input-70">
			        	<input type="text" name="key" value="" lay-verify="required" autocomplete="off" placeholder="请输入key" class="layui-input">
			      	</div>
		    	</div>
		  	</div>
			<div class="layui-form-item">
		    	<div class="layui-inline">
			      	<label class="layui-form-label"><i class='layui-must'>*</i>过期时间
			      	</label>
			      	<div class="layui-input-inline layui-input-70">
			        	<input type="text" name="expire" value="-1" lay-verify="required" autocomplete="off" placeholder="请输入过期时间" class="layui-input">
			      	</div>
		    	</div>
		  	</div>
			<div class="layui-form-item">
		    	<div class="layui-inline">
			      	<label class="layui-form-label"><i class='layui-must'>*</i>type
			      	</label>
			      	<div class="layui-input-inline layui-input-70">
					    <select name="keyType" lay-search="" lay-filter="keyType">
					    	 <option value="string">string</option>  
							 <option value="set">set</option>
							 <option value="list">list</option>  
							 <option value="zset">zset</option>
							 <option value="hash">hash</option>
							 <option value="HashSet">HashSet</option>
							 <option value="ArrayList">ArrayList</option>
					    </select>
			      	</div>
		    	</div>
		  	</div>
		  	<div class="layui-form-item">
		    	<div class="layui-inline">
			      	<label class="layui-form-label"><i class='layui-must'>*</i>value
			      	</label>
			      	<div class="layui-input-inline layui-input-70" id="div-value">
			      		<textarea name="keyValue" lay-verify="required" placeholder="请输入value" class="layui-textarea"></textarea>
			      	</div>
		    	</div>
		  	</div>
		  	</div>
		   	<!-- layui-footer layui-fixbar -->
		  	<div class="layui-form-item">
		  		<div class="layui-inline compre-footer">
			      	<label class="layui-form-label"></label>
			      	<button class="layui-btn compre-btn" lay-submit="" lay-filter="form-submit">提交</button>
			      	<button type="reset" class="layui-btn compre-btn compre-btn-reset layui-btn-primary">重置</button>
			    </div>
		  	</div>
		</form>
	</body>
	<script>
	layui.use('form', function(){
		  var form = layui.form,
		  		layer = layui.layer,
		  		$ = layui.$;
		  //监听下拉列表change事件
		  form.on('select(keyType)', function(data){
		  	  window.keyType = data.value;
		  	  if(keyType == 'string'){
		  		  $('#div-value').html('<textarea name="keyValue" lay-verify="required" placeholder="请输入value" class="layui-textarea"></textarea>');
		  	  }else if(keyType == 'zset' || keyType == 'hash'){
		  		  var html = buildZsetHtml();
			  	  $('#div-value').html(html);
			  	  //绑定添加或删除事件
		  		  bindEvent();
		  	  }else{
		  		  var html = buildListHtml();
		  		  $('#div-value').html(html);
		  		  //绑定添加或删除事件
		  		  bindEvent();
		  	  }
		  });
		  //绑定添加或删除事件
		  function bindEvent(){
			  //解绑添加点击事件
			  $('span.zset-span-add').unbind();
			  $('span.zset-span-add').on('click', function(){
				  var html = "";
				  if(window.keyType == 'zset' || window.keyType == 'hash'){
					  html = buildZsetHtml();
				  }else {
					  html = buildListHtml();
				  }
			  	  $('#div-value').append(html);
			  	  //绑定添加或删除事件
		  		  bindEvent();
			  });
			  
			  //解绑删除点击事件
			  $('span.zset-span-del').unbind();
			  $('span.zset-span-del').on('click', function(){
				  if($('span.zset-span-del').length> 1){
					  $(this).parent().remove();
				  }else {
					  top.layer.msg('至少保留一个，禁止删除');
				  }
			  });
		  }
		  function buildZsetHtml(){
			  var zsetHtml = '<span>' +
						  		'<input type=text name="valuek" value="" lay-verify="required" autocomplete="off" placeholder="请输入' + (window.keyType == 'zset'?'member': 'field') + '" class="layui-input" style="display: inline;width: 45%;">' +
						  		'<input type=text name="valuev" value="" lay-verify="required' + (window.keyType == 'zset'?'|number': '') + '" autocomplete="off" placeholder="请输入' + (window.keyType == 'zset'?'score': 'value') + '" class="layui-input" style="display: inline;width: 45%;">' +
								'<span class="layui-icon zset-span-add" title="添加" style="font-size:18px;margin-left: 5px;">&#xe654;</span>' +
					      		'<span class="layui-icon zset-span-del" title="删除" style="color: red;font-size:18px;margin-left: 5px;">&#xe640;</span>' +
							'</span>';
			  return zsetHtml;
		  }
		  function buildListHtml(){
			  var listHtml = '<span>' +
						  		'<input type=text name="valuek" value="" lay-verify="required" autocomplete="off" placeholder="请输入value" class="layui-input" style="display: inline;width: 90%;">' +
								'<span class="layui-icon zset-span-add" title="添加" style="font-size:18px;margin-left: 5px;">&#xe654;</span>' +
					      		'<span class="layui-icon zset-span-del" title="删除" style="color: red;font-size:18px;margin-left: 5px;">&#xe640;</span>' +
							'</span>';
			  return listHtml;
		  }
		  //监听提交
		  form.on('submit(form-submit)', function(data){
			var params = data.field;
			//构造参数
			var buildParam = "";
			if(window.keyType != 'string'){
				var valueks = new Array(),valuevs= new Array();
			  	$('input[name="valuek"]').each(function(){
				  	var v = $(this).val();
				  	valueks.push('valuek' + '=' + v);
			  	});
			  	$('input[name="valuev"]').each(function(){
				  	var v = $(this).val();
				  	valuevs.push('valuev' + '=' + v);
			  	});
			  	buildParam = valueks.join('&') + '&' + valuevs.join('&');
			  	delete params["valuek"];
				delete params["valuev"];
			  }
		  	$.post('${basePath}redis/cacheAdd?' + buildParam, params, function(result){
		  		if(result.code != 200){
		  			top.layer.msg(result.msg);
		  		}else{
		  			top.layer.closeAll();
		  			top.layer.msg(result.msg);
		  		}
		  	});
		    return false;
		  });
		  //测试连接
		  $('#checkConnection').on('click', function(){
			  var dbUrl = $('input[name="dbUrl"]').val();
			  if(dbUrl == ''){
				  top.layer.msg('请输入ip地址');
				  return;
			  }
			  var dbPort = $('input[name="dbPort"]').val();
			  if(dbPort == ''){
				  top.layer.msg('请输入端口');
				  return;
			  }
			  var password = $('input[name="password"]').val();
			  var index = top.layer.load(1, {
			  	  shade: [0.3,'#fff']
			  });
			  $.post('${basePath}/redis/checkConnection',{dbUrl: dbUrl, dbPort:dbPort, password: password},function(result){
				  top.layer.close(index);
				  if(result.code != 200){
			  	  	  top.layer.msg("连接失败");
			  	  }else{
			  	  	  top.layer.msg("连接成功");
			  	  }
			  },'json');			  
		  })
	});
	</script>
</html>