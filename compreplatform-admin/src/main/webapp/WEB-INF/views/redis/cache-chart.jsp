<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags"%>
<!DOCTYPE html>
<html style="height:80%;overflow-y: auto;">
<head>
  <meta charset="utf-8">
  <title>${headTitle}</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <!-- 为兼容火狐此引用必须放在这里 -->
 <tag:header headerType="base"/>
 <tag:header headerType="echarts"/>
 <style type="text/css">
 	.float-e-margins{
 		padding-left: 10px;
 	}
 	.col-sm-3{
 		border-right: solid 3px;
 	}
 </style>
</head>
<body style="height:500px;overflow-y: auto;overflow-x: hidden;">
	<div class="row" style="background-color: #333;border-bottom: solid 4px #FFF;margin-bottom: 10px;height:50px;padding: 0px;">
		<div class="col-sm-3" style="margin-top: 5px;margin-left: 10px;">
			<div class="layui-left-search">
		  		<form class="layui-form" style="width: 100%;">
				    <select name="dbId" id="dbId" lay-search="" lay-filter="dbId">
				    	<c:forEach items="${list}" var="ls">
				    		<option value="${ls.id}">${ls.dbDesc}</option>
				    	</c:forEach>
				    </select>
			    </form>
		  	</div> 
        </div>
        <div class="col-sm-3" style="margin-top: 5px;padding: 0px;">
			<div class="layui-left-search">
		  		<form class="layui-form" style="width: 100%;">
				    <select name="intervalTime" id="intervalTime" lay-filter="intervalTime">
				    	<option value="">设置自动刷新，默认10秒</option>
				    	<option value="5">5秒</option>
				    	<option value="10">10秒</option>
				    	<option value="30">30秒</option>
				    </select>
			    </form>
		  	</div> 
        </div>
	</div>
	        
	<div style="background: #333;color:#fff;margin-bottom: 10px;height:80px;">
	    <div class="row">
	    	<div class="col-sm-3">
	            <div class="ibox float-e-margins">
	                <div class="ibox-title">
	                	<h5>used_memory</h5>
	                	<span class="lable-span pull-right" style="margin-top: -30px;">分配的内存</span>
	                </div>
	                <div class="ibox-content" style="line-height: 1;">
	                	<i class="layui-icon" style="font-size: 35px;"></i>
	                	<span class="pull-right">
		                    <h2 class="no-margins" style="margin-top: 0px;" id="used_memory">0</h2>
	                    </span>
	                </div>
	            </div>
	        </div>
	        <div class="col-sm-3">
	            <div class="ibox float-e-margins">
	                <div class="ibox-title">
	                	<h5>total_keys </h5>
	                	<span class="lable-span pull-right" style="margin-top: -30px;">缓存KEY总数</span>
	                </div>
	                <div class="ibox-content" style="line-height: 1;">
	                	<i class="layui-icon" style="font-size: 35px;">&#xe637;</i>
	                	<span class="pull-right">
		                    <h2 class="no-margins" style="margin-top: 0px;" id="totalkeys">0</h2>
	                    </span>
	                </div>
	            </div>
	        </div>
	        <div class="col-sm-3">
	            <div class="ibox float-e-margins">
	                <div class="ibox-title">
	                    <h5>clients</h5>
	                    <span class="lable-span pull-right" style="margin-top: -30px;">当前服务器已连接客户端</span>
	                </div>
	                <div class="ibox-content" style="line-height: 1;">
	                	<i class="layui-icon" style="font-size: 35px;">&#xe62c;</i>
	                	<span class="pull-right">
		                    <h2 class="no-margins" style="margin-top: 0px;" id="clients">0</h2>
	                    </span>
	                </div>
	            </div>
	        </div>
	        <div class="col-sm-3">
	            <div class="ibox float-e-margins">
	                <div class="ibox-title">
	                    <h5>commands_processed</h5>
	                    <span class="lable-span pull-right" style="margin-top: -30px;">已执行命令总数</span>
	                </div>
	                <div class="ibox-content" style="line-height: 1;">
	                	<i class="layui-icon" style="font-size: 35px;">&#xe652;</i>
	                	<span class="pull-right">
		                    <h2 class="no-margins" style="margin-top: 0px;" id="commands_processed">0</h2>
	                    </span>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
	<div id="tpsChart" style="width: 100%;border-bottom:solid 3px #fff ; height: 200px; -moz-user-select: none; position: relative; background: transparent none repeat scroll 0% 0%;" _echarts_instance_="ec_1515575675805"><div style="position: relative; overflow: hidden; width: 783px; height: 380px; padding: 0px; margin: 0px; border-width: 0px; cursor: default;"><canvas style="position: absolute; left: 0px; top: 0px; width: 783px; height: 380px; -moz-user-select: none; padding: 0px; margin: 0px; border-width: 0px;" width="783" height="380" data-zr-dom-id="zr_0"></canvas></div><div style="position: absolute; display: none; border-style: solid; white-space: nowrap; z-index: 9999999; transition: left 0.4s cubic-bezier(0.23, 1, 0.32, 1) 0s, top 0.4s cubic-bezier(0.23, 1, 0.32, 1) 0s; background-color: rgba(50, 50, 50, 0.7); border-width: 0px; border-color: rgb(51, 51, 51); border-radius: 4px; color: rgb(255, 255, 255); font: normal normal normal normal 14px/21px Microsoft YaHei; padding: 5px; left: 320px; top: 240px;">利用方式 <br>婚姻凭证 : 0 (0%)</div></div>
	<div id="qpsChart" style="width: 100%;border-bottom:solid 3px #fff ; height: 200px; -moz-user-select: none; position: relative; background: transparent none repeat scroll 0% 0%;" _echarts_instance_="ec_1515575675805"><div style="position: relative; overflow: hidden; width: 783px; height: 380px; padding: 0px; margin: 0px; border-width: 0px; cursor: default;"><canvas style="position: absolute; left: 0px; top: 0px; width: 783px; height: 380px; -moz-user-select: none; padding: 0px; margin: 0px; border-width: 0px;" width="783" height="380" data-zr-dom-id="zr_0"></canvas></div><div style="position: absolute; display: none; border-style: solid; white-space: nowrap; z-index: 9999999; transition: left 0.4s cubic-bezier(0.23, 1, 0.32, 1) 0s, top 0.4s cubic-bezier(0.23, 1, 0.32, 1) 0s; background-color: rgba(50, 50, 50, 0.7); border-width: 0px; border-color: rgb(51, 51, 51); border-radius: 4px; color: rgb(255, 255, 255); font: normal normal normal normal 14px/21px Microsoft YaHei; padding: 5px; left: 320px; top: 240px;">利用方式 <br>婚姻凭证 : 0 (0%)</div></div>
	
	<div id="usedMemoryChart" style="width: 100%;border-bottom:solid 3px #fff ; height: 200px; -moz-user-select: none; position: relative; background: transparent none repeat scroll 0% 0%;" _echarts_instance_="ec_1515575675805"><div style="position: relative; overflow: hidden; width: 783px; height: 380px; padding: 0px; margin: 0px; border-width: 0px; cursor: default;"><canvas style="position: absolute; left: 0px; top: 0px; width: 783px; height: 380px; -moz-user-select: none; padding: 0px; margin: 0px; border-width: 0px;" width="783" height="380" data-zr-dom-id="zr_0"></canvas></div><div style="position: absolute; display: none; border-style: solid; white-space: nowrap; z-index: 9999999; transition: left 0.4s cubic-bezier(0.23, 1, 0.32, 1) 0s, top 0.4s cubic-bezier(0.23, 1, 0.32, 1) 0s; background-color: rgba(50, 50, 50, 0.7); border-width: 0px; border-color: rgb(51, 51, 51); border-radius: 4px; color: rgb(255, 255, 255); font: normal normal normal normal 14px/21px Microsoft YaHei; padding: 5px; left: 320px; top: 240px;">利用方式 <br>婚姻凭证 : 0 (0%)</div></div>
	<div id="usedcpusysChart" style="width: 100%;border-bottom:solid 3px #fff ; height: 200px; -moz-user-select: none; position: relative; background: transparent none repeat scroll 0% 0%;" _echarts_instance_="ec_1515575675805"><div style="position: relative; overflow: hidden; width: 783px; height: 380px; padding: 0px; margin: 0px; border-width: 0px; cursor: default;"><canvas style="position: absolute; left: 0px; top: 0px; width: 783px; height: 380px; -moz-user-select: none; padding: 0px; margin: 0px; border-width: 0px;" width="783" height="380" data-zr-dom-id="zr_0"></canvas></div><div style="position: absolute; display: none; border-style: solid; white-space: nowrap; z-index: 9999999; transition: left 0.4s cubic-bezier(0.23, 1, 0.32, 1) 0s, top 0.4s cubic-bezier(0.23, 1, 0.32, 1) 0s; background-color: rgba(50, 50, 50, 0.7); border-width: 0px; border-color: rgb(51, 51, 51); border-radius: 4px; color: rgb(255, 255, 255); font: normal normal normal normal 14px/21px Microsoft YaHei; padding: 5px; left: 320px; top: 240px;">利用方式 <br>婚姻凭证 : 0 (0%)</div></div>
</body>
<script type="text/javascript">
	//JavaScript代码区域
	layui.use(['layer','jquery','element','form'], function(){
		var $ = layui.jquery,
      		element = layui.element,
      		form = layui.form,
      		layer = layui.layer;
    window.timeTicket = setInterval(function (){
    	loadAsyncChart();
    }, 5000);

	//监听redis配置下拉列表change事件
  	form.on('select(dbId)', function(data){
  		loadAsyncChart();
  	});
	
    //监听刷新时间下拉列表change事件
  	form.on('select(intervalTime)', function(data){
  		clearInterval(window.timeTicket);
  		window.timeTicket = setInterval(function (){
  	    	loadAsyncChart();
  	    }, data.value * 1000);
  	});
    //used memory (内存分布图)
    var usedMemoryOption = {
		    title: {
		        text: '内存分配',
		        left: 'left',
		        top: 0,
		        textStyle: {
		            color: '#fff'
		        }
		    },
    	    tooltip : {
    	        trigger: 'axis',
    	        axisPointer: {
    	            type: 'cross',
    	            label: {
    	                backgroundColor: '#6a7985'
    	            }
    	        }
    	    },
    	    legend: {
    	        data:['最高值','当前值']
    	    },
    	    grid: {
    	        left: '5%',
    	        right: '5%',
    	        bottom: '3%',
    	        containLabel: true
    	    },
    	    xAxis : [
    	        {
    	            type : 'category',
    	            boundaryGap : false,
    	            data : ['0','0','0','0','0','0','0','0','0','0']
    	        }
    	    ],
    	    yAxis : [
    	        {
    	            type : 'value'
    	        }
    	    ],
    	    series : [
    	        {
    	            name:'最高值',
    	            type:'line',
    	            stack: '总量',
//     	            areaStyle: {normal: {}},
    	            data:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    	        },
    	        {
    	            name:'当前值',
    	            type:'line',
    	            stack: '总量',
//     	            areaStyle: {normal: {}},
    	            data:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    	        }
    	    ]
    	};
  	//初始化echarts实例
    var usedMemoryCharts = echarts.init(document.getElementById('usedMemoryChart'),'dark');
 	//使用刚指定的配置项和数据显示图表。
    usedMemoryCharts.setOption(usedMemoryOption);
 	
    //usedcpusys (CPU总占时图表)
    var usedcpusysOption = {
		    title: {
		        text: 'CPU总占时',
		        left: 'left',
		        top: 0,
		        textStyle: {
		            color: '#fff'
		        }
		    },
    	    tooltip : {
    	        trigger: 'axis',
    	        axisPointer: {
    	            type: 'cross',
    	            label: {
    	                backgroundColor: '#6a7985'
    	            }
    	        }
    	    },
    	    legend: {
    	        data:['占时']
    	    },
    	    grid: {
    	        left: '5%',
    	        right: '5%',
    	        bottom: '3%',
    	        containLabel: true
    	    },
    	    xAxis : [
    	        {
    	            type : 'category',
    	            boundaryGap : false,
    	            data : ['0','0','0','0','0','0','0','0','0','0']
    	        }
    	    ],
    	    yAxis : [
    	        {
    	            type : 'value'
    	        }
    	    ],
    	    series : [
    	        {
    	            name:'占时',
    	            type:'line',
    	            stack: '总量',
    	            areaStyle: {normal: {}},
    	            data:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    	        }
    	    ]
    	};
  	//初始化echarts实例
    var usedcpusysCharts = echarts.init(document.getElementById('usedcpusysChart'),'dark');
 	//使用刚指定的配置项和数据显示图表。
    usedcpusysCharts.setOption(usedcpusysOption);
 	
    //TPS (每秒处理命令)
    var tpsOption = {
		    title: {
		        text: 'TPS (每秒处理命令)',
		        left: 'left',
		        top: 0,
		        textStyle: {
		            color: '#fff'
		        }
		    },
    	    tooltip : {
    	        trigger: 'axis',
    	        axisPointer: {
    	            type: 'cross',
    	            label: {
    	                backgroundColor: '#6a7985'
    	            }
    	        }
    	    },
    	    legend: {
    	        data:['TPS']
    	    },
    	    grid: {
    	        left: '5%',
    	        right: '35%',
    	        bottom: '3%',
    	        containLabel: true
    	    },
    	    xAxis : [
    	        {
    	            type : 'category',
    	            boundaryGap : false,
    	            data : ['0','0','0','0','0','0','0','0','0','0']
    	        }
    	    ],
    	    yAxis : [
    	        {
    	            type : 'value'
    	        }
    	    ],
    	    series : [
    	        {
    	            name:'TPS',
    	            type:'line',
    	            stack: '每秒处理命令',
    	            itemStyle : {  
                        normal : {  
                            lineStyle:{  
                                color:'red'  
                            }  
                        }  
                    },
    	            data:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    	        },
    	        {
	                name:'命令处理比例',
	                type:'pie',
		            center: ['80%', '45%'],
	                radius : '45%',
	                label: {
	                    normal: {
	                        position: 'outside',
	                        formatter: '{b}:【占比{d}%】',
	                        itemStyle: {
	    		                emphasis: {
	    		                    shadowBlur: 10,
	    		                    shadowOffsetX: 0,
	    		                    shadowColor: 'rgba(0, 0, 0, 0.5)'
	    		                }
	    		            }
	                    }
	                    
	                },
	                data:[
	                    {value:0, name:'成功处理'},
	                    {value:0, name:'失败处理'}
	                ]
            	}
    	    ]
    	};
  	//初始化echarts实例
    var tpsCharts = echarts.init(document.getElementById('tpsChart'),'dark');
 	//使用刚指定的配置项和数据显示图表。
    tpsCharts.setOption(tpsOption);
 	
    //QPS (每秒执行命令)
    var qpsOption = {
		    title: {
		        text: 'QPS (每秒执行命令)',
		        left: 'left',
		        top: 0,
		        textStyle: {
		            color: '#fff'
		        }
		    },
    	    tooltip : {
    	        trigger: 'axis',
    	        axisPointer: {
    	            type: 'cross',
    	            label: {
    	                backgroundColor: '#6a7985'
    	            }
    	        }
    	    },
    	    legend: {
    	        data:['QPS']
    	    },
    	    grid: {
    	        left: '5%',
    	        right: '5%',
    	        bottom: '3%',
    	        containLabel: true
    	    },
    	    xAxis : [
    	        {
    	            type : 'category',
    	            boundaryGap : false,
    	            data : ['0','0','0','0','0','0','0','0','0','0']
    	        }
    	    ],
    	    yAxis : [
    	        {
    	            type : 'value'
    	        }
    	    ],
    	    series : [
    	        {
    	            name:'QPS',
    	            type:'line',
    	            stack: '每秒执行命令',
    	            itemStyle : {  
                        normal : {  
                            lineStyle:{  
                                color:'#fee905'  
                            }  
                        }  
                    },
    	            data:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    	        }
    	    ]
    	};
  	//初始化echarts实例
    var qpsCharts = echarts.init(document.getElementById('qpsChart'),'dark');
 	//使用刚指定的配置项和数据显示图表。
    qpsCharts.setOption(qpsOption);
    function loadAsyncChart(){
    	var dbId = $('#dbId').find('option:selected').val(),
    		intervalTime = $('#intervalTime').find('option:selected').val();
    	if(intervalTime == ''){
    		intervalTime = 10;
    	}
    	//异步加载统计信息
        $.post('${basePath}redis/loadAsyncChart',{dbId: dbId},function(result){
        	var used_memory = 0,totalkeys = 0, clients = 0, commands_processed = 0, used_memory_peak = 0, used_cpu_sys = 0,
        	instantaneous_ops_per_sec = 0, qps = 0, keyspace_hits=0,keyspace_misses=0;
        	if(result) {
       			//当前内存使用
       			used_memory = result['used_memory'];
       			//内存使用峰值
       			used_memory_peak = result['used_memory_peak'];
       			//使用redis库总数
       			totalkeys = result['totalKeys'];
       			//连接客户端数量
       			clients = result['connected_clients'];
       			//已执行总命令数
       			commands_processed = result['total_commands_processed'];
       			if(window.commands_processeds == 'undefined'){
       				qps = 0;
       			}else{
       				qps = (commands_processed - window.commands_processeds)/intervalTime;
       			}
       			window.commands_processeds = commands_processed;
       			//CPU总占时
       			used_cpu_sys = result['used_cpu_sys'];
       			//TPS
       			instantaneous_ops_per_sec = result['instantaneous_ops_per_sec'];
       			//命中次数
       			keyspace_hits = result['keyspace_hits'];
       			//未命中次数
       			keyspace_misses = result['keyspace_misses'];
       			
        		$('#used_memory').text(bytesToSize(used_memory));
            	$('#totalkeys').text(totalkeys);
            	$('#clients').text(clients);
            	$('#commands_processed').text(commands_processed);
            	
            	// 内存使用图表
            	var datetime = new Date();
     			axisData = datetime.getHours()+":" + datetime.getMinutes() +":" + datetime.getSeconds();
     			var usedMemoryxAxis = usedMemoryOption.xAxis[0]['data']
     			usedMemoryxAxis.push(axisData);
     			usedMemoryxAxis.shift();
     			
     			//最高值
     			var usedMemorySeries0 = usedMemoryOption.series[0]['data'];
     			var used_memory_peak_vert = bytesToSize(used_memory_peak).split(' ');
     			usedMemorySeries0.push(used_memory_peak_vert[0]);
     			usedMemorySeries0.shift();
     			usedMemoryOption['title']['text'] = '内存分配  单位: ' + used_memory_peak_vert[1];
     			//当前值
     			var usedMemorySeries1 = usedMemoryOption.series[1]['data'];
     			var used_memory_vert = bytesbyItem(used_memory, used_memory_peak_vert[1]);
     			usedMemorySeries1.push(used_memory_vert);
     			usedMemorySeries1.shift();
     			usedMemoryCharts.setOption(usedMemoryOption);
     			
     			//CPU占时图表
     			var usedcpusysxAxis = usedcpusysOption.xAxis[0]['data']
     			usedcpusysxAxis.push(axisData);
     			usedcpusysxAxis.shift();
     			
     			//占时
     			var usedcpusysSeries = usedcpusysOption.series[0]['data'];
     			usedcpusysSeries.push(used_cpu_sys);
     			usedcpusysSeries.shift();
     			usedcpusysCharts.setOption(usedcpusysOption);
     			
     			//TPS
     			var tpsxAxis = tpsOption.xAxis[0]['data']
     			tpsxAxis.push(axisData);
     			tpsxAxis.shift();
     			
     			var tpsSeries = tpsOption.series[0]['data'];
     			tpsSeries.push(instantaneous_ops_per_sec);
     			tpsSeries.shift();
     			
     			//命令执行比例
     			var hitsSeries = tpsOption.series[1]['data'];
     			hitsSeries[0]['value'] = keyspace_hits;
     			hitsSeries[1]['value'] = keyspace_misses;
     			tpsCharts.setOption(tpsOption);
     			
     			//QPS
     			var qpsxAxis = qpsOption.xAxis[0]['data']
     			qpsxAxis.push(axisData);
     			qpsxAxis.shift();
     			
     			var qpsSeries = qpsOption.series[0]['data'];
     			qpsSeries.push(qps);
     			qpsSeries.shift();
     			qpsCharts.setOption(qpsOption);
        	}
        },'json');
    }
    function bytesToSize(bytes) {
        if (bytes === 0) return '0 B';
        var k = 1024, // or 1024
            sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
            i = Math.floor(Math.log(bytes) / Math.log(k));
     
       return (bytes / Math.pow(k, i)).toPrecision(3) + ' ' + sizes[i];
    }
    function bytesbyItem(bytes, unit){
    	var size = "";
    	if( unit == 'B' && bytes < 0.1 * 1024 ){ //如果小于0.1KB转化成B  
            size = bytes.toFixed(2) + "";    
        }else if(unit == 'KB' && bytes < 0.1 * 1024 * 1024 ){//如果小于0.1MB转化成KB  
            size = (bytes / 1024).toFixed(2) + "";              
        }else if(unit == 'MB' && bytes < 0.1 * 1024 * 1024 * 1024){ //如果小于0.1GB转化成MB  
            size = (bytes / (1024 * 1024)).toFixed(2) + "";  
        }else{ //其他转化成GB  
            size = (bytes / (1024 * 1024 * 1024)).toFixed(2) + "";  
        } 
    	var sizestr = size + "";   
        var len = sizestr.indexOf("\.");  
        var dec = sizestr.substr(len + 1, 2);  
        if(dec == "00"){//当小数点后为00时 改成01
            return sizestr.substring(0,len) + ".01";  
        }else{
        	return sizestr;
        }
      }
	});
</script>
</html>