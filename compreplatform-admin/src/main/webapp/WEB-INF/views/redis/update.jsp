<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags"%>
<!DOCTYPE html>
<html>
	<head>
	  	<meta charset="utf-8">
	  	<title>layuiTile</title>
	  	<meta name="renderer" content="webkit">
	  	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	  	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	  	<!-- 为兼容火狐此引用必须放在这里 -->
	 	<tag:header headerType="base"/>
	</head>
	<body>  
		<form class="layui-form" action="" method='POST' style="width:100%;">
			<input type="hidden" name="id" value="${entity.id}"/>
		  	<div class="compre-content">
			<div class="layui-form-item">
		    	<div class="layui-inline">
			      	<label class="layui-form-label"><i class='layui-must'>*</i>描述
			      	</label>
			      	<div class="layui-input-inline layui-input-70">
			        	<input type="text" name="dbDesc" value="${entity.dbDesc}" lay-verify="required" autocomplete="off" placeholder="请输入描述" class="layui-input">
			      	</div>
		    	</div>
		  	</div>
			<div class="layui-form-item">
		    	<div class="layui-inline">
			      	<label class="layui-form-label"><i class='layui-must'>*</i>数据库类型
			      	</label>
			      	<div class="layui-input-inline layui-input-70">
			        	<input type="text" name="dbType" value="${entity.dbType}" lay-verify="required" autocomplete="off" placeholder="请输入数据库类型" class="layui-input">
			      	</div>
		    	</div>
		  	</div>
			<div class="layui-form-item">
		    	<div class="layui-inline">
			      	<label class="layui-form-label"><i class='layui-must'>*</i>ip地址
			      	</label>
			      	<div class="layui-input-inline layui-input-70">
			        	<input type="text" name="dbUrl" value="${entity.dbUrl}" lay-verify="required" autocomplete="off" placeholder="请输入ip地址" class="layui-input">
			      	</div>
		    	</div>
		  	</div>
			<div class="layui-form-item">
		    	<div class="layui-inline">
			      	<label class="layui-form-label">端口
			      	</label>
			      	<div class="layui-input-inline layui-input-70">
			        	<input type="text" name="dbPort" value="${entity.dbPort}" lay-verify="required|number" onkeyup="this.value=this.value.replace(/[^\d]/g,'') " onafterpaste="this.value=this.value.replace(/[^\d]/g,'') " autocomplete="off" placeholder="请输入端口,且只能输入数字" class="layui-input">
			      	</div>
		    	</div>
		  	</div>
			<div class="layui-form-item">
		    	<div class="layui-inline">
			      	<label class="layui-form-label">密码
			      	</label>
			      	<div class="layui-input-inline layui-input-70">
			        	<input type="text" name="password" value="${entity.password}" lay-verify="" autocomplete="off" placeholder="请输入密码" class="layui-input">
			      	</div>
		    	</div>
		  	</div>
		  	</div>
		   	<!-- layui-footer layui-fixbar -->
		  	<div class="layui-form-item">
		  		<div class="layui-inline compre-footer">
			      	<label class="layui-form-label"></label>
			      	<button class="layui-btn compre-btn" lay-submit="" lay-filter="form-submit">提交</button>
			      	<button type="reset" class="layui-btn compre-btn compre-btn-reset layui-btn-primary">重置</button>
			      	<button type="button" class="layui-btn layui-btn-danger" id="checkConnection" style="margin-top: 8px;">测试连接</button>
			    </div>
		  	</div>
		</form>
	</body>
	<script>
	layui.use('form', function(){
		  var form = layui.form,
		  		layer = layui.layer,
		  		$ = layui.$;
		  //监听提交
		  form.on('submit(form-submit)', function(data){
		  	$.post('${basePath}redis/update',data.field,function(result){
		  		if(result.code != 200){
		  			top.layer.msg(result.msg);
		  		}else{
		  			top.layer.closeAll();
		  			top.layer.msg(result.msg);
		  		}
		  	});
		    return false;
		  });
		//测试连接
		  $('#checkConnection').on('click', function(){
			  var dbUrl = $('input[name="dbUrl"]').val();
			  if(dbUrl == ''){
				  top.layer.msg('请输入ip地址');
				  return;
			  }
			  var dbPort = $('input[name="dbPort"]').val();
			  if(dbPort == ''){
				  top.layer.msg('请输入端口');
				  return;
			  }
			  var password = $('input[name="password"]').val();
			  var index = top.layer.load(1, {
			  	  shade: [0.3,'#fff']
			  });
			  $.post('${basePath}/redis/checkConnection',{dbUrl: dbUrl, dbPort:dbPort, password: password},function(result){
				  top.layer.close(index);
				  if(result.code != 200){
			  			top.layer.msg("连接失败");
			  		}else{
			  			top.layer.msg("连接成功");
			  		}
			  },'json');			  
		  })
	});
	</script>
</html>