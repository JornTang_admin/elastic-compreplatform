<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>layui</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <!-- 为兼容火狐此引用必须放在这里 -->
 <tag:header headerType="base"/>
</head>
<body>  
<form class="layui-form" action="" method='POST' style="width:100%;">
  <input name="id" type="hidden" value="${sysConfig.id}"/>
  <div class="compre-content">
  <div class="layui-form-item">
    <label class="layui-form-label">描述</label>
    <div class="layui-input-inline layui-input-70">
      <input type="text" name="dbDesc" value="${sysConfig.dbDesc}" lay-verify="required" autocomplete="off" placeholder="请输入描述" class="layui-input">
    </div>
  </div>
  
  <div class="layui-form-item">
    <label class="layui-form-label">数据库类型</label>
    <div class="layui-input-inline layui-input-70">
      <select name="dbType" lay-verify="required" lay-filter="dbType">
        <option value=""></option>
        <option value="Mysql" <c:if test="${sysConfig.dbType eq 'Mysql'}">selected="true"</c:if>>Mysql</option>
        <option value="Oracle" <c:if test="${sysConfig.dbType eq 'Oracle'}">selected="true"/</c:if>>Oracle</option>
		<!-- 目前只支持mysql、oracle -->
<!--         <option value="SQLServer2000">SQLServer2000</option> -->
<!--         <option value="SQLServer2005">SQLServer2005</option> -->
<!--         <option value="JTDs for SQLServer">JTDs for SQLServer</option> -->
<!--         <option value="PostgreSql">PostgreSql</option> -->
<!--         <option value="Sybase">Sybase</option> -->
<!--         <option value="DB2">DB2</option> -->
<!--         <option value="HsqlDB">HsqlDB</option> -->
<!--         <option value="Derby">Derby</option> -->
      </select>
    </div>
  </div>
  
  <div class="layui-form-item">
    <div class="layui-inline">
      <label class="layui-form-label">数据库url</label>
      <div class="layui-input-inline layui-input-70">
        <input type="tel" name="dbUrl" value="${sysConfig.dbUrl}" lay-verify="required" autocomplete="off" placeholder="请输入IP地址" class="layui-input">
      </div>
    </div>
  </div>
  
  <div class="layui-form-item">
    <div class="layui-inline">
      <label class="layui-form-label">Schema</label>
      <div class="layui-input-inline layui-input-70">
        <input type="text" name="dbSchema" value="${sysConfig.dbSchema}" lay-verify="required" placeholder="请输入数据库schema" autocomplete="off" class="layui-input">
      </div>
    </div>
  </div>
  
  <div class="layui-form-item">
    <div class="layui-inline">
      <label class="layui-form-label">用户名</label>
      <div class="layui-input-inline layui-input-70">
        <input type="text" name="username" value="${sysConfig.username}" lay-verify="required" autocomplete="off" placeholder="请输入用户名" class="layui-input">
      </div>
    </div>
  </div>
  
  <div class="layui-form-item">
    <div class="layui-inline">
      <label class="layui-form-label">密码</label>
      <div class="layui-input-inline layui-input-70">
        <input type="text" name="password" value="${sysConfig.password}" lay-verify="required" autocomplete="off" placeholder="请输入密码" class="layui-input">
      </div>
    </div>
  </div>
  </div>
   <!-- layui-footer layui-fixbar -->
  <div class="layui-form-item">
  	<div class="layui-inline compre-footer">
      <label class="layui-form-label"></label>
      <button class="layui-btn compre-btn" lay-submit="" lay-filter="form-submit">提交</button>
      <button type="reset" class="layui-btn compre-btn compre-btn-reset layui-btn-primary">重置</button>
    </div>
  </div>
</form>
</body>
<script>
layui.use('form', function(){
	  var form = layui.form,
	  		layer = layui.layer,
	  		$ = layui.$;
	  
	  
	  //监听提交
	  form.on('submit(form-submit)', function(data){
	  	$.post('${basePath}codegener/saveOrUpdateDbconf.do',data.field,function(result){
	  		if(result.code != 200){
	  			window.top.layer.msg(result.msg);
	  		}else{
	  			window.top.layer.closeAll();
	  			window.top.layer.msg(result.msg);
	  		}
	  	});
	    return false;
	  });
});
</script>
</html>