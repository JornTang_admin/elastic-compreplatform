<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>layui</title>
		<meta name="renderer" content="webkit">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<!-- 为兼容火狐此引用必须放在这里 -->
		<tag:header headerType="base"/>
	</head>
	<body>
		<div class="compre-search">
		 	<div class="layui-btn-group layui-btn-left compreTable">
		    	<button class="layui-btn layui-btn-primary" data-type="dbConfig"><i class="layui-icon"></i>添加</button>
		  	</div>
		  	<div class="layui-right-search">
		  		<input type="text" name="" id="dbDesc" required lay-verify="required" placeholder="根据描述搜索" autocomplete="off" class="layui-input layui-input-right">  
		  		<button class="layui-btn layui-btn-primary layui-btn-left" id="searchTable"><i class="layui-icon">&#xe615;</i>搜索</button>
		  	</div> 
	  	</div>
		<table class="layui-table" lay-data="{height: 'full-120', cellMinWidth: 50, limit:15}" lay-filter="compreTable">
	  	<thead>
		    <tr>
		      <th lay-data="{type:'checkbox', fixed: 'left'}"></th>
		      <th lay-data="{field:'dbDesc'}">描述</th>
		      <th lay-data="{field:'dbType', sort: true}">数据库类型</th>
		      <th lay-data="{field:'dbUrl'}">IP地址</th>
		      <th lay-data="{field:'schema'}">schema</th>
		      <th lay-data="{field:'username'}">用户名</th>
		      <th lay-data="{field:'password'}">密码</th>
		      <th lay-data="{field:'dbDriver'}">数据库驱动</th>
		      <th lay-data="{fixed: 'right', width:178, align:'center', toolbar: '#barcompreTable'}">操作</th>
		    </tr>
	  </thead>
	</table>
	 
	<script type="text/html" id="barcompreTable">
  <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
  <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
	</script>
	               
	<!-- 
	(function () {
	    //加载列表的后端 url
	    var getListUrl = '';
	
	    //对于任意一个 table，按照官方的说法，有三种不同的初始化渲染方式，不多介绍，而这里使用的方式姑且看做第三种：转换静态表格 方式
	    //转换静态表格方式，自然首先需要有一个已经存在的表格，然后再通过 js 方式转化为 Layui 表格
	    //无论哪种方式的 Layui table 初始化自然需要配置项
	    //通过转化的方式初始化 Layui table，配置项部分可以在 源table中，部分在js中，源 table 的源代码上文已经给出，下面给出一个示例的 js 中的配置项
	    var tableOptions = {
	        url: getListUrl, //请求地址
	        method: 'POST', //方式
	        id: 'listReload', //生成 Layui table 的标识 id，必须提供，用于后文刷新操作，笔者该处出过问题
	        page: false, //是否分页
	        where: { type: "all" }, //请求后端接口的条件，该处就是条件错误点，按照官方给出的代码示例，原先写成了 where: { key : { type: "all" } }，结果并不是我想的那样，如此写，key 将是后端的一个类作为参数，里面有 type 属性，如果误以为 key 是 Layui 提供的格式，那就大错特错了
	        response: { //定义后端 json 格式，详细参见官方文档
	            statusName: 'Code', //状态字段名称
	            statusCode: '200', //状态字段成功值
	            msgName: 'Message', //消息字段
	            countName: 'Total', //总数字段
	            dataName: 'Result' //数据字段
	        }
	    };
	
	    //
	    layui.use(['table', 'layer'], function () {//layui 模块引用，根据需要自行修改
	        var layer = layui.layer, table = layui.table;
	
	        //表初始化
	        var createTable = function () {
	            table.init('EditListTable', tableOptions);
	// table lay-filter
	
	        };
	
	        //表刷新方法
	        var reloadTable = function (item) {
	            table.reload("listReload", { //此处是上文提到的 初始化标识id
	                where: {
	                    //key: { //该写法上文已经提到
	                        type: item.type, id: item.id
	                    //}
	                }
	            });
	        };
	
	        //表初始化
	        createTable();
	
	        //其他和 tree 相关的方法，其中包括 点击 tree 项调用刷新方法
	    });
	})();
	 -->
	<script>
	layui.use('table', function(){
	  var table = layui.table,
	  		$ = layui.$;
	  var tableOptions = {
	        url: '${basePath}codegener/dblist.do', //请求地址
	        method: 'POST', //方式
	        id: 'listReload', //生成 Layui table 的标识 id，必须提供，用于后文刷新操作，笔者该处出过问题
	        page: true, //是否分页
	        where: { type: "all" }, //请求后端接口的条件，该处就是条件错误点，按照官方给出的代码示例，原先写成了 where: { key : { type: "all" } }，结果并不是我想的那样，如此写，key 将是后端的一个类作为参数，里面有 type 属性，如果误以为 key 是 Layui 提供的格式，那就大错特错了
	        response: { //定义后端 json 格式，详细参见官方文档
	            statusName: 'code', //状态字段名称
	            statusCode: '200', //状态字段成功值
	            msgName: 'msg', //消息字段
	        }
	    };
	    table.init('compreTable', tableOptions);
	  //条件搜索	
	  $('#searchTable').on('click',function(){
	  		table.reload("listReload", {
	             where: {
	                     dbDesc: $('#dbDesc').val()
	             }
	        });
	  });
	  //监听表格复选框选择
	  table.on('checkbox(compreTable)', function(obj){
	    console.log(obj)
	  });
	  //监听工具条
	  table.on('tool(compreTable)', function(obj){
	    var data = obj.data;
	    if(obj.event === 'del'){
	      top.layer.confirm('确定删除?', function(index){
	      	$.post('${basePath}codegener/deleteDbconf.do',{id: data.id},function(result){
	      		top.layer.msg(result.msg);
	      		table.reload('listReload');
	        	top.layer.close(index);
	      	});
	      });
	    } else if(obj.event === 'edit'){
	      window.top.$.layerFrame({callBack:function(){table.reload('listReload');},content: ['${basePath}codegener/toDbconfAddOrUpdate.do?id=' + data.id,'no'],title: '修改数据库配置',});
	    }
	  });
	  
	  var $ = layui.$, active = {
	  	//数据库配置
	  	dbConfig: function(opt){
	  		window.top.$.layerFrame({callBack:function(){table.reload('listReload');},content: ['${basePath}codegener/toDbconfAddOrUpdate.do','no'],title: '添加数据库配置',});
	  	}
	  };
	  
	  $('.compreTable .layui-btn').on('click', function(){
	    var type = $(this).data('type');
	    active[type] ? active[type].call(this) : '';
	  });
	});
	</script>
	
	</body>
</html>