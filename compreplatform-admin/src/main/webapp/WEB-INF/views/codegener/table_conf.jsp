<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Elastic Comprepaltform</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <!-- 为兼容火狐此引用必须放在这里 -->
 <tag:header headerType="base"/>
</head>
<body>  
<form class="layui-form" action="" style="width:100%;">
  <input name="id" type="hidden" value="${dbConfig.id}"/>
  <input name="tableName" type="hidden" value="${tableName}"/>
  <div class="compre-content">
	  <div class="layui-form-item">
	    <label class="layui-form-label">基础包名</label>
	    <div class="layui-input-inline layui-input-70">
	      <input type="text" name="basepackage" lay-verify="required" autocomplete="off" placeholder="请输入基础包名" class="layui-input">
	    </div>
	  </div>
	  
	  
	  <div class="layui-form-item">
	    <div class="layui-inline">
	      <label class="layui-form-label">表名前缀</label>
	      <div class="layui-input-inline layui-input-70">
	        <input type="tel" name="tableRemovePrefixes" lay-verify="" autocomplete="off" placeholder="需要移除的表名前缀,多个前缀使用逗号,分割" class="layui-input">
	      </div>
	    </div>
	  </div>
  
  </div>
   <!-- layui-footer layui-fixbar -->
  <div class="layui-form-item">
  	<div class="layui-inline compre-footer">
      <label class="layui-form-label"></label>
      <button class="layui-btn compre-btn" lay-submit="" lay-filter="form-submit">提交</button>
      <button type="reset" class="layui-btn compre-btn compre-btn-reset layui-btn-primary">重置</button>
    </div>
  </div>
</form>
</body>
<script>
function convertJson(json){
	var array = new Array();
	for(var key in json){
		array.push(key+"=" +json[key]);
	}
	return array.join('&');
}
layui.use('form', function(){
	  var form = layui.form,
	  		layer = layui.layer,
	  		$ = layui.$;
	  //监听提交
	  form.on('submit(form-submit)', function(data){
	   	var url = '${basePath}user/codeBuild.do?' + convertJson(data.field);
	  	window.open(url,'top');
	  	//window.top.layer.closeAll();
	  });
});
</script>
</html>