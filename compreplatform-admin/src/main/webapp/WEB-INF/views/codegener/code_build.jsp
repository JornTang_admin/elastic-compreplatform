<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>layui</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <!-- 为兼容火狐此引用必须放在这里 -->
  <tag:header headerType="base"/>
</head>
<body>
	<div class="compre-search">
	 	<div class="layui-btn-left compreTable">
	 		<form class="layui-form layui-form-left" action="">
			    <select name="dbType" id="dbType" lay-filter="dbType" lay-verify="required" lay-search="">
			      <option value="">选择或搜索数据库配置</option>
			      <c:forEach items="${sysConfigs}" var="conf">
			      	<option value="${conf.id}">${conf.dbDesc}</option>
			      </c:forEach>
			    </select>
		    </form>
	    	<button class="layui-btn layui-btn-primary" data-type="codeBuild"><i class="layui-icon"></i>生成代码</button>
	  	</div>
	  	<div class="layui-right-search">
	  		<input type="text" name="" id="tableName" required lay-verify="required" placeholder="根据表名搜索" autocomplete="off" class="layui-input layui-input-right">  
	  		<button class="layui-btn layui-btn-primary layui-btn-left" id="searchTable"><i class="layui-icon">&#xe615;</i>搜索</button>
	  	</div> 
  	</div>
	<table class="layui-table" lay-data="{height: 'full-170', cellMinWidth: 50, url:'${basePath}codegener/searchTablelist.do', page:true, limit:15, id:'listReload',method:'POST'}" lay-filter="compreTable">
  	<thead>
	    <tr>
	      <th lay-data="{type:'checkbox', fixed: 'left'}"></th>
	      <th lay-data="{field:'TABLENAME'}">表名</th>
	      <th lay-data="{field:'TABLETYPE', sort: true}">数据库类型</th>
	      <th lay-data="{field:'TABLECOMMENT'}">注释</th>
	      <th lay-data="{field:'CREATETIME'}">创建时间</th>
	    </tr>
  </thead>
</table>
 
<script type="text/html" id="barcompreTable">
  <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
  <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
</script>
<script>
layui.use(['table','form','laypage'], function(){
  var table = layui.table,
  		form = layui.form,
  		$ = layui.$;
  var tableOptions = {
        url: "${basePath}codegener/searchTablelist.do", //请求地址
        method: 'POST', //方式
        id: 'listReload', //生成 Layui table 的标识 id，必须提供，用于后文刷新操作，笔者该处出过问题
        page: true, //是否分页
        where: { type: "all" }, //请求后端接口的条件，该处就是条件错误点，按照官方给出的代码示例，原先写成了 where: { key : { type: "all" } }，结果并不是我想的那样，如此写，key 将是后端的一个类作为参数，里面有 type 属性，如果误以为 key 是 Layui 提供的格式，那就大错特错了
        response: { //定义后端 json 格式，详细参见官方文档
            statusName: 'code', //状态字段名称
            statusCode: '200', //状态字段成功值
            msgName: 'msg', //消息字段
        }
    };
    table.init('compreTable', tableOptions);
  //条件搜索	
  $('#searchTable').on('click',function(){
  		table.reload("listReload", {
             where: {
                     tableName: $('#tableName').val()
             }
        });
  });
  form.on('select(dbType)', function(data){
  		table.reload("listReload", {
             where: {
                     id: data.value
             },
             page:{
        		curr: 1
        	}
        });
  });
  //监听表格复选框选择
  table.on('checkbox(compreTable)', function(obj){
    console.log(obj)
  });
  //监听工具条
  table.on('tool(compreTable)', function(obj){
    var data = obj.data;
    if(obj.event === 'del'){
      top.layer.confirm('确定删除?', function(index){
      	$.post('${basePath}codegener/deleteDbconf.do',{id: data.id},function(result){
      		top.layer.msg(result.msg);
      		table.reload('listReload');
        	top.layer.close(index);
      	});
      });
    } else if(obj.event === 'edit'){
      window.top.$.layerFrame({callBack:function(){table.reload('listReload');},content: ['${basePath}codegener/toDbconfAddOrUpdate.do?id=' + data.id,'no'],title: '修改数据库配置',});
    }
  });
  
  var $ = layui.$, active = {
  	//代码生成配置
  	codeBuild: function(data){
  		var dbId = $('#dbType').find("option:selected").val()
  		var checkStatus = table.checkStatus('listReload'),data = checkStatus.data;
  		if(data.length< 1){
  			top.layer.msg('至少选择一条记录操作');
  			return;
  		}
  		if(data.length> 1){
  			top.layer.msg('只能选择一条记录操作');
  			return;
  		}
  		window.top.$.layerFrame({callBack:function(){table.reload('listReload');},content: ['${basePath}codegener/toTableConf.do?id=' + dbId +"&tableName=" + data[0].TABLENAME,'no'],title: '添加代码生成配置',});
  	}
  };
  
  $('.compreTable .layui-btn').on('click', function(){
    var type = $(this).data('type');
    active[type] ? active[type].call(this) : '';
  });
});
</script>

</body>
</html>