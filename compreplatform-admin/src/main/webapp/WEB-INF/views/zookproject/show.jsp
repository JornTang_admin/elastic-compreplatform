<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags"%>
<!DOCTYPE html>
<html>
	<head>
	  	<meta charset="utf-8">
	  	<title>layuiTile</title>
	  	<meta name="renderer" content="webkit">
	  	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	  	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	  	<!-- 为兼容火狐此引用必须放在这里 -->
	 	<tag:header headerType="base"/>
	</head>
	<body>  
		<form class="layui-form" action="" method='POST' style="width:100%;">
		  	<div class="compre-content">
			<div class="layui-form-item">
		    	<div class="layui-inline">
			      	<label class="layui-form-label">AppName
			      	</label>
			      	<div class="layui-input-inline layui-input-70">
			        	<input type="text" name="appname" value="${entity.appname}" lay-verify="required" autocomplete="off" placeholder="请输入AppName" readonly="readonly" class="layui-input">
			      	</div>
		    	</div>
		  	</div>
			<div class="layui-form-item">
		    	<div class="layui-inline">
			      	<label class="layui-form-label">项目名称
			      	</label>
			      	<div class="layui-input-inline layui-input-70">
			        	<input type="text" name="title" value="${entity.title}" lay-verify="required" autocomplete="off" placeholder="请输入项目名称" readonly="readonly" class="layui-input">
			      	</div>
		    	</div>
		  	</div>
		  	</div>
		</form>
	</body>
	<script>
	layui.use('form', function(){
		  var form = layui.form,
		  		layer = layui.layer,
		  		$ = layui.$;
	});
	</script>
</html>