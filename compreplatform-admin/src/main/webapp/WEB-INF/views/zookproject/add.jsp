<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags"%>
<!DOCTYPE html>
<html>
	<head>
	  	<meta charset="utf-8">
	  	<title>layuiTile</title>
	  	<meta name="renderer" content="webkit">
	  	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	  	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	  	<!-- 为兼容火狐此引用必须放在这里 -->
	 	<tag:header headerType="base"/>
	</head>
	<body>  
		<form class="layui-form" action="" method='POST' style="width:100%;">
		  	<div class="compre-content">
			<div class="layui-form-item">
		    	<div class="layui-inline">
			      	<label class="layui-form-label"><i class='layui-must'>*</i>AppName
			      	</label>
			      	<div class="layui-input-inline layui-input-70">
			        	<input type="tel" name="appname" value="" maxlength="20" lay-verify="required|appname" autocomplete="off" placeholder="请输入AppName" class="layui-input">
			      	</div>
		    	</div>
		  	</div>
			<div class="layui-form-item">
		    	<div class="layui-inline">
			      	<label class="layui-form-label"><i class='layui-must'>*</i>项目名称
			      	</label>
			      	<div class="layui-input-inline layui-input-70">
			        	<input type="tel" name="title" value="" lay-verify="required" autocomplete="off" placeholder="请输入项目名称" class="layui-input">
			      	</div>
		    	</div>
		  	</div>
		  	</div>
		   	<!-- layui-footer layui-fixbar -->
		  	<div class="layui-form-item">
		  		<div class="layui-inline compre-footer">
			      	<label class="layui-form-label"></label>
			      	<button class="layui-btn compre-btn" lay-submit="" lay-filter="form-submit">提交</button>
			      	<button type="reset" class="layui-btn compre-btn compre-btn-reset layui-btn-primary">重置</button>
			    </div>
		  	</div>
		</form>
	</body>
	<script>
	layui.use('form', function(){
		  var form = layui.form,
		  		layer = layui.layer,
		  		$ = layui.$;
		//自定义验证规则  
		  form.verify({  
			  appname: [/^[a-z][a-z0-9_]*$/, '限制小写字母开头，且由字母、数字、下划线组成']  
		  });  
		  //监听提交
		  form.on('submit(form-submit)', function(data){
		  	$.post('${basePath}zookProject/add',data.field,function(result){
		  		if(result.code != 200){
		  			top.layer.msg(result.msg);
		  		}else{
		  			top.layer.closeAll();
		  			top.layer.msg(result.msg);
		  		}
		  	});
		    return false;
		  });
	});
	</script>
</html>