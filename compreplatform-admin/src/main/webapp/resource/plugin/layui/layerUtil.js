layui.use('layer', function(){
  var table = layui.table,
  		layer = layui.layer;
$.extend({
	layerMsg : function(opt) {
		var defaults = {
			msg : '服务异常,请联系系统管理员',
			icon : 2,//1=success,2=error,3=warning,4=lock,5=sad,6=smile,7=info
			shade : [ 0.3, 'rgb(0, 0, 0)' ],
			area: 'auto',
			timne : 1500,
			callBack: function(){}
		};
		var setts = $.extend(defaults, opt);
		var index= layer.msg(setts.msg, {
			icon : setts.icon,
			shade : [ 0.3, 'rgb(0, 0, 0)' ],
			area: setts.area,
			time: setts.timne,
			end: function(){
				if(opt.callBack){
					opt.callBack(index);
				}
			}
		})
		return index;
	},
	layerFloor : function(opt) {
		var defaults = {
			type : 1, //层样式
			offset : 'auto',
			skin : 'layui-layer-floor', //样式类名
			closeBtn : 1, //显示关闭按钮0=关闭，1=显示
			callBack : null,
			area : [ '450px', '270px' ], //宽高
			shift : 5, //展现方式
			shadeClose : true, //开启遮罩关闭
			title : '<span>系统信息</span>', //层标题
			fix : false, //不固定
			maxmin : false,
			cancel : function() {
				if (opt.callBack) {
					opt.callBack();
				}
			},
			content : '系统内容...'
		};
		var setts = $.extend(defaults, opt);
		layer.open(setts);
	},
	layerFrame : function(opt) {
		var defaults = {
			type: 2,
			title: '系统信息', //层标题
			skin: 'layui-layer-floor', //样式类名
			area: [ '700px', '530px' ],
			//fix: false, //不固定
			maxmin: false,
			content: '',
			end: function(){
				if (opt.callBack) {
					opt.callBack();
				}
			}
		};
		var setts = $.extend(defaults, opt);
		layer.open(setts);
	},
	layerConfirm : function(opt) {
	var defaults = {
		url : 'http://zsda.gov.cn:81/ArchiveWeb/',
		data : {},
		async : true,
		type : 'post',
		cache : false,
		dataType : 'json',
		title : '信息提示',
		area : [ '350px', '200px' ],
		successMsg : '操作成功',
		errorMsg : '操作失败',
		content : '确定执行?',
		callBack : null,
		enterBtn : '确定',
		cancelBtn : '取消',
		ifImg : "false",
	};
	var setts = $.extend(defaults, opt);
	//询问框
	layer.confirm(
					setts.ifImg == 'true' ? "<img src='${basePath}resource/common/scripts/plug-in/layer/confirm.gif' style='position: relative;top: 20%;padding-right: 10px;'></img>"
							+ setts.content
							: setts.content,
					{
						btn : [ setts.enterBtn, setts.cancelBtn ], //按钮
						title : setts.title,
						area : setts.area,
					},
					function() {
						layer.closeAll();
						//确定操作
						$.ajax({
									url : setts.url,
									data : setts.data,
									async : setts.async,
									type : setts.type,
									cache : setts.cache,
									dataType : setts.dataType,
									success : function(data) {
										if (data
												&& data.success) {
											var successMsg = data.msg
													|| setts.successMsg;
											if (setts.callBack)
												setts.callBack();
											tip(successMsg);
										} else {
											var errorMsg = data.msg
													|| setts.errorMsg;
											tip(errorMsg);
										}
									},
									error : function() {
										tip('服务器异常,请联系管理员');
									}
								});
					}, function() {
						//取消操作
					});
	},
	layerLoading : function(opt) {
		var defaults = {
			type: 1,
			time: 3000,
			shade: [ 0.3, '#000' ]
		};
		var setts = $.extend(defaults, opt);
		var index = layer.load(setts.type, {
			shade: setts.shade,
			time: setts.time
		//0.1透明度的白色背景
			});
			return index;
		}
	});
});