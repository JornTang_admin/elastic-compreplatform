<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags"%>
<tag:header headerType="base"/>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Elastic Compreplatform</title>
    <link rel="stylesheet" href="${basePath}/resource/common/login/login.css">
</head>
<body>
<div id="particles-js" class="login" style="width: 100%;height: 100%;left:0%;top:0%;margin: 0px;background-color: transparent;"></div>
<!-- particles.js container style="filter:alpha(Opacity=60);-moz-opacity:0.6;opacity: 0.6;"-->
<div class="layui-carousel video_mask" id="login_carousel" style="z-index: 90;">
    <div carousel-item>
        <div class="carousel_div1"></div>
        <div class="carousel_div2"></div>
        <div class="carousel_div3"></div>
        <div class="carousel_div4"></div>
        <div class="carousel_div5"></div>
    </div>
</div>
 <div class="login layui-anim layui-anim-up">
        <h1>Elastic Compreplatform</h1></p>
        <form class="layui-form" action="" method="post">
            <div class="layui-form-item">
                <input type="text" name="username" lay-verify="required" placeholder="请输入账号" autocomplete="off"  value="admin" class="layui-input">
            </div>
            <div class="layui-form-item">
                <input type="password" name="password" lay-verify="required" placeholder="请输入密码" autocomplete="off" value="" class="layui-input">
            </div>
            <div class="layui-form-item form_code">
                <input class="layui-input" name="code" placeholder="验证码" lay-verify="required" type="text" autocomplete="off">
                <div class="code"><img src="${basePath}user/anon/captcha.do" width="116" height="34"></div>
            </div>
			<div class="layui-form-item">
				<input type="checkbox" name="rememberme" lay-skin="primary" title="<span style='color:#ffffff;'>记住我</span>" checked="" >
			</div>
            <button class="layui-btn login_btn layui-anim layui-anim-scale" lay-submit="" lay-filter="login">登陆系统</button>
        </form>
    </div>
<script>
	/**重新生成验证码*/
    function reqCaptcha() {
        var url = "${basePath}user/anon/captcha.do?nocache=" + new Date().getTime()
        $('.code img').attr("src",url)
    }
    /**点击验证码重新生成*/
    $('.code img').on('click', function () {
        reqCaptcha();
    });
    layui.config({
        base : "resource/jquery/"
    }).use(['form','layer','jquery','common','carousel', 'element'], function () {
        var $ = layui.jquery,
            form = layui.form,
            common = layui.common,
            carousel = layui.carousel,
            layer = layui.layer;
        /**背景图片轮播*/
        carousel.render({
            elem: '#login_carousel',
            width: '100%',
            height: '100%',
            interval:10000,
            arrow: 'none',
            anim: 'fade',
            indicator:'none'
        });
   
        layer.open({
            type: 1
            ,title: false //不显示标题栏
            ,closeBtn: false
            ,shade: 0.8
            ,id: 'LAY_layuipro' //设定一个id，防止重复弹出
            ,btn: ['确定']
            ,btnAlign: 'c'
            ,moveType: 1 //拖拽模式，0或者1
            ,content: '<div style="padding: 30px; line-height: 22px; background-color: #393D49; color: #fff; font-weight: 300;">' +
                        '用户名：admin<br>' +
                        '密&nbsp;&nbsp;&nbsp;码：admin<br>' +
                        '开源地址：<br>' +
                        '<a href="https://gitee.com/JornTang/elastic-compreplatform/" target="_blank" style="color: greenyellow">https://gitee.com/JornTang/elastic-compreplatform(点击跳转)</a></div>'
            ,success: function(layero){

            }
        })
       
        /**监听登陆提交*/
        form.on('submit(login)', function (data) {
            //弹出loading
            var loginIndex = layer.msg('登陆中，请稍候', {icon: 16, time: false, shade: 0.8});
            //记录ajax请求返回值
            var ajaxReturnData;
            //登陆验证
            $.ajax({
                url: '${basePath}user/anon/loginCheck.do',
                type: 'post',
                async: false,
                data: data.field,
                success: function (data) {
                    ajaxReturnData = data;
                }
            });
            layer.close(loginIndex);
            //登陆成功
            if (ajaxReturnData.code == 0000) {
                window.location.href="${basePath}";
                return false;
            } else {
                common.cmsLayErrorMsg(ajaxReturnData.msg);
                reqCaptcha();
                return false;
            }
        });

    });
</script>
<tag:header headerType="particles"/>
</body>
</html>