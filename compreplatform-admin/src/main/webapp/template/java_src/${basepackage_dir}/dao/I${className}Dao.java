<#assign className = table.className>   
<#assign classNameLower = className?uncap_first>   
package ${basepackage}.dao;

import org.elastic.compreplatform.common.dao.BaseDao;
import com.github.pagehelper.Page;

${basepackage}.dao;

<#include "/java_imports.include">

public interface I${className}Dao extends BaseDao<${className}>{
	
	<#list table.columns as column>
	<#if column.unique && !column.pk>
	public ${className} getBy${column.columnName}(${column.javaType} v) {
		return (${className})getSqlSessionTemplate().selectOne("${className}.getBy${column.columnName}",v);
	}	
	
	</#if>
	</#list>
	/**
	 * 分页查询
	 */
	public Page<${className}> pagelist(${className}Query query);
}
