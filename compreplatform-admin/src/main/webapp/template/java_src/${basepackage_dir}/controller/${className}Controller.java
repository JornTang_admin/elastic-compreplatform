<#include "/custom.include">
<#include "/java_copyright.include">
<#assign className = table.className>   
<#assign classNameLower = className?uncap_first>
<#assign classNameLowerCase = table.classNameLowerCase> 
package ${basepackage}.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.elastic.compreplatform.admin.util.PageHelperUtil;
import org.elastic.compreplatform.common.constant.RespEnum;
import org.elastic.compreplatform.common.model.PageHelper;
import org.elastic.compreplatform.common.util.RespMsgUtil;
import org.elastic.compreplatform.common.util.SeqUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import com.github.pagehelper.Page;
import ${basepackage}.service.impl.${className}ServiceImpl;

@Controller
@RequestMapping("/${classNameLower}")
public class ${className}Controller{
	private static Logger log = LoggerFactory.getLogger(${className}Controller.class); 
	/**
	 * 注解注入
	 */
	@Resource
	private ${className}ServiceImpl ${classNameLower}Service;
	
	public ${className}Controller() {
	}
	/** 
	 * 进入列表页面
	 **/
	@RequestMapping("/toList")
	public String toList(Model model) {
		return "${classNameLowerCase}/list";
	}
	/** 
	 * 分页查询
	 **/
	@RequestMapping("/pageList")
	@ResponseBody
	public PageHelper pagelist(HttpServletRequest request, HttpServletResponse response, ${className}Query query, PageHelper page) {
		Page<${className}> list = ${classNameLower}Service.pagelist(query, page);
        return PageHelperUtil.doHandlePage(list);
	}
	/** 
	 * 进入添加页面
	 **/
	@RequestMapping("/toAdd")
	public String toAdd(Model model) {
		return "${classNameLowerCase}/add";
	}
	/** 
	 * 添加
	 **/
	@RequestMapping("/add")
	@ResponseBody
	public RespMsg add(HttpServletRequest request, HttpServletResponse response, Model model, ${className} entity) {
		entity.set${table.idColumn.columnName}(SeqUtil.getSeq());
		boolean isOk = ${classNameLower}Service.insert(entity);
		return isOk?RespMsgUtil.returnMsg(RespEnum.SUCCESS): RespMsgUtil.returnMsg(RespEnum.ERROR);
	}
	/** 
	 * 进入修改页面
	 **/
	@RequestMapping("/toUpdate")
	public String toUpdate(Model model, ${table.idColumn.javaType} id) {
		${className} entity = ${classNameLower}Service.selectById(id);
		model.addAttribute("entity", entity);
		return "${classNameLowerCase}/update";
	}
	/** 
	 * 修改
	 **/
	@RequestMapping("/update")
	@ResponseBody
	public RespMsg update(HttpServletRequest request, HttpServletResponse response, ${className} entity) {
		boolean isOk = ${classNameLower}Service.updateById(entity);
		return isOk?RespMsgUtil.returnMsg(RespEnum.SUCCESS): RespMsgUtil.returnMsg(RespEnum.ERROR);
	}
	/** 
	 * 删除
	 **/
	@RequestMapping("/delete")
	@ResponseBody
	public RespMsg delete(HttpServletRequest request, HttpServletResponse response, ${table.idColumn.javaType} id) throws Exception {
		boolean isOk = ${classNameLower}Service.deleteById(id);
		return isOk?RespMsgUtil.returnMsg(RespEnum.SUCCESS): RespMsgUtil.returnMsg(RespEnum.ERROR);
	}
	/** 
	 * 查看详情
	 **/
	@RequestMapping("/show")
	public String delete(HttpServletRequest request, HttpServletResponse response, Model model, ${table.idColumn.javaType} id) throws Exception {
		${className} entity = ${classNameLower}Service.selectById(id);
		model.addAttribute("entity", entity);
		return "${classNameLowerCase}/show";
	}
}

<#macro generateIdParameter>
	<#if table.compositeId>
		${className}Id id = new ${className}Id();
		bind(request, id);
	<#else>
		<#list table.compositeIdColumns as column>
		${column.javaType} id = new ${column.javaType}(request.getParameter("${column.columnNameLower}"));
		</#list>
	</#if>
</#macro>