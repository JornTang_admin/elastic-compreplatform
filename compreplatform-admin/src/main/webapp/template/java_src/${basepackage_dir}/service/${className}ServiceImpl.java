<#include "/java_copyright.include">
<#assign className = table.className>   
<#assign classNameLower = className?uncap_first> 
package ${basepackage}.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.elastic.compreplatform.common.model.PageHelper;
import ${basepackage}.vo.query.${className}Query;
import com.github.pagehelper.Page;

@Service
public class ${className}ServiceImpl extends BaseServiceImpl<${className}>{
	
	@Resource
	private I${className}Dao ${classNameLower}Dao;
	
<#list table.columns as column>
	<#if column.unique && !column.pk>
	@Transactional(readOnly=true)
	public ${className} getBy${column.columnName}(${column.javaType} v) {
		return ${classNameLower}Dao.getBy${column.columnName}(v);
	}	
	
	</#if>
</#list>
	/**
	 * 分页查询
	 */
	public Page<${className}> pagelist(${className}Query query, PageHelper page){
		com.github.pagehelper.PageHelper.startPage(page.getPage(), page.getLimit());
		return ${classNameLower}Dao.pagelist(query);
	}
}
