<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<#include "/macro.include"/> 
<#include "/custom.include"/> 
<#assign className = table.className>   
<#assign classNameLower = className?uncap_first>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags"%>
<!DOCTYPE html>
<html>
	<head>
	  	<meta charset="utf-8">
	  	<title>layuiTile</title>
	  	<meta name="renderer" content="webkit">
	  	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	  	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	  	<!-- 为兼容火狐此引用必须放在这里 -->
	 	<tag:header headerType="base"/>
	</head>
	<body>  
		<form class="layui-form" action="" method='POST' style="width:100%;">
		  	<div class="compre-content">
		  	<#list table.columns as column>
			<#if !column.htmlHidden>
			<div class="layui-form-item">
		    	<div class="layui-inline">
			      	<label class="layui-form-label">${column.remarks}
			      	</label>
			      	<div class="layui-input-inline layui-input-70">
			        	<input type="text" name="${column.columnNameFirstLower}" value="${r'${entity.'}${column.columnNameFirstLower}${r'}'}" lay-verify="<#if !column.isNullable()>required</#if>" autocomplete="off" placeholder="请输入${column.remarks}" readonly="readonly" class="layui-input">
			      	</div>
		    	</div>
		  	</div>
			</#if>
			</#list>
		  	</div>
		</form>
	</body>
	<script>
	layui.use('form', function(){
		  var form = layui.form,
		  		layer = layui.layer,
		  		$ = layui.$;
	});
	</script>
</html>