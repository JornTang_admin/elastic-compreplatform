package org.elastic.compreplatform.conf.core.listener;

/**
 * ClassName: ConfListener 
 * @Description: 配置监听器
 * @author JornTang
 * @date 2018年3月4日
 */
public interface ConfListener {

    /**
     * invoke when xxl conf change
     *
     * @param key
     */
    public void onChange(String key, String value) throws Exception;

}
