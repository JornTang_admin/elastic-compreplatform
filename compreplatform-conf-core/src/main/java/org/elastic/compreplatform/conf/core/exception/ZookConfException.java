package org.elastic.compreplatform.conf.core.exception;

/**
 * ClassName: ZookConfException 
 * @Description: 自定义配置异常
 * @author JornTang
 * @date 2018年3月4日
 */
public class ZookConfException extends RuntimeException {

    private static final long serialVersionUID = 42L;

    public ZookConfException(String msg) {
        super(msg);
    }

    public ZookConfException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public ZookConfException(Throwable cause) {
        super(cause);
    }

}
