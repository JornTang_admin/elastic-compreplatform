package org.elastic.compreplatform.conf.core.listener.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.elastic.compreplatform.conf.core.listener.ConfListener;
import org.elastic.compreplatform.conf.core.spring.ZookConfFactory;

/**
 * ClassName: BeanRefreshConfListener 
 * @Description: 更新配置监听
 * @author JornTang
 * @date 2018年3月4日
 */
public class BeanRefreshConfListener implements ConfListener {


    // ---------------------- listener ----------------------

    // object + field
    public static class BeanField{
        private String beanName;
        private String property;

        public BeanField() {
        }

        public BeanField(String beanName, String property) {
            this.beanName = beanName;
            this.property = property;
        }

        public String getBeanName() {
            return beanName;
        }

        public void setBeanName(String beanName) {
            this.beanName = beanName;
        }

        public String getProperty() {
            return property;
        }

        public void setProperty(String property) {
            this.property = property;
        }
    }

    // key : object-field[]
    private static Map<String, List<BeanField>> key2BeanField = new ConcurrentHashMap<String, List<BeanField>>();
    public static void addBeanField(String key, BeanField beanField){
        List<BeanField> beanFieldList = key2BeanField.get(key);
        if (beanFieldList == null) {
            beanFieldList = new ArrayList<BeanField>();
            key2BeanField.put(key, beanFieldList);
        }
        for (BeanField item: beanFieldList) {
            if (item.getBeanName() == beanField.getBeanName() && item.getProperty()==beanField.getProperty()) {
                return;
            }
        }
        beanFieldList.add(beanField);
    }

    // ---------------------- onChange ----------------------

    @Override
    public void onChange(String key, String value) throws Exception {
        List<BeanField> beanFieldList = key2BeanField.get(key);
        if (beanFieldList!=null && beanFieldList.size()>0) {
            for (BeanField beanField: beanFieldList) {
                ZookConfFactory.refreshBeanField(beanField, value);
            }
        }
    }
}
