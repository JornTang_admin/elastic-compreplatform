package org.elastic.compreplatform.conf.core.listener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * ClassName: ConfListenerFactory 
 * @Description: 配置监听工厂
 * @author JornTang
 * @date 2018年3月4日
 */
public class ConfListenerFactory {
    private static Logger logger = LoggerFactory.getLogger(ConfListenerFactory.class);

    /**
     * xxl conf listener repository
     */
    private static ConcurrentHashMap<String, List<ConfListener>> keyListenerRepository = new ConcurrentHashMap<String, List<ConfListener>>();
    private static List<ConfListener> noKeyConfListener = Collections.synchronizedList(new ArrayList<ConfListener>());

    /**
     * add listener with xxl conf change
     *
     * @param key   empty will listener all key
     * @param ConfListener
     * @return
     */
    public static boolean addListener(String key, ConfListener confListener){
        if (confListener == null) {
            return false;
        }
        if (key==null || key.trim().length()==0) {
            noKeyConfListener.add(confListener);
            return true;
        } else {
            List<ConfListener> listeners = keyListenerRepository.get(key);
            if (listeners == null) {
                listeners = new ArrayList<ConfListener>();
                keyListenerRepository.put(key, listeners);
            }
            listeners.add(confListener);
            return true;
        }
    }

    /**
     * invoke listener on xxl conf change
     *
     * @param key
     */
    public static void onChange(String key, String value){
        if (key==null || key.trim().length()==0) {
            return;
        }
        List<ConfListener> keyListeners = keyListenerRepository.get(key);
        if (keyListeners!=null && keyListeners.size()>0) {
            for (ConfListener listener : keyListeners) {
                try {
                    listener.onChange(key, value);
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }
            }
        }
        if (noKeyConfListener.size() > 0) {
            for (ConfListener confListener: noKeyConfListener) {
                try {
                    confListener.onChange(key, value);
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }
            }
        }
    }

}
