package org.elastic.compreplatform.conf.core.core;

import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import org.elastic.compreplatform.conf.core.env.Environment;
import org.elastic.compreplatform.conf.core.util.PropUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * local prop conf
 *
 * @author xuxueli 2018-02-01 19:25:52
 */
public class ConfPropConf {
    private static Logger logger = LoggerFactory.getLogger(ConfPropConf.class);

    private static final ConcurrentHashMap<String, String> propConf = new ConcurrentHashMap<String, String>();
    private static void init(){
        // local prop
        if (Environment.LOCAL_PROP!=null && Environment.LOCAL_PROP.trim().length()>0) {
            Properties localProp = PropUtil.loadProp(Environment.LOCAL_PROP);
            if (localProp!=null && localProp.stringPropertyNames()!=null && localProp.stringPropertyNames().size()>0) {
                for (String key: localProp.stringPropertyNames()) {
                    propConf.put(key, localProp.getProperty(key));
                }
            }
        }

        logger.info(">>>>>>>>>> xxl-conf, XxlConfPropConf init success.");
    }
    static {
        init();
    }


    /**
     * get conf from local prop
     *
     * @param key
     * @return
     */
    public static String get(String key){
        return propConf.get(key);
    }

}
