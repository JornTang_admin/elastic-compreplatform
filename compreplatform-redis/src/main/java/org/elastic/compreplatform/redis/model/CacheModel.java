package org.elastic.compreplatform.redis.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * ClassName: CacheModel 
 * @Description: 缓存model
 * @author JornTang
 * @date 2018年2月28日
 */
public class CacheModel implements Serializable{
	private static final long serialVersionUID = 5454155825314635342L;
	private Long dbId;
	private Integer dbIndex;
	private String key;
    private String keyValue;
    private String keyType;
    private List<String> list;
    private Set<String> set;
    private List<Map<String, Object>> listMap;
    private String[] valuek;
    private String[] valuev;
    private String expire;
    private String keys;
    
	public String getKeys() {
		return keys;
	}
	public void setKeys(String keys) {
		this.keys = keys;
	}
	public Long getDbId() {
		return dbId;
	}
	public void setDbId(Long dbId) {
		this.dbId = dbId;
	}
	public Integer getDbIndex() {
		return dbIndex;
	}
	public void setDbIndex(Integer dbIndex) {
		this.dbIndex = dbIndex;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getKeyValue() {
		return keyValue;
	}
	public void setKeyValue(String keyValue) {
		this.keyValue = keyValue;
	}
	public String getKeyType() {
		return keyType;
	}
	public void setKeyType(String keyType) {
		this.keyType = keyType;
	}
	public List<String> getList() {
		return list;
	}
	public void setList(List<String> list) {
		this.list = list;
	}
	public Set<String> getSet() {
		return set;
	}
	public void setSet(Set<String> set) {
		this.set = set;
	}
	public List<Map<String, Object>> getListMap() {
		return listMap;
	}
	public void setListMap(List<Map<String, Object>> listMap) {
		this.listMap = listMap;
	}
	public String[] getValuek() {
		return valuek;
	}
	public void setValuek(String[] valuek) {
		this.valuek = valuek;
	}
	public String[] getValuev() {
		return valuev;
	}
	public void setValuev(String[] valuev) {
		this.valuev = valuev;
	}
	public String getExpire() {
		return expire;
	}
	public void setExpire(String expire) {
		this.expire = expire;
	}
}

