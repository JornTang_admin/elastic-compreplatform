package org.elastic.compreplatform.redis.model;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.elastic.compreplatform.common.model.BaseModel;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


@TableName("sys_config")
public class RedisModel extends BaseModel{
	private static final long serialVersionUID = 5454155825314635342L;
	
	//alias
	public static final String TABLE_ALIAS = "SysConfig";
	public static final String ALIAS_ID = "主键id";
	public static final String ALIAS_DB_DESC = "描述";
	public static final String ALIAS_DB_TYPE = "数据库类型";
	public static final String ALIAS_DB_URL = "ip地址";
	public static final String ALIAS_DB_SCHEMA = "oracle需要配置scheme";
	public static final String ALIAS_USERNAME = "用户名";
	public static final String ALIAS_PASSWORD = "密码";
	public static final String ALIAS_DB_DRIVER = "数据库驱动";
	
	//date formats
	public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	
	//Can be used directly: @Length (max=50, message= "username length can not be greater than 50") display error messages
	//columns START
    /**
     * 主键id       db_column: id 
     */	
	
	@TableId
	private java.lang.Long id;
    /**
     * 描述       db_column: db_desc 
     */	
	@NotBlank @Length(max=50)
	private java.lang.String dbDesc;
    /**
     * 数据库类型       db_column: db_type 
     */	
	@NotBlank @Length(max=50)
	private java.lang.String dbType;
    /**
     * ip地址       db_column: db_url 
     */	
	@NotBlank @Length(max=100)
	private java.lang.String dbUrl;
	/**
     * 端口      db_column: db_port
     */	
	@NotBlank @Length(max=10)
	private java.lang.Integer dbPort;
    /**
     * oracle需要配置scheme       db_column: db_schema 
     */	
	@Length(max=50)
	private java.lang.String dbSchema;
    /**
     * 用户名       db_column: username 
     */	
	@NotBlank @Length(max=50)
	private java.lang.String username;
    /**
     * 密码       db_column: password 
     */	
	@NotBlank @Length(max=50)
	private java.lang.String password;
    /**
     * 数据库驱动       db_column: db_driver 
     */	
	@NotBlank @Length(max=100)
	private java.lang.String dbDriver;
	//columns END

	public RedisModel(){
	}

	public RedisModel(
		java.lang.Long id
	){
		this.id = id;
	}

	public void setId(java.lang.Long value) {
		this.id = value;
	}
	
	public java.lang.Long getId() {
		return this.id;
	}
	public void setDbDesc(java.lang.String value) {
		this.dbDesc = value;
	}
	
	public java.lang.String getDbDesc() {
		return this.dbDesc;
	}
	public void setDbType(java.lang.String value) {
		this.dbType = value;
	}
	
	public java.lang.String getDbType() {
		return this.dbType;
	}
	public void setDbUrl(java.lang.String value) {
		this.dbUrl = value;
	}
	
	public java.lang.String getDbUrl() {
		return this.dbUrl;
	}
	
	public java.lang.Integer getDbPort() {
		return dbPort;
	}

	public void setDbPort(java.lang.Integer dbPort) {
		this.dbPort = dbPort;
	}

	public void setDbSchema(java.lang.String value) {
		this.dbSchema = value;
	}
	
	public java.lang.String getDbSchema() {
		return this.dbSchema;
	}
	public void setUsername(java.lang.String value) {
		this.username = value;
	}
	
	public java.lang.String getUsername() {
		return this.username;
	}
	public void setPassword(java.lang.String value) {
		this.password = value;
	}
	
	public java.lang.String getPassword() {
		return this.password;
	}
	public void setDbDriver(java.lang.String value) {
		this.dbDriver = value;
	}
	
	public java.lang.String getDbDriver() {
		return this.dbDriver;
	}

	public String toString() {
		return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
			.append("Id",getId())
			.append("DbDesc",getDbDesc())
			.append("DbType",getDbType())
			.append("DbUrl",getDbUrl())
			.append("DbSchema",getDbSchema())
			.append("Username",getUsername())
			.append("Password",getPassword())
			.append("DbDriver",getDbDriver())
			.toString();
	}
	
	public int hashCode() {
		return new HashCodeBuilder()
			.append(getId())
			.toHashCode();
	}
	
	public boolean equals(Object obj) {
		if(obj instanceof RedisModel == false) return false;
		if(this == obj) return true;
		RedisModel other = (RedisModel)obj;
		return new EqualsBuilder()
			.append(getId(),other.getId())
			.isEquals();
	}
}

