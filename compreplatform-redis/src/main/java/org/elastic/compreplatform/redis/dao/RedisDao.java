package org.elastic.compreplatform.redis.dao;

import org.elastic.compreplatform.common.dao.BaseDao;
import org.elastic.compreplatform.redis.model.RedisModel;
import org.elastic.compreplatform.redis.vo.query.RedisQuery;

import com.github.pagehelper.Page;

public interface RedisDao extends BaseDao<RedisModel>{
	/**
	 * 分页查询
	 */
	public Page<RedisModel> pagelist(RedisQuery query);
}
