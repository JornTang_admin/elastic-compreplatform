package org.elastic.compreplatform.redis.service;

import javax.annotation.Resource;

import org.elastic.compreplatform.common.model.PageHelper;
import org.elastic.compreplatform.common.service.impl.BaseServiceImpl;
import org.elastic.compreplatform.redis.dao.RedisDao;
import org.elastic.compreplatform.redis.model.RedisModel;
import org.elastic.compreplatform.redis.vo.query.RedisQuery;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;

@Service
public class RedisServiceImpl extends BaseServiceImpl<RedisModel>{
	@Resource
	private RedisDao redisDao;
	/**
	 * 分页查询
	 */
	public Page<RedisModel> pagelist(RedisQuery query, PageHelper page){
		com.github.pagehelper.PageHelper.startPage(page.getPage(), page.getLimit());
		return redisDao.pagelist(query);
	}
}
