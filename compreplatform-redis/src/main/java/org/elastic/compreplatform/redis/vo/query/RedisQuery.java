/*
 * Copyright © 2018 JornTang personal. 
 *
 * All right reserved. 
 *
 * Author JornTang 
 *
 * Date 2018-02-28
 */
package org.elastic.compreplatform.redis.vo.query;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.elastic.compreplatform.common.model.BaseModel;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

public class RedisQuery extends BaseModel {
    private static final long serialVersionUID = 3148176768559230877L;
    

	/** 主键id */
	private java.lang.Long id;
	/** 描述 */
	private java.lang.String dbDesc;
	/** 数据库类型 */
	private java.lang.String dbType;
	/** ip地址 */
	private java.lang.String dbUrl;
	/** 端口*/
	private java.lang.Integer dbPort;
	/** oracle需要配置scheme */
	private java.lang.String dbSchema;
	/** 用户名 */
	private java.lang.String username;
	/** 密码 */
	private java.lang.String password;
	/** 数据库驱动 */
	private java.lang.String dbDriver;

	public java.lang.Long getId() {
		return this.id;
	}
	
	public void setId(java.lang.Long value) {
		this.id = value;
	}
	
	public java.lang.String getDbDesc() {
		return this.dbDesc;
	}
	
	public void setDbDesc(java.lang.String value) {
		this.dbDesc = value;
	}
	
	public java.lang.String getDbType() {
		return this.dbType;
	}
	
	public void setDbType(java.lang.String value) {
		this.dbType = value;
	}
	
	public java.lang.String getDbUrl() {
		return this.dbUrl;
	}
	
	public void setDbUrl(java.lang.String value) {
		this.dbUrl = value;
	}
	
	public java.lang.Integer getDbPort() {
		return dbPort;
	}

	public void setDbPort(java.lang.Integer dbPort) {
		this.dbPort = dbPort;
	}

	public java.lang.String getDbSchema() {
		return this.dbSchema;
	}
	
	public void setDbSchema(java.lang.String value) {
		this.dbSchema = value;
	}
	
	public java.lang.String getUsername() {
		return this.username;
	}
	
	public void setUsername(java.lang.String value) {
		this.username = value;
	}
	
	public java.lang.String getPassword() {
		return this.password;
	}
	
	public void setPassword(java.lang.String value) {
		this.password = value;
	}
	
	public java.lang.String getDbDriver() {
		return this.dbDriver;
	}
	
	public void setDbDriver(java.lang.String value) {
		this.dbDriver = value;
	}
	

	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.MULTI_LINE_STYLE);
	}
	
}

