package org.elastic.compreplatform.common.service;

import com.baomidou.mybatisplus.service.IService;
/**
 * ClassName: IBaseService 
 * @Description: 基础服务接口
 * @author JornTang
 * @date 2018年2月25日
 */
public interface IBaseService<T> extends IService<T>{

}
