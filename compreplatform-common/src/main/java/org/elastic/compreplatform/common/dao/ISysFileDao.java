package org.elastic.compreplatform.common.dao;

import org.elastic.compreplatform.common.model.SysFile;
import org.elastic.compreplatform.common.vo.query.SysFileQuery;

import com.github.pagehelper.Page;

/**
 * ClassName: 
 * @Description: 
 * @author 
 * @date 
 */
public interface ISysFileDao extends BaseDao<SysFile>{
	
	/**
	 * 分页查询
	 */
	public Page<SysFile> pagelist(SysFileQuery query);
}
