package org.elastic.compreplatform.common.handler;

import org.apache.ibatis.reflection.MetaObject;

import com.baomidou.mybatisplus.mapper.MetaObjectHandler;
/**
 * ClassName: CustomMetaObjectHandler 
 * @Description: 自定义填充处理器
 * @author JornTang
 * @date 2018年2月25日
 */
public class CustomMetaObjectHandler extends MetaObjectHandler{
	@Override
    public void insertFill(MetaObject metaObject) {
        //this.setFieldValByName("ctime", new Date(), metaObject);
    }

    @Override
    public boolean openUpdateFill() {
        return false;
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        // 关闭更新填充、这里不执行
    }
}
