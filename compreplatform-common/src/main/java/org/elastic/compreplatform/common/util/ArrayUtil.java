package org.elastic.compreplatform.common.util;

import java.util.Set;
import java.util.TreeSet;

/**
 * ClassName: ArrayUtil 
 * @Description: 集合工具类
 * @author JornTang
 * @date 2018年1月28日
 */
public class ArrayUtil {
	/**
	 * 把数组转换成set
	 * @param array
	 * @return
	 */
	public static Set<?> array2Set(Object[] array) {
		Set<Object> set = new TreeSet<Object>();
		for (Object id : array) {
			if(null != id){
				set.add(id);
			}
		}
		return set;
	}
}
