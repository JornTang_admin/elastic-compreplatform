package org.elastic.compreplatform.common.constant;

/**
 * ClassName: ResponseDef 
 * @Description: 返回消息常量定义
 * @author JornTang
 * @date 2018年1月28日
 */
public enum RespEnum {
	// 成功
	SUCCESS("200","操作成功"),
	// 分页查询状态
	PAGE_SUCCESS("0","操作成功"),
	//失败
	ERROR("500", "操作失败"),

	// 成功
	GLOBAL_SUCCESS("0000","成功"),
	//失败
	GLOBAL_ERROR("9999", "系统正在维护中,请稍后再试!"),

	/**通用响应信息 */
    GLOBAL_LOGIN_NAME_NULL("0501","用户名不能为空"),
    GLOBAL_LOGIN_PASS_NULL("0502","密码不能为空"),

	GLOBAL_LOGIN_FAIL("0503","用户名或密码不匹配"),

	GLOBAL_LOGIN_ERROR("0504","系统登录异常"),

    GLOBAL_CAPTCHA_NULL("0505","验证码不能为空"),

    GLOBAL_CAPTCHA_ERROR("0506","验证码输入错误"),

	RES_SAVE_ERROR("1501","菜单资源信息保存失败"),
	
	ROLE_SAVE_ERROR("1502","角色信息保存失败"),
	
	USER_SAVE_ERROR("1503","用户信息保存失败"),
	
	USER_ROLE_SAVE_ERROR("1504","用户分配角色信息失败"),
	
	USER_FAIL_ERROR("1505","失效用户失败,程序异常"),
	
	ROLE_FAILK_ERROR("1506","失效角色失败,程序异常"),
	
	RES_FAILK_ERROR("1507","失效资源失败,程序异常"),
	
    USER_LOGIN_NAME_EXIST("1508","用户账号已存在，请重新输入"),
    
    ROLE_RES_SAVE_ERROR("1509","角色分配菜单失败"),
    
    ROLE_NAME_EXIST("1508","角色名称已存在，请重新输入"),
    
    //  请求失败
    REQUEST_FAIL("0000", "fail"),
    //  请求成功
    REQUEST_SUCCESS(    "1111", "successfully"),
    //  邮件发送成功
    MAIL_SEND_SUCCESS(  "1001", "send mail successfully"),
    //  认证成功
    AUTH_SUCCESS(       "1002", "auth successfully"),
    //  已认证
    HAVE_AUTH(          "1003", "have authentication"),
    //  邮箱验证成功
    EMAIL_VALID(        "1004", "email valid"),

    //  请求没有授权
    REQUEST_UNAUTHORIZED(   "3333", "fail"),
    //  没有权限获取数据
    GET_DATA_UNAUTHORIZED(  "3333", "unauthorized"),

    //  邮件发送失败
    MAIL_SEND_FAIL(     "4001", "send mail fairly"),
    //  认证失败
    AUTH_FAIL(          "4002", "auth fairly"),
    //  链接失效
    AUTH_LINK_TIMEOUT(  "4003", "link failure"),
    //  邮箱验证失败
    EMAIL_NOT_VALID(    "4004", "email not valid"),
    //  未认证
    HAVE_NOT_AUTH(      "4005", "have not authentication"),


    // 数据已存在
    DATA_EXIT("5000", "data exist");

	RespEnum(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	private String code;

	private String msg;
	

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
}
