package org.elastic.compreplatform.common.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


/**
 * ClassName: DateUtil 
 * @Description: 时间工具类
 * @author JornTang
 * @date 2018年1月28日
 */
public class DateUtil {
	public static void main(String[] args) {
		try {
			System.err.println(beforeHour(1));
			System.err.println(beforeDay(1));
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	/**
	 * @Description: 获取时间的年、月、日
	 * @param d
	 * @param type
	 * @return   
	 * @return int  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月9日
	 */
	public static int filterTime(Date d, int type){
		Calendar cal = Calendar.getInstance();
		cal.setTime(d);
		//年
		if(type == 1){
			return cal.get(Calendar.YEAR);
		//月
		}else if(type == 2){
			return (cal.get(Calendar.MONTH)+1);
		//日
		}else if(type == 3){
			return cal.get(Calendar.DAY_OF_MONTH);
		}
		return -1;
	}
	/**
	 * @Description: 获取上个月时间
	 * @return   
	 * @return String  
	 * @throws ParseException 
	 * @throws
	 * @author JornTang
	 * @date 2018年2月9日
	 */
	public static Date previousMonth(){
		Calendar cal = Calendar.getInstance(); 
		cal.setTime(new Date());
		cal.add(Calendar.MONTH, -1);
		return cal.getTime();
	}
	/**
	 * @Description: 获取某年、月份的第一天
	 * @return   
	 * @return String  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月9日
	 */
	public static String monthFirstDay(int year, int month){
		Calendar cal = Calendar.getInstance();  
        //设置年份  
        cal.set(Calendar.YEAR,year);  
        //设置月份  
        cal.set(Calendar.MONTH, month-1);  
        //获取某月最小天数  
        int firstDay = cal.getActualMinimum(Calendar.DAY_OF_MONTH);  
        //设置日历中月份的最小天数  
        cal.set(Calendar.DAY_OF_MONTH, firstDay);  
        //格式化日期  
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");  
        String firstDayOfMonth = sdf.format(cal.getTime());  
        return firstDayOfMonth; 
	}
	/**
	 * @Description: 获取某年、月份的最后一天
	 * @return   
	 * @return String  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月9日
	 */
	public static String monthLastDay(int year,int month){
		Calendar cal = Calendar.getInstance();  
        //设置年份  
        cal.set(Calendar.YEAR,year);  
        //设置月份  
        cal.set(Calendar.MONTH, month-1);  
        //获取某月最大天数  
        int lastDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);  
        //设置日历中月份的最大天数  
        cal.set(Calendar.DAY_OF_MONTH, lastDay);  
        //格式化日期  
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");  
        String lastDayOfMonth = sdf.format(cal.getTime());  
        return lastDayOfMonth;
	}
	/**
	 * @Description: 获取距离当前时间的小时
	 * @param hour 小时数
	 * @return   
	 * @return String  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月9日
	 */
	public static String beforeHour(int hour){
		Calendar calendar = Calendar.getInstance();
		/* HOUR_OF_DAY 指示一天中的小时 */
		calendar.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY) - hour);
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return df.format(calendar.getTime());
	}
	/**
	 * @Description: 获取距离当前时间的小时
	 * @param day 天数
	 * @return   
	 * @return String  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月9日
	 */
	public static String beforeDay(int day){
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.DATE, - day);
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		return df.format(cal.getTime());
	}
	/**
	 * @Description: 获取当前时间
	 * @return   
	 * @return String  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月9日
	 */
	public static String currentTime(){
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return  df.format(new Date());
	}
	/**
	 * @Description: 毫秒转date
	 * @param millis
	 * @param format
	 * @return   
	 * @return String  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月28日
	 */
	public static String formatMillis(Long millis, String format) {
		DateFormat sf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(millis);
        return sf.format(calendar.getTime());
	}
}
