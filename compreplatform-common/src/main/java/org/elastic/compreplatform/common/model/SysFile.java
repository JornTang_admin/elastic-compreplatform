package org.elastic.compreplatform.common.model;

import javax.validation.constraints.*;

import org.hibernate.validator.constraints.*;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.xiaoleilu.hutool.date.DateUtil;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;


@TableName("sys_file")
public class SysFile extends BaseModel{
	private static final long serialVersionUID = 5454155825314635342L;
	
	//alias
	public static final String TABLE_ALIAS = "SysFile";
	public static final String ALIAS_ID = "主键ID";
	public static final String ALIAS_BUSI_ID = "业务ID";
	public static final String ALIAS_FILE_FNAM = "附件全局唯一名称（UUID+后缀）";
	public static final String ALIAS_FILE_FPRIMALNAM = "附件原名称";
	public static final String ALIAS_FILE_TYPE = "附件类型";
	public static final String ALIAS_FILE_FORMAT = "附件后缀";
	public static final String ALIAS_FILE_SIZE = "附件大小";
	public static final String ALIAS_FILE_PATH = "存储路径";
	public static final String ALIAS_STATUS = "状态";
	public static final String ALIAS_CREATE_USER = "上传者";
	public static final String ALIAS_CREATE_TIME = "上传时间";
	public static final String ALIAS_UPDATE_TIME = "修改时间";
	
	//date formats
	public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	public static final String FORMAT_CREATE_TIME = DATE_FORMAT;
	public static final String FORMAT_UPDATE_TIME = DATE_FORMAT;
	
	//Can be used directly: @Length (max=50, message= "username length can not be greater than 50") display error messages
	//columns START
    /**
     * 主键ID       db_column: id 
     */	
	@Length(max=32)
	@TableId
	private java.lang.String id;
    /**
     * 业务ID       db_column: busi_id 
     */	
	@NotBlank @Length(max=32)
	private java.lang.String busiId;
    /**
     * 附件全局唯一名称（UUID+后缀）       db_column: file_fnam 
     */	
	@NotBlank @Length(max=200)
	private java.lang.String fileFnam;
    /**
     * 附件原名称       db_column: file_fprimalnam 
     */	
	@NotBlank @Length(max=1024)
	private java.lang.String fileFprimalnam;
    /**
     * 附件类型       db_column: file_type 
     */	
	@Length(max=64)
	private java.lang.String fileType;
    /**
     * 附件后缀       db_column: file_format 
     */	
	@NotBlank @Length(max=80)
	private java.lang.String fileFormat;
    /**
     * 附件大小       db_column: file_size 
     */	
	@NotNull 
	private java.lang.Long fileSize;
    /**
     * 存储路径       db_column: file_path 
     */	
	@Length(max=255)
	private java.lang.String filePath;
    /**
     * 状态       db_column: status 
     */	
	@NotNull 
	private java.lang.Integer status;
    /**
     * 上传者       db_column: create_user 
     */	
	@NotBlank @Length(max=20)
	private java.lang.String createUser;
    /**
     * 上传时间       db_column: create_time 
     */	
	@NotNull 
	private java.util.Date createTime;
    /**
     * 修改时间       db_column: update_time 
     */	
	
	private java.util.Date updateTime;
	//columns END

	public SysFile(){
	}

	public SysFile(
		java.lang.String id
	){
		this.id = id;
	}

	public void setId(java.lang.String value) {
		this.id = value;
	}
	
	public java.lang.String getId() {
		return this.id;
	}
	public void setBusiId(java.lang.String value) {
		this.busiId = value;
	}
	
	public java.lang.String getBusiId() {
		return this.busiId;
	}
	public void setFileFnam(java.lang.String value) {
		this.fileFnam = value;
	}
	
	public java.lang.String getFileFnam() {
		return this.fileFnam;
	}
	public void setFileFprimalnam(java.lang.String value) {
		this.fileFprimalnam = value;
	}
	
	public java.lang.String getFileFprimalnam() {
		return this.fileFprimalnam;
	}
	public void setFileType(java.lang.String value) {
		this.fileType = value;
	}
	
	public java.lang.String getFileType() {
		return this.fileType;
	}
	public void setFileFormat(java.lang.String value) {
		this.fileFormat = value;
	}
	
	public java.lang.String getFileFormat() {
		return this.fileFormat;
	}
	public void setFileSize(java.lang.Long value) {
		this.fileSize = value;
	}
	
	public java.lang.Long getFileSize() {
		return this.fileSize;
	}
	public void setFilePath(java.lang.String value) {
		this.filePath = value;
	}
	
	public java.lang.String getFilePath() {
		return this.filePath;
	}
	public void setStatus(java.lang.Integer value) {
		this.status = value;
	}
	
	public java.lang.Integer getStatus() {
		return this.status;
	}
	public void setCreateUser(java.lang.String value) {
		this.createUser = value;
	}
	
	public java.lang.String getCreateUser() {
		return this.createUser;
	}
	public String getCreateTimeString() {
		return DateUtil.format(getCreateTime(), FORMAT_CREATE_TIME);
	}
	public void setCreateTimeString(String value) {
		setCreateTime(DateUtil.parse(value, FORMAT_CREATE_TIME));
	}
	public void setCreateTime(java.util.Date value) {
		this.createTime = value;
	}
	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	public String getUpdateTimeString() {
		return DateUtil.format(getUpdateTime(), FORMAT_UPDATE_TIME);
	}
	public void setUpdateTimeString(String value) {
		setUpdateTime(DateUtil.parse(value, FORMAT_UPDATE_TIME));
	}
	public void setUpdateTime(java.util.Date value) {
		this.updateTime = value;
	}
	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}

	public String toString() {
		return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
			.append("Id",getId())
			.append("BusiId",getBusiId())
			.append("FileFnam",getFileFnam())
			.append("FileFprimalnam",getFileFprimalnam())
			.append("FileType",getFileType())
			.append("FileFormat",getFileFormat())
			.append("FileSize",getFileSize())
			.append("FilePath",getFilePath())
			.append("Status",getStatus())
			.append("CreateUser",getCreateUser())
			.append("CreateTime",getCreateTime())
			.append("UpdateTime",getUpdateTime())
			.toString();
	}
	
	public int hashCode() {
		return new HashCodeBuilder()
			.append(getId())
			.toHashCode();
	}
	
	public boolean equals(Object obj) {
		if(obj instanceof SysFile == false) return false;
		if(this == obj) return true;
		SysFile other = (SysFile)obj;
		return new EqualsBuilder()
			.append(getId(),other.getId())
			.isEquals();
	}
}

