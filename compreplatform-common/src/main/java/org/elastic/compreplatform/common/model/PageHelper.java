package org.elastic.compreplatform.common.model;

import org.elastic.compreplatform.common.constant.RespEnum;


public class PageHelper {
	//排序列 默认id
	private String sortColumns = "id";
	//返回Code
    private String code = RespEnum.SUCCESS.getCode();
    //返回描述
    private String msg;
    //返回数据
    private Object data;
    //总数
    private long count =0;
    //分页长度
    private int limit=15;
    //分页页号
    private int page=1;
    //分页开始位
    private int offset;
    
    public int getOffset() {
        if (page > 0) {
        	offset = (limit * (page - 1));
        } else {
        	offset = 0;
        }
        return offset;
    }
	public String getSortColumns() {
		return sortColumns;
	}

	public void setSortColumns(String sortColumns) {
		this.sortColumns = sortColumns;
	}

	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	public long getCount() {
		return count;
	}
	public void setCount(long count) {
		this.count = count;
	}
}
