/*
 * Copyright © 2018 JornTang personal. 
 *
 * All right reserved. 
 *
 * Author JornTang 
 *
 * Date 2018-03-04
 */
package org.elastic.compreplatform.common.service.impl;

import javax.annotation.Resource;

import org.elastic.compreplatform.common.dao.ISysFileDao;
import org.elastic.compreplatform.common.model.PageHelper;
import org.elastic.compreplatform.common.model.SysFile;
import org.elastic.compreplatform.common.vo.query.SysFileQuery;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;

@Service
public class SysFileServiceImpl extends BaseServiceImpl<SysFile>{
	
	@Resource
	private ISysFileDao sysFileDao;
	
	/**
	 * 分页查询
	 */
	public Page<SysFile> pagelist(SysFileQuery query, PageHelper page){
		com.github.pagehelper.PageHelper.startPage(page.getPage(), page.getLimit());
		return sysFileDao.pagelist(query);
	}
}
