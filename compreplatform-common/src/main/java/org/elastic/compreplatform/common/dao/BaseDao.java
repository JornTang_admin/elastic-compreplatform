package org.elastic.compreplatform.common.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * ClassName: BaseDao 
 * @Description: 基础dao定义
 * @author JornTang
 * @date 2018年2月25日
 */
public interface BaseDao<T> extends BaseMapper<T> {

}
