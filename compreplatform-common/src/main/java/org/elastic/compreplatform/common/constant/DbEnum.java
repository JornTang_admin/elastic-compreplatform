package org.elastic.compreplatform.common.constant;

/**
 * ClassName: DriverEnum 
 * @Description: 数据库驱动枚举定义
 * @author JornTang
 * @date 2018年2月3日
 */
public enum DbEnum {
	// oracle
	ORACLE("Oracle","oracle.jdbc.driver.OracleDriver", "select t.TABLE_NAME as TABLENAME,tc.table_type as TABLETYPE,tc.comments as TABLECOMMENT,ob.CREATED as CREATETIME from user_tables t left join user_tab_comments tc on tc.table_name = t.TABLE_NAME left join dba_objects ob on ob.OBJECT_NAME = t.TABLE_NAME where ob.OWNER = '{}' "),
	
	// mysql
	MYSQL("Mysql" ,"com.mysql.jdbc.Driver", "SELECT table_name as TABLENAME,table_comment as TABLECOMMENT,TABLE_TYPE as TABLETYPE,CREATE_TIME as CREATETIME FROM information_schema.TABLES t WHERE table_schema = '{}' "),
	
	// H2
	H2("H2" ,"org.h2.Driver", ""),
	
	// SQLServer2000
	SQLSERVER2000("SQLServer2000" ,"com.microsoft.jdbc.sqlserver.SQLServerDriver", ""),

	// SQLServer2005
	SQLSERVER2005("SQLServer2005" ,"com.microsoft.sqlserver.jdbc.SQLServerDriver", ""),

	// JTDs for SQLServer
	JTDS("JTDs for SQLServer" ,"net.sourceforge.jtds.jdbc.Driver", ""),

	// PostgreSql
	POSTGRESQL("PostgreSql" ,"org.postgresql.Driver", ""),

	// Sybase
	SYBASE("Sybase" ,"com.sybase.jdbc.SybDriver", ""),
	
	// DB2
	DB2("DB2" ,"com.ibm.db2.jcc.DB2Driver", ""),

	// HsqlDB
	HSQLDB("HsqlDB" ,"org.hsqldb.jdbcDriver", ""),
	
	// Derby
	DERBY("Derby" ,"org.apache.derby.jdbc.ClientDriver", "");
	
	DbEnum(String type, String driver, String sql) {
		this.type = type;
		this.driver = driver;
		this.sql = sql;
	}
	private String type;

	private String driver;
	
	private String sql;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDriver() {
		return driver;
	}

	public void setDriver(String driver) {
		this.driver = driver;
	}

	public String getSql() {
		return sql;
	}

	public void setSql(String sql) {
		this.sql = sql;
	}
	
}
