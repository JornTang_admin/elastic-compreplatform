package org.elastic.compreplatform.common.constant;

/**
 * ClassName: ProxyEnum 
 * @Description: IP代理网址定义
 * @author JornTang
 * @date 2018年2月11日
 */
public enum ProxyEnum {
	// http://www.ip181.com/
	IP181("ip181","http://www.ip181.com/", "http://www.ip181.com/"),
	
	// http://www.xicidaili.com/nn/
	XICIDAILI("xicidaili" ,"http://www.xicidaili.com/nn/", "http://www.xicidaili.com/nn/");
	
	ProxyEnum(String type, String url, String desc) {
		this.type = type;
		this.url = url;
		this.desc = desc;
	}
	private String type;

	private String url;
	
	private String desc;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
}
