package org.elastic.compreplatform.common.service.impl;

import org.elastic.compreplatform.common.dao.BaseDao;
import org.elastic.compreplatform.common.service.IBaseService;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;

public class BaseServiceImpl<T> extends ServiceImpl<BaseDao<T>, T> implements IBaseService<T>{


}
