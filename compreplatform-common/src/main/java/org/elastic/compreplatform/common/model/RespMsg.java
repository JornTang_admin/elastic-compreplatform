package org.elastic.compreplatform.common.model;

import java.io.Serializable;

import org.elastic.compreplatform.common.constant.RespEnum;

/**
 * ClassName: RespMsg 
 * @Description: 消息返回
 * @author JornTang
 * @date 2018年1月28日
 */
public class RespMsg implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = -3648894337576543601L;
	//返回Code
    private String code = RespEnum.SUCCESS.getCode();
    //返回描述
    private String msg;
    //返回数据
    private Object data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
