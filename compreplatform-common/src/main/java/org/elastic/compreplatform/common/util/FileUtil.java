package org.elastic.compreplatform.common.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * ClassName: FileUtil 
 * @Description: 文件操作工具类
 * @author JornTang
 * @date 2018年3月8日
 */
public class FileUtil {
	private static Logger log = LoggerFactory.getLogger(FileUtil.class);
	/**
	 * @Description: 输入流转file并存储
	 * @param in
	 * @param file   
	 * @return void  
	 * @throws
	 * @author JornTang
	 * @date 2018年3月8日
	 */
	public static void inputstreamToFile(InputStream in,File file){
		OutputStream os = null;
		try {
			os = new FileOutputStream(file);
			int bytesRead = 0;
			byte[] buffer = new byte[8192];
			while ((bytesRead = in.read(buffer, 0, 8192)) != -1) {
				os.write(buffer, 0, bytesRead);
			}
		} catch (FileNotFoundException e) {
			log.error("FileNotFoundException", e);
		} catch (IOException e) {
			log.error("IOException", e);
		} finally {
			try {
				if(os!= null) {
					os.close();
				}
				if(in!= null) {
					in.close();
				}
			} catch (IOException e) {
				log.error("关闭流异常", e);
			}
		}
	}
}
