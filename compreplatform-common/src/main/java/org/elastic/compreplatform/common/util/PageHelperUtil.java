package org.elastic.compreplatform.common.util;

import org.elastic.compreplatform.common.constant.RespEnum;
import org.elastic.compreplatform.common.model.PageHelper;

import com.github.pagehelper.Page;

/**
 * ClassName: PageHelperUtil 
 * @Description: 分页工具类
 * @author JornTang
 * @date 2018年2月2日
 */
public class PageHelperUtil {
	/**
	 * @Description: 分页对象处理封装
	 * @return   
	 * @return PageHelper  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月2日
	 */
	@SuppressWarnings("rawtypes")
	public static PageHelper doHandlePage(Page page){
		PageHelper helper = new PageHelper();
		if(page == null){
			return helper;
		}
		helper.setCode(RespEnum.SUCCESS.getCode());
		helper.setCount(page.getTotal());
		helper.setPage(page.getPageNum());
		helper.setLimit(page.getPageSize());
		helper.setData(page.getResult());
		return helper;
	}
}
