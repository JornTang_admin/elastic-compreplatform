/*
 * Copyright © 2018 JornTang personal. 
 *
 * All right reserved. 
 *
 * Author JornTang 
 *
 * Date 2018-03-04
 */
package org.elastic.compreplatform.common.vo.query;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.elastic.compreplatform.common.model.BaseModel;

public class SysFileQuery extends BaseModel {
    private static final long serialVersionUID = 3148176768559230877L;
    

	/** 主键ID */
	private java.lang.String id;
	/** 业务ID */
	private java.lang.String busiId;
	/** 附件全局唯一名称（UUID+后缀） */
	private java.lang.String fileFnam;
	/** 附件原名称 */
	private java.lang.String fileFprimalnam;
	/** 附件类型 */
	private java.lang.String fileType;
	/** 附件后缀 */
	private java.lang.String fileFormat;
	/** 附件大小 */
	private java.lang.Long fileSize;
	/** 存储路径 */
	private java.lang.String filePath;
	/** 状态 */
	private java.lang.Integer status;
	/** 上传者 */
	private java.lang.String createUser;
	/** 上传时间 */
	private java.util.Date createTimeBegin;
	private java.util.Date createTimeEnd;
	/** 修改时间 */
	private java.util.Date updateTimeBegin;
	private java.util.Date updateTimeEnd;

	public java.lang.String getId() {
		return this.id;
	}
	
	public void setId(java.lang.String value) {
		this.id = value;
	}
	
	public java.lang.String getBusiId() {
		return this.busiId;
	}
	
	public void setBusiId(java.lang.String value) {
		this.busiId = value;
	}
	
	public java.lang.String getFileFnam() {
		return this.fileFnam;
	}
	
	public void setFileFnam(java.lang.String value) {
		this.fileFnam = value;
	}
	
	public java.lang.String getFileFprimalnam() {
		return this.fileFprimalnam;
	}
	
	public void setFileFprimalnam(java.lang.String value) {
		this.fileFprimalnam = value;
	}
	
	public java.lang.String getFileType() {
		return this.fileType;
	}
	
	public void setFileType(java.lang.String value) {
		this.fileType = value;
	}
	
	public java.lang.String getFileFormat() {
		return this.fileFormat;
	}
	
	public void setFileFormat(java.lang.String value) {
		this.fileFormat = value;
	}
	
	public java.lang.Long getFileSize() {
		return this.fileSize;
	}
	
	public void setFileSize(java.lang.Long value) {
		this.fileSize = value;
	}
	
	public java.lang.String getFilePath() {
		return this.filePath;
	}
	
	public void setFilePath(java.lang.String value) {
		this.filePath = value;
	}
	
	public java.lang.Integer getStatus() {
		return this.status;
	}
	
	public void setStatus(java.lang.Integer value) {
		this.status = value;
	}
	
	public java.lang.String getCreateUser() {
		return this.createUser;
	}
	
	public void setCreateUser(java.lang.String value) {
		this.createUser = value;
	}
	
	public java.util.Date getCreateTimeBegin() {
		return this.createTimeBegin;
	}
	
	public void setCreateTimeBegin(java.util.Date value) {
		this.createTimeBegin = value;
	}	
	
	public java.util.Date getCreateTimeEnd() {
		return this.createTimeEnd;
	}
	
	public void setCreateTimeEnd(java.util.Date value) {
		this.createTimeEnd = value;
	}
	
	public java.util.Date getUpdateTimeBegin() {
		return this.updateTimeBegin;
	}
	
	public void setUpdateTimeBegin(java.util.Date value) {
		this.updateTimeBegin = value;
	}	
	
	public java.util.Date getUpdateTimeEnd() {
		return this.updateTimeEnd;
	}
	
	public void setUpdateTimeEnd(java.util.Date value) {
		this.updateTimeEnd = value;
	}
	

	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.MULTI_LINE_STYLE);
	}
	
}

