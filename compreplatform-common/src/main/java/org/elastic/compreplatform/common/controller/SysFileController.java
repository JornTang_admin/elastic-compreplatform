/*
 * Copyright © 2018 JornTang personal. 
 *
 * All right reserved. 
 *
 * Author JornTang 
 *
 * Date 2018-03-04
 */
package org.elastic.compreplatform.common.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.elastic.compreplatform.common.constant.RespEnum;
import org.elastic.compreplatform.common.model.PageHelper;
import org.elastic.compreplatform.common.model.RespMsg;
import org.elastic.compreplatform.common.model.SysFile;
import org.elastic.compreplatform.common.service.impl.SysFileServiceImpl;
import org.elastic.compreplatform.common.util.PageHelperUtil;
import org.elastic.compreplatform.common.util.RespMsgUtil;
import org.elastic.compreplatform.common.vo.query.SysFileQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;

@Controller
@RequestMapping("/sysFile")
public class SysFileController{
	private static Logger log = LoggerFactory.getLogger(SysFileController.class); 
	/**
	 * 注解注入
	 */
	@Resource
	private SysFileServiceImpl sysFileService;
	
	public SysFileController() {
	}
	/** 
	 * 进入列表页面
	 **/
	@RequestMapping("/toList")
	public String toList(Model model) {
		return "sysfile/list";
	}
	/** 
	 * 分页查询
	 **/
	@RequestMapping("/pageList")
	@ResponseBody
	public PageHelper pagelist(HttpServletRequest request, HttpServletResponse response, SysFileQuery query, PageHelper page) {
		Page<SysFile> list = sysFileService.pagelist(query, page);
        return PageHelperUtil.doHandlePage(list);
	}
	/** 
	 * 进入添加页面
	 **/
	@RequestMapping("/toAdd")
	public String toAdd(Model model) {
		return "sysfile/add";
	}
	/** 
	 * 添加
	 **/
	@RequestMapping("/add")
	@ResponseBody
	public RespMsg add(HttpServletRequest request, HttpServletResponse response, Model model, SysFile entity) {
		boolean isOK = sysFileService.insert(entity);
		return isOK?RespMsgUtil.returnMsg(RespEnum.SUCCESS): RespMsgUtil.returnMsg(RespEnum.ERROR);
	}
	/** 
	 * 进入修改页面
	 **/
	@RequestMapping("/toUpdate")
	public String toUpdate(Model model, java.lang.String id) {
		SysFile entity = sysFileService.selectById(id);
		model.addAttribute("entity", entity);
		return "sysfile/update";
	}
	/** 
	 * 修改
	 **/
	@RequestMapping("/update")
	@ResponseBody
	public RespMsg update(HttpServletRequest request, HttpServletResponse response, SysFile entity) {
		boolean isOk = sysFileService.updateById(entity);
		return isOk?RespMsgUtil.returnMsg(RespEnum.SUCCESS): RespMsgUtil.returnMsg(RespEnum.ERROR);
	}
	/** 
	 * 删除
	 **/
	@RequestMapping("/delete")
	@ResponseBody
	public RespMsg delete(HttpServletRequest request, HttpServletResponse response, java.lang.String id) throws Exception {
		boolean isOk = sysFileService.deleteById(id);
		return isOk?RespMsgUtil.returnMsg(RespEnum.SUCCESS): RespMsgUtil.returnMsg(RespEnum.ERROR);
	}
	/** 
	 * 查看详情
	 **/
	@RequestMapping("/show")
	public String delete(HttpServletRequest request, HttpServletResponse response, Model model, java.lang.String id) throws Exception {
		SysFile entity = sysFileService.selectById(id);
		model.addAttribute("entity", entity);
		return "sysfile/show";
	}
}

