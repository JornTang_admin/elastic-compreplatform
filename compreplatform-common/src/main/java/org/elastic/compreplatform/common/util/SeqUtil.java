package org.elastic.compreplatform.common.util;

import java.util.UUID;

import com.xiaoleilu.hutool.lang.Snowflake;

/**
 * ClassName: SeqUtil 
 * @Description: 全局唯一ID工具类
 * @author JornTang
 * @date 2018年2月2日
 */
public class SeqUtil {
	/** 工作机器ID(0~31) */
	private static final long workerId = 1L;
	/** 数据中心ID(0~31) */
	private static final long datacenterId = 1L;
	public static long getSeq(){
		return new Snowflake(workerId, datacenterId, Boolean.FALSE).nextId();
	}
	public static String getUUID(){
		return UUID.randomUUID().toString().replace("-", "");
	}
}
