package org.elastic.compreplatform.common.model;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;

/**
 * ClassName: BaseModel 
 * @Description: 通用字段定义
 * @author JornTang
 * @date 2018年1月28日
 */
public class BaseModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 876466919260870424L;
    /**
     * 创建人
     */
	@TableField(exist = false)
    private String createUser;
    /**
     * 创建时间
     */
	@TableField(exist = false)
    private Date createTime;
    /**
     * 修改人
     */
	@TableField(exist = false)
    private String updateUser;
    /**
     * 修改时间
     */
	@TableField(exist = false)
    private Date updateTime;
    /**
     * 逻辑删除标志
     */
	@TableField(exist = false)
    private Integer delFlag;
	/**
	 * 排序字段
	 */
	@TableField(exist = false)
	private String sortColumns = "id";
    
	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Integer getDelFlag() {
		return delFlag;
	}

	public void setDelFlag(Integer delFlag) {
		this.delFlag = delFlag;
	}

	public String getSortColumns() {
		return sortColumns;
	}

	public void setSortColumns(String sortColumns) {
		this.sortColumns = sortColumns;
	}
}
