package org.elastic.compreplatform.conf.admin.dao;

import org.elastic.compreplatform.common.dao.BaseDao;
import org.elastic.compreplatform.conf.admin.model.ZookProject;
import org.elastic.compreplatform.conf.admin.vo.query.ZookProjectQuery;

import com.github.pagehelper.Page;

/**
 * ClassName: 
 * @Description: 
 * @author 
 * @date 
 */
public interface IZookProjectDao extends BaseDao<ZookProject>{
	
	/**
	 * 分页查询
	 */
	public Page<ZookProject> pagelist(ZookProjectQuery query);
}
