/*
 * Copyright © 2018 JornTang personal. 
 *
 * All right reserved. 
 *
 * Author JornTang 
 *
 * Date 2018-03-04
 */
package org.elastic.compreplatform.conf.admin.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.elastic.compreplatform.common.constant.RespEnum;
import org.elastic.compreplatform.common.model.PageHelper;
import org.elastic.compreplatform.common.model.RespMsg;
import org.elastic.compreplatform.common.util.PageHelperUtil;
import org.elastic.compreplatform.common.util.RespMsgUtil;
import org.elastic.compreplatform.common.util.SeqUtil;
import org.elastic.compreplatform.conf.admin.model.ZookNode;
import org.elastic.compreplatform.conf.admin.model.ZookProject;
import org.elastic.compreplatform.conf.admin.service.ZookManager;
import org.elastic.compreplatform.conf.admin.service.ZookNodeServiceImpl;
import org.elastic.compreplatform.conf.admin.service.ZookProjectServiceImpl;
import org.elastic.compreplatform.conf.admin.vo.query.ZookNodeQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.github.pagehelper.Page;

@Controller
@RequestMapping("/zookNode")
public class ZookNodeController{
	private static Logger log = LoggerFactory.getLogger(ZookNodeController.class); 
	/**
	 * 注解注入
	 */
	@Resource
	private ZookNodeServiceImpl zookNodeService;
	
	@Resource
	private ZookProjectServiceImpl zookProjectService;
	
	@Resource
	private ZookManager zookManager;
	
	public ZookNodeController() {
	}
	/** 
	 * 进入列表页面
	 **/
	@RequestMapping("/toList")
	public String toList(Model model) {
		//获取所有项目配置
		EntityWrapper<ZookProject> wrapper = new EntityWrapper<ZookProject>();
		List<ZookProject> list = zookProjectService.selectList(wrapper);
		model.addAttribute("list", list);
		return "zooknode/list";
	}
	/** 
	 * 分页查询
	 **/
	@RequestMapping("/pageList")
	@ResponseBody
	public PageHelper pagelist(HttpServletRequest request, HttpServletResponse response, ZookNodeQuery query, PageHelper page) {
		Page<ZookNode> list = zookNodeService.pagelist(query, page);
        return PageHelperUtil.doHandlePage(list);
	}
	/** 
	 * 进入添加页面
	 **/
	@RequestMapping("/toAdd")
	public String toAdd(Model model, ZookNode entity) {
		model.addAttribute("entity", entity);
		return "zooknode/add";
	}
	/** 
	 * 添加
	 **/
	@RequestMapping("/add")
	@ResponseBody
	public RespMsg add(HttpServletRequest request, HttpServletResponse response, Model model, ZookNode entity) {
		RespMsg msg = RespMsgUtil.returnMsg(RespEnum.SUCCESS);
		try {
			entity.setId(SeqUtil.getSeq());
			entity.setNodeKey(entity.getAppname() + "." + entity.getNodeKey());
			//判断key是否存在
			EntityWrapper<ZookNode> nodeWrapper = new EntityWrapper<ZookNode>();
			nodeWrapper.where("node_key = {0}", entity.getNodeKey());
			List<ZookNode> ndoes = zookNodeService.selectList(nodeWrapper);
			if(ndoes!= null && ndoes.size()> 0) {
				msg = RespMsgUtil.returnMsg(RespEnum.ERROR);
				msg.setMsg("添加失败，key已存在");
			}else {
				boolean isOk = zookNodeService.insert(entity);
				if(isOk) {
					zookManager.set(entity.getNodeKey(), entity.getNodeValue());
				}
			}
		} catch (Exception e) {
			msg = RespMsgUtil.returnMsg(RespEnum.ERROR);
			log.error("添加节点异常", e);
		}
		return msg;
	}
	/** 
	 * 进入修改页面
	 **/
	@RequestMapping("/toUpdate")
	public String toUpdate(Model model, java.lang.Long id) {
		ZookNode entity = zookNodeService.selectById(id);
		model.addAttribute("entity", entity);
		return "zooknode/update";
	}
	/** 
	 * 修改
	 **/
	@RequestMapping("/update")
	@ResponseBody
	public RespMsg update(HttpServletRequest request, HttpServletResponse response, ZookNode entity) {
		RespMsg msg = RespMsgUtil.returnMsg(RespEnum.SUCCESS);
		try {
			entity.setNodeKey(entity.getAppname() + "." + entity.getNodeKey());
			boolean isOk = zookNodeService.updateById(entity);
			if(isOk) {
				zookManager.set(entity.getNodeKey(), entity.getNodeValue());
			}
		} catch (Exception e) {
			msg = RespMsgUtil.returnMsg(RespEnum.ERROR);
			log.error("修改节点异常", e);
		}
		return msg;
	}
	/** 
	 * 删除
	 **/
	@RequestMapping("/delete")
	@ResponseBody
	public RespMsg delete(HttpServletRequest request, HttpServletResponse response, java.lang.Long id) throws Exception {
		RespMsg msg = RespMsgUtil.returnMsg(RespEnum.SUCCESS);
		try {
			ZookNode entity = zookNodeService.selectById(id);
			boolean isOk = zookNodeService.deleteById(id);
			if(isOk) {
				zookManager.delete(entity.getNodeKey());
			}
		} catch (Exception e) {
			msg = RespMsgUtil.returnMsg(RespEnum.ERROR);
			log.error("删除节点值异常", e);
		}
		return msg;
	}
	/** 
	 * 查看详情
	 **/
	@RequestMapping("/show")
	public String delete(HttpServletRequest request, HttpServletResponse response, Model model, java.lang.Long id) throws Exception {
		ZookNode entity = zookNodeService.selectById(id);
		model.addAttribute("entity", entity);
		return "zooknode/show";
	}
}

