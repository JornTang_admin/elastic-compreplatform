package org.elastic.compreplatform.conf.admin.model;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.elastic.compreplatform.common.model.BaseModel;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


@TableName("zook_node")
public class ZookNode extends BaseModel{
	private static final long serialVersionUID = 5454155825314635342L;
	
	//alias
	public static final String TABLE_ALIAS = "ZookNode";
	public static final String ALIAS_KEY = "配置Key";
	public static final String ALIAS_APPNAME = "所属项目AppName";
	public static final String ALIAS_TITLE = "配置描述";
	public static final String ALIAS_VALUE = "配置Value";
	
	//date formats
	public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	
	//Can be used directly: @Length (max=50, message= "username length can not be greater than 50") display error messages
	//columns START
	/**
     * 主键ID       db_column: ID 
     */	
	@Length(max=20)
	@TableId
	private java.lang.Long id;
	/**
     * 配置nodeKey       db_column: nodeKey 
     */	
	@Length(max=200)
	private java.lang.String nodeKey;
    /**
     * 所属项目AppName       db_column: appname 
     */	
	@NotBlank @Length(max=100)
	private java.lang.String appname;
    /**
     * 配置描述       db_column: title 
     */	
	@NotBlank @Length(max=100)
	private java.lang.String title;
    /**
     * 配置nodeValue       db_column: nodeValue 
     */	
	@Length(max=2000)
	private java.lang.String nodeValue;
	//columns END

	public ZookNode(){
	}

	public ZookNode(
			java.lang.Long id
	){
		this.id = id;
	}
	
	public java.lang.Long getId() {
		return id;
	}

	public void setId(java.lang.Long id) {
		this.id = id;
	}

	public void setAppname(java.lang.String value) {
		this.appname = value;
	}
	
	public java.lang.String getAppname() {
		return this.appname;
	}
	public void setTitle(java.lang.String value) {
		this.title = value;
	}
	
	public java.lang.String getTitle() {
		return this.title;
	}
	
	public java.lang.String getNodeKey() {
		return nodeKey;
	}

	public void setNodeKey(java.lang.String nodeKey) {
		this.nodeKey = nodeKey;
	}

	public java.lang.String getNodeValue() {
		return nodeValue;
	}

	public void setNodeValue(java.lang.String nodeValue) {
		this.nodeValue = nodeValue;
	}

	public String toString() {
		return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
			.append("id",getId())
			.append("nodeKey",getNodeKey())
			.append("Appname",getAppname())
			.append("Title",getTitle())
			.append("nodeValue",getNodeValue())
			.toString();
	}
	
	public int hashCode() {
		return new HashCodeBuilder()
			.append(getId())
			.toHashCode();
	}
	
	public boolean equals(Object obj) {
		if(obj instanceof ZookNode == false) return false;
		if(this == obj) return true;
		ZookNode other = (ZookNode)obj;
		return new EqualsBuilder()
			.append(getId(),other.getId())
			.isEquals();
	}
}

