/*
 * Copyright © 2018 JornTang personal. 
 *
 * All right reserved. 
 *
 * Author JornTang 
 *
 * Date 2018-03-04
 */
package org.elastic.compreplatform.conf.admin.service;

import javax.annotation.Resource;

import org.elastic.compreplatform.common.model.PageHelper;
import org.elastic.compreplatform.common.service.impl.BaseServiceImpl;
import org.elastic.compreplatform.conf.admin.dao.IZookNodeDao;
import org.elastic.compreplatform.conf.admin.model.ZookNode;
import org.elastic.compreplatform.conf.admin.vo.query.ZookNodeQuery;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;

@Service
public class ZookNodeServiceImpl extends BaseServiceImpl<ZookNode>{
	
	@Resource
	private IZookNodeDao zookNodeDao;
	
	/**
	 * 分页查询
	 */
	public Page<ZookNode> pagelist(ZookNodeQuery query, PageHelper page){
		com.github.pagehelper.PageHelper.startPage(page.getPage(), page.getLimit());
		return zookNodeDao.pagelist(query);
	}
}
