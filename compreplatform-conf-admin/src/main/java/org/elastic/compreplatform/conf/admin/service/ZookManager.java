package org.elastic.compreplatform.conf.admin.service;

import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.elastic.compreplatform.conf.core.util.ZkClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


/**
 * ClassName: ZookManager 
 * @Description: ZooKeeper cfg client (Watcher + some utils)
 * @author JornTang
 * @date 2018年3月5日
 */
@Component
public class ZookManager implements InitializingBean, DisposableBean {
	private static Logger logger = LoggerFactory.getLogger(ZookManager.class);


	@Value("${zook.address}")
	private String zkaddress;

	@Value("${zook.path}")
	private String zkpath;


	// ------------------------------ zookeeper client ------------------------------
	private static ZkClient zkClient = null;
	@Override
	public void afterPropertiesSet() throws Exception {
		Watcher watcher = new Watcher() {
			@Override
			public void process(WatchedEvent watchedEvent) {
				logger.info(">>>>>>>>>> xxl-conf: XxlConfManager watcher:{}", watchedEvent);

				// session expire, close old and create new
				if (watchedEvent.getState() == Event.KeeperState.Expired) {
					zkClient.destroy();
					zkClient.getClient();
					logger.info(">>>>>>>>>> xxl-conf, XxlConfManager re-connect success.");
				}
			}
		};

		zkClient = new ZkClient(zkaddress, watcher);
		logger.info(">>>>>>>>>> xxl-conf, XxlConfManager init success.");
	}
	@Override
	public void destroy() throws Exception {
		if (zkClient!=null) {
			zkClient.destroy();
		}
	}


	// ------------------------------ conf opt ------------------------------

	/**
	 * set zk conf
	 *
	 * @param key
	 * @param data
	 * @return
	 */
	public void set(String key, String data) {
		String path = keyToPath(key);
		zkClient.setPathData(path, data);
	}

	/**
	 * delete zk conf
	 *
	 * @param key
	 */
	public void delete(String key){
		String path = keyToPath(key);
		zkClient.deletePath(path);
	}

	/**
	 * get zk conf
	 *
	 * @param key
	 * @return
	 */
	public String get(String key){
		String path = keyToPath(key);
		return zkClient.getPathData(path);
	}


	// ------------------------------ key 2 path / genarate key ------------------------------

	/**
	 * path 2 key
	 * @param nodePath
	 * @return ZnodeKey
	 */
	public String pathToKey(String nodePath){
		if (nodePath==null || nodePath.length() <= zkpath.length() || !nodePath.startsWith(zkpath)) {
			return null;
		}
		return nodePath.substring(zkpath.length()+1, nodePath.length());
	}

	/**
	 * key 2 path
	 * @param nodeKey
	 * @return znodePath
	 */
	public String keyToPath(String nodeKey){
		return zkpath + "/" + nodeKey;
	}


}