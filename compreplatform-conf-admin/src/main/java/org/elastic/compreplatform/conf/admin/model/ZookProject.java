package org.elastic.compreplatform.conf.admin.model;

import javax.validation.constraints.*;

import org.hibernate.validator.constraints.*;
import org.elastic.compreplatform.common.model.BaseModel;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;


@TableName("zook_project")
public class ZookProject extends BaseModel{
	private static final long serialVersionUID = 5454155825314635342L;
	
	//alias
	public static final String TABLE_ALIAS = "ZookProject";
	public static final String ALIAS_ID = "主键ID";
	public static final String ALIAS_APPNAME = "AppName";
	public static final String ALIAS_TITLE = "项目名称";
	
	//date formats
	public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	
	//Can be used directly: @Length (max=50, message= "username length can not be greater than 50") display error messages
	//columns START
    /**
     * 主键ID       db_column: id 
     */	
	
	@TableId
	private java.lang.Long id;
    /**
     * AppName       db_column: appname 
     */	
	@NotBlank @Length(max=100)
	private java.lang.String appname;
    /**
     * 项目名称       db_column: title 
     */	
	@NotBlank @Length(max=100)
	private java.lang.String title;
	//columns END

	public ZookProject(){
	}

	public ZookProject(
		java.lang.Long id
	){
		this.id = id;
	}

	public void setId(java.lang.Long value) {
		this.id = value;
	}
	
	public java.lang.Long getId() {
		return this.id;
	}
	public void setAppname(java.lang.String value) {
		this.appname = value;
	}
	
	public java.lang.String getAppname() {
		return this.appname;
	}
	public void setTitle(java.lang.String value) {
		this.title = value;
	}
	
	public java.lang.String getTitle() {
		return this.title;
	}

	public String toString() {
		return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
			.append("Id",getId())
			.append("Appname",getAppname())
			.append("Title",getTitle())
			.toString();
	}
	
	public int hashCode() {
		return new HashCodeBuilder()
			.append(getId())
			.toHashCode();
	}
	
	public boolean equals(Object obj) {
		if(obj instanceof ZookProject == false) return false;
		if(this == obj) return true;
		ZookProject other = (ZookProject)obj;
		return new EqualsBuilder()
			.append(getId(),other.getId())
			.isEquals();
	}
}

