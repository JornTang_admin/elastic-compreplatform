/*
 * Copyright © 2018 JornTang personal. 
 *
 * All right reserved. 
 *
 * Author JornTang 
 *
 * Date 2018-03-04
 */
package org.elastic.compreplatform.conf.admin.vo.query;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.elastic.compreplatform.common.model.BaseModel;

public class ZookNodeQuery extends BaseModel {
    private static final long serialVersionUID = 3148176768559230877L;
    
    /** 主键ID */
	private java.lang.Long id;
	/** 配置Key */
	private java.lang.String nodeKey;
	/** 所属项目AppName */
	private java.lang.String appname;
	/** 配置描述 */
	private java.lang.String title;
	/** 配置Value */
	private java.lang.String nodeValue;
	
	public java.lang.Long getId() {
		return id;
	}

	public void setId(java.lang.Long id) {
		this.id = id;
	}

	public java.lang.String getAppname() {
		return this.appname;
	}
	
	public void setAppname(java.lang.String value) {
		this.appname = value;
	}
	
	public java.lang.String getTitle() {
		return this.title;
	}
	
	public void setTitle(java.lang.String value) {
		this.title = value;
	}
	
	public java.lang.String getNodeKey() {
		return nodeKey;
	}

	public void setNodeKey(java.lang.String nodeKey) {
		this.nodeKey = nodeKey;
	}

	public java.lang.String getNodeValue() {
		return nodeValue;
	}

	public void setNodeValue(java.lang.String nodeValue) {
		this.nodeValue = nodeValue;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.MULTI_LINE_STYLE);
	}
	
}

