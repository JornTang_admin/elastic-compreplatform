/*
 * Copyright © 2018 JornTang personal. 
 *
 * All right reserved. 
 *
 * Author JornTang 
 *
 * Date 2018-03-04
 */
package org.elastic.compreplatform.conf.admin.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.elastic.compreplatform.common.constant.RespEnum;
import org.elastic.compreplatform.common.model.PageHelper;
import org.elastic.compreplatform.common.model.RespMsg;
import org.elastic.compreplatform.common.util.PageHelperUtil;
import org.elastic.compreplatform.common.util.RespMsgUtil;
import org.elastic.compreplatform.common.util.SeqUtil;
import org.elastic.compreplatform.conf.admin.model.ZookNode;
import org.elastic.compreplatform.conf.admin.model.ZookProject;
import org.elastic.compreplatform.conf.admin.service.ZookNodeServiceImpl;
import org.elastic.compreplatform.conf.admin.service.ZookProjectServiceImpl;
import org.elastic.compreplatform.conf.admin.vo.query.ZookProjectQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.github.pagehelper.Page;

@Controller
@RequestMapping("/zookProject")
public class ZookProjectController{
	private static Logger log = LoggerFactory.getLogger(ZookProjectController.class); 
	/**
	 * 注解注入
	 */
	@Resource
	private ZookProjectServiceImpl zookProjectService;
	
	@Resource
	private ZookNodeServiceImpl zookNodeService;
	
	public ZookProjectController() {
	}
	/** 
	 * 进入列表页面
	 **/
	@RequestMapping("/toList")
	public String toList(Model model) {
		return "zookproject/list";
	}
	/** 
	 * 分页查询
	 **/
	@RequestMapping("/pageList")
	@ResponseBody
	public PageHelper pagelist(HttpServletRequest request, HttpServletResponse response, ZookProjectQuery query, PageHelper page) {
		Page<ZookProject> list = zookProjectService.pagelist(query, page);
        return PageHelperUtil.doHandlePage(list);
	}
	/** 
	 * 进入添加页面
	 **/
	@RequestMapping("/toAdd")
	public String toAdd(Model model) {
		return "zookproject/add";
	}
	/** 
	 * 添加
	 **/
	@RequestMapping("/add")
	@ResponseBody
	public RespMsg add(HttpServletRequest request, HttpServletResponse response, Model model, ZookProject entity) {
		entity.setId(SeqUtil.getSeq());
		boolean isOk = zookProjectService.insert(entity);
		return isOk?RespMsgUtil.returnMsg(RespEnum.SUCCESS): RespMsgUtil.returnMsg(RespEnum.ERROR);
	}
	/** 
	 * 进入修改页面
	 **/
	@RequestMapping("/toUpdate")
	public String toUpdate(Model model, java.lang.Long id) {
		ZookProject entity = zookProjectService.selectById(id);
		model.addAttribute("entity", entity);
		return "zookproject/update";
	}
	/** 
	 * 修改
	 **/
	@RequestMapping("/update")
	@ResponseBody
	public RespMsg update(HttpServletRequest request, HttpServletResponse response, ZookProject entity) {
		boolean isOk = zookProjectService.updateById(entity);
		return isOk?RespMsgUtil.returnMsg(RespEnum.SUCCESS): RespMsgUtil.returnMsg(RespEnum.ERROR);
	}
	/** 
	 * 删除
	 **/
	@RequestMapping("/delete")
	@ResponseBody
	public RespMsg delete(HttpServletRequest request, HttpServletResponse response, java.lang.Long id) throws Exception {
		RespMsg msg = RespMsgUtil.returnMsg(RespEnum.SUCCESS);
		try {
			ZookProject zookProject = zookProjectService.selectById(id);
			EntityWrapper<ZookNode> wrapper = new EntityWrapper<ZookNode>();
			wrapper.like("node_key", zookProject.getAppname());
			List<ZookNode> list = zookNodeService.selectList(wrapper);
			if(list != null && list.size()> 0) {
				msg = RespMsgUtil.returnMsg(RespEnum.ERROR, "已绑定节点，禁止删除");
			}else {
				zookProjectService.deleteById(id);
			}
		} catch (Exception e) {
			msg = RespMsgUtil.returnMsg(RespEnum.ERROR);
			log.error("项目删除异常", e);
		}
		return msg;
	}
	/** 
	 * 查看详情
	 **/
	@RequestMapping("/show")
	public String delete(HttpServletRequest request, HttpServletResponse response, Model model, java.lang.Long id) throws Exception {
		ZookProject entity = zookProjectService.selectById(id);
		model.addAttribute("entity", entity);
		return "zookproject/show";
	}
}

