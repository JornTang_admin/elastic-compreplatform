package org.elastic.compreplatform.conf.admin.dao;

import org.elastic.compreplatform.common.dao.BaseDao;
import org.elastic.compreplatform.conf.admin.model.ZookNode;
import org.elastic.compreplatform.conf.admin.vo.query.ZookNodeQuery;

import com.github.pagehelper.Page;

/**
 * ClassName: 
 * @Description: 
 * @author 
 * @date 
 */
public interface IZookNodeDao extends BaseDao<ZookNode>{
	
	/**
	 * 分页查询
	 */
	public Page<ZookNode> pagelist(ZookNodeQuery query);
}
