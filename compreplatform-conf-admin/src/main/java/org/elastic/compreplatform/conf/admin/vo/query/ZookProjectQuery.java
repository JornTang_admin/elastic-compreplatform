/*
 * Copyright © 2018 JornTang personal. 
 *
 * All right reserved. 
 *
 * Author JornTang 
 *
 * Date 2018-03-04
 */
package org.elastic.compreplatform.conf.admin.vo.query;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.elastic.compreplatform.common.model.BaseModel;

public class ZookProjectQuery extends BaseModel {
    private static final long serialVersionUID = 3148176768559230877L;
    

	/** 主键ID */
	private java.lang.Long id;
	/** AppName */
	private java.lang.String appname;
	/** 项目名称 */
	private java.lang.String title;

	public java.lang.Long getId() {
		return this.id;
	}
	
	public void setId(java.lang.Long value) {
		this.id = value;
	}
	
	public java.lang.String getAppname() {
		return this.appname;
	}
	
	public void setAppname(java.lang.String value) {
		this.appname = value;
	}
	
	public java.lang.String getTitle() {
		return this.title;
	}
	
	public void setTitle(java.lang.String value) {
		this.title = value;
	}
	

	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.MULTI_LINE_STYLE);
	}
	
}

