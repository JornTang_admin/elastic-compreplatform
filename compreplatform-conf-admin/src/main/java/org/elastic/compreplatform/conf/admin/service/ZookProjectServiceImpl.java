/*
 * Copyright © 2018 JornTang personal. 
 *
 * All right reserved. 
 *
 * Author JornTang 
 *
 * Date 2018-03-04
 */
package org.elastic.compreplatform.conf.admin.service;

import javax.annotation.Resource;

import org.elastic.compreplatform.common.model.PageHelper;
import org.elastic.compreplatform.common.service.impl.BaseServiceImpl;
import org.elastic.compreplatform.conf.admin.dao.IZookProjectDao;
import org.elastic.compreplatform.conf.admin.model.ZookProject;
import org.elastic.compreplatform.conf.admin.vo.query.ZookProjectQuery;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;

@Service
public class ZookProjectServiceImpl extends BaseServiceImpl<ZookProject>{
	
	@Resource
	private IZookProjectDao zookProjectDao;
	
	/**
	 * 分页查询
	 */
	public Page<ZookProject> pagelist(ZookProjectQuery query, PageHelper page){
		com.github.pagehelper.PageHelper.startPage(page.getPage(), page.getLimit());
		return zookProjectDao.pagelist(query);
	}
}
