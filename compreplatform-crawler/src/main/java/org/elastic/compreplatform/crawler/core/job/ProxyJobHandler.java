package org.elastic.compreplatform.crawler.core.job;

import org.elastic.compreplatform.common.constant.ProxyEnum;
import org.elastic.compreplatform.crawler.core.processor.ProxyIProcessor;
import org.elastic.compreplatform.crawler.core.processor.ProxyStat;
import org.elastic.compreplatform.crawler.core.thread.TaskThreadPoolExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.minstone.quartz.core.biz.model.ReturnT;
import com.minstone.quartz.core.handler.IJobHandler;
import com.minstone.quartz.core.handler.annotation.JobHander;

/**
 * ClassName: ProxyJobHandler 
 * @Description: IP免费代理爬取任务
 * @author JornTang
 * @date 2018年2月10日
 */
@JobHander(value="proxyJobHandler")
@Component
public class ProxyJobHandler extends IJobHandler {
	private static Logger log = LoggerFactory.getLogger(ProxyJobHandler.class);
	@Override
	public ReturnT<String> execute(String... params) throws Exception {
		ReturnT<String> returnT = ReturnT.SUCCESS;
		String param = params[0];
		try {
			if(ProxyEnum.IP181.getType().equals(param)){
				ProxyIProcessor.htmlunit(ProxyEnum.IP181.getUrl(), ProxyEnum.IP181.getType());
			}else if(ProxyEnum.XICIDAILI.getType().equals(param)){
				ProxyIProcessor.htmlunit(ProxyEnum.XICIDAILI.getUrl(), ProxyEnum.XICIDAILI.getType());
			}
			//打印统计信息
			TaskThreadPoolExecutor.isEndTask();
			log.info("抓取{}有效IP地址【{}】个", param, ProxyStat.getProxy().get());
			ProxyStat.getProxy().set(0L);
		} catch (Exception e) {
			returnT = ReturnT.FAIL;
		}
		return returnT;
	}
	
}