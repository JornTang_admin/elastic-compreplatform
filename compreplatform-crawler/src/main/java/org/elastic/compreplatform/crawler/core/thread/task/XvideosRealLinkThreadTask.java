package org.elastic.compreplatform.crawler.core.thread.task;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.net.URLConnection;
import java.util.Map;

import org.elastic.compreplatform.common.util.SeqUtil;
import org.elastic.compreplatform.conf.core.core.ConfZkConf;
import org.elastic.compreplatform.crawler.core.elasticsearch.ESRestClient;
import org.elastic.compreplatform.crawler.core.thread.TaskThreadPoolExecutor;
import org.elastic.compreplatform.crawler.util.AnalyUtil;
import org.elastic.compreplatform.crawler.util.HttpclientUtil;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.baomidou.mybatisplus.toolkit.StringUtils;

/**
 * ClassName: XvideosRealLinkThreadTask 
 * @Description: Xvideo分析实际连接
 * @author JornTang
 * @date 2018年3月7日
 */
public class XvideosRealLinkThreadTask implements Runnable,Serializable{  
	private static final long serialVersionUID = -2224584753890457984L;
	private static Logger log = LoggerFactory.getLogger(XvideosRealLinkThreadTask.class);
    private Elements thumbBlocks;
    private Map<String, Object> source;
        
    public XvideosRealLinkThreadTask(Map<String, Object> source,Elements thumbBlocks) {    
        super();
        this.thumbBlocks = thumbBlocks;
        this.source = source;
    }    
    public XvideosRealLinkThreadTask() {    
        super();    
    }
    @Override
    public void run() {
    	try {
			// xvideo high downlaod
			String video_high_link = source.get("video_high_link")!= null?source.get("video_high_link")+"" : "";
			if(StringUtils.isNotEmpty(video_high_link)) {
				buildDownload(source, video_high_link);
			}else {
				// 分析xvideo实际连接
				Map<String,Object> map = AnalyUtil.html5PlayerRegular(thumbBlocks.toString());
				source.putAll(map);
				source.put("handle_type", 1);
				String _video_high_link = source.get("video_high_link")!= null?source.get("video_high_link")+"" : "";
				buildDownload(source, _video_high_link);
			}
			ESRestClient.lowLevelPutIndex("xvideos", "xvideo", source);
		} catch (Exception e) {
			log.error("Xvideo分析实际连接异常", e);
		} finally {
			thumbBlocks = null;
			source = null;
		}
    }
    public static void buildDownload(Map<String,Object> source, String video_high_link) throws IOException {
    	URLConnection conn = HttpclientUtil.get(video_high_link);
		long videoSize = conn.getContentLengthLong();
		if(videoSize> 0L) {
			String xvideoPath = ConfZkConf.get("elastic_compre.xvideo_path");
			String fileFprimalnam = SeqUtil.getUUID();
			String filename = fileFprimalnam + ".mp4";
			xvideoPath = xvideoPath + fileFprimalnam + File.separator + "high";
			//创建目录
			File xvideoFile = new File(xvideoPath);
			if(!xvideoFile.exists()) {
				xvideoFile.mkdirs();
			}
			String position = fileFprimalnam + File.separator + "high" + File.separator + filename;
			source.put("video_high_size", videoSize);
			source.put("video_high_path", position);
			source.put("handle_type", 2);
			String savePath = xvideoPath + File.separator + filename;
			XvideosDownloadThreadTask downloadThreadTask = new XvideosDownloadThreadTask(conn.getInputStream(), new File(savePath));
			TaskThreadPoolExecutor.addTaskToThreadPool(downloadThreadTask);
		}
    }
} 
