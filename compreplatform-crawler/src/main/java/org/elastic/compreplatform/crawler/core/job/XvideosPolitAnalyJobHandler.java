package org.elastic.compreplatform.crawler.core.job;

import org.elastic.compreplatform.crawler.core.processor.ProxyStat;
import org.elastic.compreplatform.crawler.core.processor.XvideosWebHtmlProcessor;
import org.elastic.compreplatform.crawler.core.thread.TaskThreadPoolExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.minstone.quartz.core.biz.model.ReturnT;
import com.minstone.quartz.core.handler.IJobHandler;
import com.minstone.quartz.core.handler.annotation.JobHander;

/**
 * ClassName: XvideosPolitAnalyJobHandler 
 * @Description: Xvideo引导连接分析
 * @author JornTang
 * @date 2018年3月7日
 */
@JobHander(value="xvideosPolitAnaly")
@Component
public class XvideosPolitAnalyJobHandler extends IJobHandler {
	private static Logger log = LoggerFactory.getLogger(XvideosPolitAnalyJobHandler.class);
	@Override
	public ReturnT<String> execute(String... params) throws Exception {
		ReturnT<String> returnT = ReturnT.SUCCESS;
		String param = params[0];
		try {
			XvideosWebHtmlProcessor.excuteXvideosPilotAnaly(Integer.valueOf(param));
			//打印统计信息
			TaskThreadPoolExecutor.isEndTask();
			log.info("已分析有效连接地址【{}】个", ProxyStat.getPolitLink().get());
			ProxyStat.getPolitLink().set(0L);
		} catch (Exception e) {
			returnT = ReturnT.FAIL;
		}
		return returnT;
	}
	
}