package org.elastic.compreplatform.crawler.core.thread.task;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

import org.elastic.compreplatform.common.util.FileUtil;
import org.elastic.compreplatform.crawler.core.processor.ProxyStat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * ClassName: XvideosDownloadThreadTask 
 * @Description: Xvideo下载
 * @author JornTang
 * @date 2018年3月7日
 */
public class XvideosDownloadThreadTask implements Runnable,Serializable{  
	private static final long serialVersionUID = -2224584753890457984L;
	private static Logger log = LoggerFactory.getLogger(XvideosDownloadThreadTask.class);
    private InputStream inputStream;
    private File destFile;
        
    public XvideosDownloadThreadTask(InputStream inputStream, File destFile) {    
        super();
        this.inputStream = inputStream;
        this.destFile = destFile;
    }    
    public XvideosDownloadThreadTask() {    
        super();    
    }
    @Override
    public void run() {
    	try {
    		//开始下载文件
    		FileUtil.inputstreamToFile(inputStream, destFile);
			//设置统计信息
			ProxyStat.getXvideoDownload().addAndGet(1L);
		} catch (Exception e) {
			log.error("Xvideo下载异常,path【{}】", destFile.getAbsolutePath(), e);
		} finally {
			if(inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					log.error("Xvideo下载输入流关闭异常", e);
				}
			}
			destFile = null;
		}
    }
} 
