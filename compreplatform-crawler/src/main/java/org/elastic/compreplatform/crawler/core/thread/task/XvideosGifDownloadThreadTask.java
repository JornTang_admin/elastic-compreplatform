package org.elastic.compreplatform.crawler.core.thread.task;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.elastic.compreplatform.common.util.EncryptUtil;
import org.elastic.compreplatform.common.util.SeqUtil;
import org.elastic.compreplatform.common.util.SpringContextUtil;
import org.elastic.compreplatform.crawler.controller.CraIpProxyController;
import org.elastic.compreplatform.crawler.core.elasticsearch.ESRestClient;
import org.elastic.compreplatform.crawler.core.elasticsearch.ESqlTemplate;
import org.elastic.compreplatform.crawler.core.processor.ProxyStat;
import org.elastic.compreplatform.crawler.model.CraIpProxy;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.baomidou.mybatisplus.toolkit.StringUtils;

/**
 * ClassName: XvideosDownloadThreadTask 
 * @Description: Xvideo下载
 * @author JornTang
 * @date 2018年3月7日
 */
public class XvideosGifDownloadThreadTask implements Runnable,Serializable{  
	private static final long serialVersionUID = -2224584753890457984L;
	private static Logger log = LoggerFactory.getLogger(XvideosGifDownloadThreadTask.class);
    private String downloadUrl;
        
    public XvideosGifDownloadThreadTask(String downloadUrl) {    
        super();
        this.downloadUrl = downloadUrl;
    }    
    public XvideosGifDownloadThreadTask() {    
        super();    
    }
    @Override
    public void run() {
    	try {
			if(StringUtils.isNotEmpty(downloadUrl)) {
				//设置统计信息
				ProxyStat.getPolitLink().addAndGet(1L);
			}
		} catch (Exception e) {
			log.error("Xvideo下载异常,url【{}】", downloadUrl, e);
		} finally {
			downloadUrl = null;
		}
    }
} 
