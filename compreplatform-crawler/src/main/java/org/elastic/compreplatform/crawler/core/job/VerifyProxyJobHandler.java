package org.elastic.compreplatform.crawler.core.job;

import org.elastic.compreplatform.crawler.core.processor.ProxyIProcessor;
import org.elastic.compreplatform.crawler.core.processor.ProxyStat;
import org.elastic.compreplatform.crawler.core.thread.TaskThreadPoolExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.minstone.quartz.core.biz.model.ReturnT;
import com.minstone.quartz.core.handler.IJobHandler;
import com.minstone.quartz.core.handler.annotation.JobHander;

/**
 * ClassName: VerifyProxyJobHandler 
 * @Description: 有效的免费代理IP过滤
 * @author JornTang
 * @date 2018年2月11日
 */
@JobHander(value="verifyProxyJobHandler")
@Component
public class VerifyProxyJobHandler extends IJobHandler {
	private static Logger log = LoggerFactory.getLogger(VerifyProxyJobHandler.class);
	@Override
	public ReturnT<String> execute(String... params) throws Exception {
		ReturnT<String> returnT = ReturnT.SUCCESS;
		try {
			ProxyIProcessor.verifyProxy(0, 10, 1);
			//打印统计信息
			TaskThreadPoolExecutor.isEndTask();
			log.info("有效过滤无效代理IP【{}】", ProxyStat.getVerifyProxy().get());
			ProxyStat.getVerifyProxy().set(0L);
		} catch (Exception e) {
			e.printStackTrace();
			returnT = ReturnT.FAIL;
		}
		return returnT;
	}
	
}