package org.elastic.compreplatform.crawler.core.processor;

import java.util.concurrent.atomic.AtomicLong;

/**
 * ClassName: ProxyStat 
 * @Description: 代理统计
 * @author JornTang
 * @date 2018年2月11日
 */
public class ProxyStat {
	private static AtomicLong proxy = new AtomicLong();
	private static AtomicLong verifyProxy = new AtomicLong();
	private static AtomicLong politLink = new AtomicLong();
	private static AtomicLong xvideoDownload = new AtomicLong();
	
	public static AtomicLong getXvideoDownload() {
		return xvideoDownload;
	}

	public static void setXvideoDownload(AtomicLong xvideoDownload) {
		ProxyStat.xvideoDownload = xvideoDownload;
	}

	public static AtomicLong getPolitLink() {
		return politLink;
	}

	public static void setPolitLink(AtomicLong politLink) {
		ProxyStat.politLink = politLink;
	}

	public static AtomicLong getProxy() {
		return proxy;
	}

	public static void setProxy(AtomicLong proxy) {
		ProxyStat.proxy = proxy;
	}

	public static AtomicLong getVerifyProxy() {
		return verifyProxy;
	}

	public static void setVerifyProxy(AtomicLong verifyProxy) {
		ProxyStat.verifyProxy = verifyProxy;
	}
	
}
