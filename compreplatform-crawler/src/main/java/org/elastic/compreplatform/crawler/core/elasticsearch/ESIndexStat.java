package org.elastic.compreplatform.crawler.core.elasticsearch;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

/**
 * ClassName: ESIndexStat 
 * @Description: 索引统计信息
 * @author JornTang
 * @date 2017年12月23日
 */
public class ESIndexStat {
	/**
	 * 单件索引量
	 */
	private static AtomicLong docIndex = new AtomicLong();
	/**
	 * 案卷索引量
	 */
	private static AtomicLong volIndex = new AtomicLong();
	/**
	 * 项目索引量
	 */
	private static AtomicLong proIndex = new AtomicLong();
	/**
	 * 索引耗时
	 */
	private static AtomicLong times = new AtomicLong();
	/**
	 * 流水号重复验证
	 */
	private static Set<String> repeat= new HashSet<String>();
	
	
	public static AtomicLong getProIndex() {
		return proIndex;
	}
	public static AtomicLong getDocIndex() {
		return docIndex;
	}
	public static AtomicLong getTimes() {
		return times;
	}
	public static AtomicLong getVolIndex() {
		return volIndex;
	}
	public static Set<String> getRepeat() {
		return repeat;
	}
}
