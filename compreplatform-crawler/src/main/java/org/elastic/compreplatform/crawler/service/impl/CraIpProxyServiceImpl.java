package org.elastic.compreplatform.crawler.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.elastic.compreplatform.common.constant.RespEnum;
import org.elastic.compreplatform.common.model.PageHelper;
import org.elastic.compreplatform.common.model.RespMsg;
import org.elastic.compreplatform.common.util.RespMsgUtil;
import org.elastic.compreplatform.crawler.dao.ICraIpProxyDao;
import org.elastic.compreplatform.crawler.model.CraIpProxy;
import org.elastic.compreplatform.crawler.service.ICraIpProxyService;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;

/**
 * ClassName: 
 * @Description: 
 * @author JornTang
 * @date 
 */

@Service
public class CraIpProxyServiceImpl implements ICraIpProxyService{
	@Resource
	private ICraIpProxyDao craIpProxyDao;

	@Override
	public RespMsg save(CraIpProxy craIpProxy) {
		int result = craIpProxyDao.insert(craIpProxy);
		return result> 0?RespMsgUtil.returnMsg(RespEnum.SUCCESS): RespMsgUtil.returnMsg(RespEnum.ERROR);
	}
	/**
	 * @Description: 根据IP、端口判断是否存在
	 * @param craIpProxy
	 * @return   
	 * @return RespMsg  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月11日
	 */
	@Override
	public RespMsg findListByItem(CraIpProxy craIpProxy) {
		List<CraIpProxy> result = craIpProxyDao.findListByItem(craIpProxy);
		return result.size()> 0?RespMsgUtil.returnMsg(RespEnum.SUCCESS): RespMsgUtil.returnMsg(RespEnum.ERROR);
	}
	/**
	 * @Description: 获取响应时间最短的代理
	 * @return   
	 * @return CraIpProxy  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月11日
	 */
	@Override
	public CraIpProxy findOptimalProxy() {
		return craIpProxyDao.findOptimalProxy();
	}
	/**
	 * @Description: 获取代理集合
	 * @param offoset
	 * @param pageSize
	 * @return   
	 * @return List<CraIpProxy>  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月11日
	 */
	@Override
	public List<CraIpProxy> findPageList(int offoset, int pageSize) {
		return craIpProxyDao.findPageList(offoset, pageSize);
	}
	/**
	 * @Description: 删除代理IP
	 * @param proxy
	 * @return   
	 * @return Object  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月11日
	 */
	@Override
	public void delete(CraIpProxy proxy) {
		craIpProxyDao.delete(proxy);
	}
	/**
	 * @Description: 分页查询IP代理列表
	 * @param proxy
	 * @param page
	 * @return   
	 * @return Page<CraIpProxy>  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月19日
	 */
	@Override
	public Page<CraIpProxy> proxyPageList(CraIpProxy proxy, PageHelper page) {
		return craIpProxyDao.proxyPageList(proxy, page.getPage(), page.getLimit(), page.getSortColumns());
	}
	/**
	 * @Description: 随机获取响应时间最短前20任意代理
	 * @param random
	 * @return   
	 * @return CraIpProxy  
	 * @throws
	 * @author JornTang
	 * @date 2018年3月7日
	 */
	@Override
	public CraIpProxy getRandomProxy(int random) {
		return craIpProxyDao.getRandomProxy(random);
	}
}
