package org.elastic.compreplatform.crawler.service;

import java.util.List;

import org.elastic.compreplatform.common.model.PageHelper;
import org.elastic.compreplatform.common.model.RespMsg;
import org.elastic.compreplatform.crawler.model.CraIpProxy;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;

/**
 * ClassName: ICraIpProxyService 
 * @Description: 
 * @author JornTang
 * @date 2018年2月10日
 */
@Service
public interface ICraIpProxyService{
	/**
	 * @Description: 保存新增对象
	 * @param craIpProxy
	 * @return   
	 * @return RespMsg  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月10日
	 */
	RespMsg save(CraIpProxy craIpProxy);
	/**
	 * @Description: 根据IP、端口判断是否存在
	 * @param craIpProxy
	 * @return   
	 * @return RespMsg  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月11日
	 */
	RespMsg findListByItem(CraIpProxy craIpProxy);
	/**
	 * @Description: 获取响应时间最短的代理
	 * @return   
	 * @return CraIpProxy  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月11日
	 */
	CraIpProxy findOptimalProxy();
	/**
	 * @Description: 获取代理集合
	 * @param offoset
	 * @param pageSize
	 * @return   
	 * @return List<CraIpProxy>  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月11日
	 */
	List<CraIpProxy> findPageList(int offoset, int pageSize);
	/**
	 * @Description: 删除代理IP
	 * @param proxy
	 * @return   
	 * @return Object  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月11日
	 */
	void delete(CraIpProxy proxy);
	/**
	 * @Description: 分页查询IP代理列表
	 * @param proxy
	 * @param page
	 * @return   
	 * @return Page<CraIpProxy>  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月19日
	 */
	Page<CraIpProxy> proxyPageList(CraIpProxy proxy, PageHelper page);
	/**
	 * @Description: 随机获取响应时间最短前20任意代理
	 * @param random
	 * @return   
	 * @return CraIpProxy  
	 * @throws
	 * @author JornTang
	 * @date 2018年3月7日
	 */
	CraIpProxy getRandomProxy(int random);
}
