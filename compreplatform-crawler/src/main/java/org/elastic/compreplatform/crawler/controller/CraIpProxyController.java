package org.elastic.compreplatform.crawler.controller;

import java.util.List;

import javax.annotation.Resource;

import org.elastic.compreplatform.common.model.PageHelper;
import org.elastic.compreplatform.common.model.RespMsg;
import org.elastic.compreplatform.common.util.PageHelperUtil;
import org.elastic.compreplatform.crawler.model.CraIpProxy;
import org.elastic.compreplatform.crawler.service.ICraIpProxyService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.Page;

/**
 * ClassName: 
 * @Description: 
 * @author JornTang
 * @date 
 */
@Controller
@RequestMapping("/craIpProxy")
public class CraIpProxyController{
	@Resource
	private ICraIpProxyService craIpProxyService;
	/**
	 * @Description: 保存新增对象
	 * @param craIpProxy
	 * @return
	 * @throws Exception   
	 * @return RespMsg  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月10日
	 */
	public RespMsg save(CraIpProxy craIpProxy){
		return craIpProxyService.save(craIpProxy);
	}
	/**
	 * @Description: 根据IP、端口判断是否存在
	 * @param craIpProxy
	 * @return
	 * @throws Exception   
	 * @return RespMsg  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月10日
	 */
	public RespMsg findListByItem(CraIpProxy craIpProxy){
		return craIpProxyService.findListByItem(craIpProxy);
	}
	/**
	 * @Description: 获取响应时间最短的代理
	 * @return   
	 * @return CraIpProxy  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月11日
	 */
	public CraIpProxy findOptimalProxy() {
		return craIpProxyService.findOptimalProxy();
	}
	/**
	 * @Description: 获取代理集合
	 * @param offoset
	 * @param pageSize
	 * @return   
	 * @return List<CraIpProxy>  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月11日
	 */
	public List<CraIpProxy> findPageList(int offoset, int pageSize) {
		return craIpProxyService.findPageList(offoset, pageSize);
	}
	/**
	 * @Description: 跳转到IP代理列表页面
	 * @return   
	 * @return ModelAndView  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月19日
	 */
	@RequestMapping("/toProxyList.do")
	public ModelAndView toProxyList(){
		ModelAndView view = new ModelAndView();
		view.setViewName("proxy/proxy-list");
		return view;
	}
	/**
	 * @Description: 分页查询IP代理列表
	 * @param proxy
	 * @param page
	 * @return   
	 * @return PageHelper  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月19日
	 */
	@RequestMapping("/proxyPageList.do")
	@ResponseBody
	public PageHelper proxyPageList(CraIpProxy proxy, PageHelper page) {
		Page<CraIpProxy> list = (Page<CraIpProxy>) craIpProxyService.proxyPageList(proxy, page);
        return PageHelperUtil.doHandlePage(list);
	}
	/**
	 * @Description: 删除代理IP
	 * @param proxy   
	 * @return void  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月11日
	 */
	public void delete(CraIpProxy proxy) {
		craIpProxyService.delete(proxy);
	}
	/**
	 * @Description: 随机获取响应时间最短前20任意代理
	 * @param random
	 * @return   
	 * @return CraIpProxy  
	 * @throws
	 * @author JornTang
	 * @date 2018年3月7日
	 */
	public CraIpProxy getRandomProxy(int random) {
		return craIpProxyService.getRandomProxy(random);
	}
}

