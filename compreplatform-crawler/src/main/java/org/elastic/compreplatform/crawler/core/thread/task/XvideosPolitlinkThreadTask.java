package org.elastic.compreplatform.crawler.core.thread.task;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.elastic.compreplatform.common.util.EncryptUtil;
import org.elastic.compreplatform.common.util.SeqUtil;
import org.elastic.compreplatform.common.util.SpringContextUtil;
import org.elastic.compreplatform.crawler.controller.CraIpProxyController;
import org.elastic.compreplatform.crawler.core.elasticsearch.ESRestClient;
import org.elastic.compreplatform.crawler.core.elasticsearch.ESqlTemplate;
import org.elastic.compreplatform.crawler.core.processor.ProxyStat;
import org.elastic.compreplatform.crawler.model.CraIpProxy;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.baomidou.mybatisplus.toolkit.StringUtils;

/**
 * ClassName: XvideosPolitlinkThreadTask 
 * @Description: Xvideo分析引导连接
 * @author JornTang
 * @date 2018年3月7日
 */
public class XvideosPolitlinkThreadTask implements Runnable,Serializable{  
	private static final long serialVersionUID = -2224584753890457984L;
	private static Logger log = LoggerFactory.getLogger(XvideosPolitlinkThreadTask.class);
    private Elements thumbBlocks;
        
    public XvideosPolitlinkThreadTask(Elements thumbBlocks) {    
        super();
        this.thumbBlocks = thumbBlocks;
    }    
    public XvideosPolitlinkThreadTask() {    
        super();    
    }
    @Override
    public void run() {
    	try {
			if(thumbBlocks != null) {
				for (int i = 0; i < thumbBlocks.size(); i++) {
					Map<String, Object> xvideoMapper = new HashMap<String, Object>();
					xvideoMapper.put("type", "xvideo");
					xvideoMapper.put("handle_type", "0");
					Element thumbBlock = thumbBlocks.get(i);
					Element thumbInside = thumbBlock.getElementsByClass("thumb-inside").first();
					Element thumb = thumbInside.getElementsByClass("thumb").first();
					//获取引导连接
					Element thumb_a = thumb.getElementsByTag("a").first();
					String polit_link = thumb_a.attr("href");
					xvideoMapper.put("documentId", SeqUtil.getSeq());
					xvideoMapper.put("polit_link", polit_link);
					xvideoMapper.put("hash", EncryptUtil.md5Encrypt(polit_link));
					
					Element thumb_img = thumb.getElementsByTag("img").first();
					String thumb_gif_link = thumb_img.attr("src");
					String thumb169_link = thumb_img.attr("data-src");
					xvideoMapper.put("thumb_gif_link", thumb_gif_link);
					xvideoMapper.put("thumb169_link", thumb169_link);
					
					Elements thumbInside_p = thumbBlock.getElementsByTag("p");
					String title = thumbInside_p.get(0).getElementsByTag("a").get(0).attr("title");
					String views = thumbInside_p.get(1).getElementsByClass("bg").get(0).getElementsByTag("span").get(1).html();
					String duration = thumbBlock.getElementsByClass("duration").get(0).html();
					xvideoMapper.put("title", title);
					xvideoMapper.put("views", StringUtils.isNotEmpty(views)?views.replace("-", "").trim(): "");
					xvideoMapper.put("duration", duration);
					//全文检索字段
					xvideoMapper.put("fullText", title);
					
					//判断连接是否重复
					long count = ESqlTemplate.searchBySql("select count(*) as count from xvideos where hash='" + xvideoMapper.get("hash") + "'", "count");
					if(count< 1) {
						ESRestClient.highLevelPutIndex("xvideos", "xvideo", xvideoMapper);
						//设置统计信息
						ProxyStat.getPolitLink().addAndGet(1L);
					}
				}
			}
		} catch (Exception e) {
			log.error("Xvideo分析引导连接异常", e);
		} finally {
			thumbBlocks = null;
		}
    }
} 
