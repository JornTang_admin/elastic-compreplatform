package org.elastic.compreplatform.crawler.core.job;

import org.elastic.compreplatform.crawler.core.processor.XvideosWebHtmlProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.minstone.quartz.core.biz.model.ReturnT;
import com.minstone.quartz.core.handler.IJobHandler;
import com.minstone.quartz.core.handler.annotation.JobHander;

/**
 * ClassName: XvideosPolitAnalyJobHandler 
 * @Description: Xvideo实际连接分析
 * @author JornTang
 * @date 2018年3月7日
 */
@JobHander(value="xvideosRealAnalyJobHandler")
@Component
public class XvideosRealAnalyJobHandler extends IJobHandler {
	private static Logger log = LoggerFactory.getLogger(XvideosRealAnalyJobHandler.class);
	@Override
	public ReturnT<String> execute(String... params) throws Exception {
		ReturnT<String> returnT = ReturnT.SUCCESS;
		try {
			XvideosWebHtmlProcessor.excuteAllLinks(0, 15, 1);
		} catch (Exception e) {
			returnT = ReturnT.FAIL;
		}
		return returnT;
	}
	
}