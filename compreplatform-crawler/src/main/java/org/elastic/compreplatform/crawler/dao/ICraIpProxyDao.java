package org.elastic.compreplatform.crawler.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.elastic.compreplatform.crawler.model.CraIpProxy;

import com.github.pagehelper.Page;

/**
 * ClassName: ICraIpProxyDao 
 * @Description: IP代理持久化类
 * @author JornTang
 * @date 2018年2月10日
 */
public interface ICraIpProxyDao {
	
	int deleteById(java.lang.Long id);

    int insert(CraIpProxy craIpProxy);
    /**
     * @Description: 根据IP、端口判断是否存在
     * @param craIpProxy
     * @return   
     * @return List<CraIpProxy>  
     * @throws
     * @author JornTang
     * @date 2018年2月11日
     */
	List<CraIpProxy> findListByItem(@Param("proxy")CraIpProxy proxy);
	/**
	 * @Description: 获取响应时间最短的代理
	 * @return   
	 * @return CraIpProxy  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月11日
	 */
	CraIpProxy findOptimalProxy();
	/**
	 * @Description: 获取代理集合
	 * @param offoset
	 * @param pageSize
	 * @return   
	 * @return List<CraIpProxy>  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月11日
	 */
	List<CraIpProxy> findPageList(@Param("offoset")int offoset, @Param("pageSize")int pageSize);
	/**
	 * @Description: 删除代理IP
	 * @param proxy   
	 * @return void  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月11日
	 */
	void delete(CraIpProxy proxy);
	/**
	 * @Description: 分页查询IP代理列表
	 * @param proxy
	 * @param offset
	 * @param limit
	 * @param sortColumns
	 * @return   
	 * @return Page<CraIpProxy>  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月19日
	 */
	Page<CraIpProxy> proxyPageList(@Param("proxy")CraIpProxy proxy, @Param("pageNum")int offset, @Param("pageSize")int limit,
			@Param("sortColumns")String sortColumns);
	/**
	 * @Description: 随机获取响应时间最短前20任意代理
	 * @param random
	 * @return   
	 * @return CraIpProxy  
	 * @throws
	 * @author JornTang
	 * @date 2018年3月7日
	 */
	CraIpProxy getRandomProxy(int random);
}
