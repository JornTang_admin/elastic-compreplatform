package org.elastic.compreplatform.crawler.exception;

public class ESIndexException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 264192768761282028L;
	
	public ESIndexException() {
	}

	public ESIndexException(String message) {
		super(message);
	}

	public ESIndexException(Throwable cause) {
		super(cause);
	}

	public ESIndexException(String message, Throwable cause) {
		super(message, cause);
	}
}
