package org.elastic.compreplatform.crawler.model;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

/**
 * ClassName: CraIpProxy 
 * @Description: 
 * @author JornTang
 * @date 2018年2月10日
 */
public class CraIpProxy implements java.io.Serializable{
	private static final long serialVersionUID = 5454155825314635342L;
	
	//alias
	public static final String TABLE_ALIAS = "CraIpProxy";
	public static final String ALIAS_ID = "主键ID";
	public static final String ALIAS_PROXY_IP = "代理ip地址";
	public static final String ALIAS_PROXY_PORT = "代理端口";
	public static final String ALIAS_CRYP_LEVEL = "代理级别";
	public static final String ALIAS_PROXY_TYPE = "代理类型";
	public static final String ALIAS_RESP_TIME = "响应时间";
	public static final String ALIAS_CITY_ADDRESS = "地理位置";
	public static final String ALIAS_VERIFY_LAST_TIME = "最后验证时间";
	public static final String ALIAS_CREAT_TIME = "创建时间";
	public static final String ALIAS_PROXY_FROM = "代理来源";
	
	//date formats
	public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	public static final String FORMAT_VERIFY_LAST_TIME = DATE_FORMAT;
	public static final String FORMAT_CREAT_TIME = DATE_FORMAT;
	
	//Can be used directly: @Length (max=50, message= "username length can not be greater than 50") display error messages
	//columns START
    /**
     * 主键ID       db_column: id 
     */	
	
	private java.lang.Long id;
    /**
     * 代理ip地址       db_column: proxy_ip 
     */	
	@NotBlank @Length(max=50)
	private java.lang.String proxyIp;
    /**
     * 代理端口       db_column: proxy_port 
     */	
	@NotNull 
	private java.lang.Integer proxyPort;
    /**
     * 代理级别       db_column: cryp_level 
     */	
	@NotBlank @Length(max=50)
	private java.lang.String crypLevel;
    /**
     * 代理类型       db_column: proxy_type 
     */	
	@NotBlank @Length(max=50)
	private java.lang.String proxyType;
    /**
     * 响应时间       db_column: resp_time 
     */	
	@NotNull 
	private java.lang.Float respTime;
    /**
     * 地理位置       db_column: city_address 
     */	
	@NotBlank @Length(max=100)
	private java.lang.String cityAddress;
    /**
     * 最后验证时间       db_column: verify_last_time 
     */	
	@NotNull 
	private java.util.Date verifyLastTime;
    /**
     * 创建时间       db_column: creat_time 
     */	
	@NotNull 
	private java.util.Date creatTime;
    /**
     * 代理来源       db_column: proxy_from 
     */	
	@NotBlank @Length(max=100)
	private java.lang.String proxyFrom;
	//columns END

	public CraIpProxy(){
	}

	public CraIpProxy(
		java.lang.Long id
	){
		this.id = id;
	}

	public void setId(java.lang.Long value) {
		this.id = value;
	}
	
	public java.lang.Long getId() {
		return this.id;
	}
	public void setProxyIp(java.lang.String value) {
		this.proxyIp = value;
	}
	
	public java.lang.String getProxyIp() {
		return this.proxyIp;
	}
	public void setProxyPort(java.lang.Integer value) {
		this.proxyPort = value;
	}
	
	public java.lang.Integer getProxyPort() {
		return this.proxyPort;
	}
	public void setCrypLevel(java.lang.String value) {
		this.crypLevel = value;
	}
	
	public java.lang.String getCrypLevel() {
		return this.crypLevel;
	}
	public void setProxyType(java.lang.String value) {
		this.proxyType = value;
	}
	
	public java.lang.String getProxyType() {
		return this.proxyType;
	}
	public void setRespTime(java.lang.Float value) {
		this.respTime = value;
	}
	
	public java.lang.Float getRespTime() {
		return this.respTime;
	}
	public void setCityAddress(java.lang.String value) {
		this.cityAddress = value;
	}
	
	public java.lang.String getCityAddress() {
		return this.cityAddress;
	}
	public void setVerifyLastTime(java.util.Date value) {
		this.verifyLastTime = value;
	}
	
	public java.util.Date getVerifyLastTime() {
		return this.verifyLastTime;
	}
	public void setCreatTime(java.util.Date value) {
		this.creatTime = value;
	}
	
	public java.util.Date getCreatTime() {
		return this.creatTime;
	}
	public void setProxyFrom(java.lang.String value) {
		this.proxyFrom = value;
	}
	
	public java.lang.String getProxyFrom() {
		return this.proxyFrom;
	}

	public String toString() {
		return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
			.append("Id",getId())
			.append("ProxyIp",getProxyIp())
			.append("ProxyPort",getProxyPort())
			.append("CrypLevel",getCrypLevel())
			.append("ProxyType",getProxyType())
			.append("RespTime",getRespTime())
			.append("CityAddress",getCityAddress())
			.append("VerifyLastTime",getVerifyLastTime())
			.append("CreatTime",getCreatTime())
			.append("ProxyFrom",getProxyFrom())
			.toString();
	}
	
	public int hashCode() {
		return new HashCodeBuilder()
			.append(getId())
			.toHashCode();
	}
	
	public boolean equals(Object obj) {
		if(obj instanceof CraIpProxy == false) return false;
		if(this == obj) return true;
		CraIpProxy other = (CraIpProxy)obj;
		return new EqualsBuilder()
			.append(getId(),other.getId())
			.isEquals();
	}
}

