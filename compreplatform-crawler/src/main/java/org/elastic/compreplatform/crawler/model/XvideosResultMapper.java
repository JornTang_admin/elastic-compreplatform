package org.elastic.compreplatform.crawler.model;

import java.util.Map;


/**
 * ClassName: XvideosResultMapper 
 * @Description: xvideos索引映射类
 * @author JornTang
 * @date 2017年12月21日
 */
public class XvideosResultMapper{
	/**
	 * 耗时，单位毫秒
	 */
	private int took;
	/**
	 * 分片
	 */
	private Map<String,Integer> _shards;
	/**
	 * 
	 */
	private Map<String,Object> hits;
	/**
	 * 聚合
	 */
	private Map<String,Object> aggregations;
	public int getTook() {
		return took;
	}
	public void setTook(int took) {
		this.took = took;
	}
	public Map<String, Integer> get_shards() {
		return _shards;
	}
	public void set_shards(Map<String, Integer> _shards) {
		this._shards = _shards;
	}
	public Map<String, Object> getHits() {
		return hits;
	}
	public void setHits(Map<String, Object> hits) {
		this.hits = hits;
	}
	public Map<String, Object> getAggregations() {
		return aggregations;
	}
	public void setAggregations(Map<String, Object> aggregations) {
		this.aggregations = aggregations;
	}
}

