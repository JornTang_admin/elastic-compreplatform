package org.elastic.compreplatform.crawler.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * ClassName: AnalyUtil 
 * @Description: 解析工具类
 * @author JornTang
 * @date 2018年3月8日
 */

import org.elastic.compreplatform.common.util.SpringContextUtil;
import org.elastic.compreplatform.crawler.controller.CraIpProxyController;
import org.elastic.compreplatform.crawler.model.CraIpProxy;
public class AnalyUtil {
	private static CraIpProxyController proxyController = SpringContextUtil.getBean(CraIpProxyController.class);
	/**
	 * @Description: 正则分析xvideo实际连接
	 * @param html5Player
	 * @return   
	 * @return Map<String,Object>  
	 * @throws
	 * @author JornTang
	 * @date 2018年3月8日
	 */
	public static Map<String, Object> html5PlayerRegular(String html5Player){
		List<Pattern> patternlist = new ArrayList<Pattern>();
		Map<String, Object> map = new HashMap<String, Object>();
		html5Player = html5Player.replace("\n", "");
		List<Object[]> ret = new ArrayList<Object[]>();
		ret.add(new Object[]{"(?<=html5player.setVideoUrlHigh\\(\\')(.+?)(?=\\'\\))", Pattern.CASE_INSENSITIVE});
		ret.add(new Object[]{"(?<=html5player.setVideoUrlLow\\(\\')(.+?)(?=\\'\\))", Pattern.CASE_INSENSITIVE});
		ret.add(new Object[]{"(?<=html5player.setVideoHLS\\(\\')(.+?)(?=\\'\\))", Pattern.CASE_INSENSITIVE});
		patternlist.add(Pattern.compile("(?<=html5player.setVideoUrlHigh\\(\\')(.+?)(?=\\'\\))", Pattern.CASE_INSENSITIVE));
		patternlist.add(Pattern.compile("(?<=html5player.setVideoUrlLow\\(\\')(.+?)(?=\\'\\))", Pattern.CASE_INSENSITIVE));
		patternlist.add(Pattern.compile("(?<=html5player.setVideoHLS\\(\\')(.+?)(?=\\'\\))", Pattern.CASE_INSENSITIVE));
		for (int i = 0; i < patternlist.size(); i++) {
			Pattern pattern = patternlist.get(i);
			Matcher matcher = pattern.matcher(html5Player);
			while(matcher.find()){
				String str = matcher.group();
				if(pattern.toString().contains("VideoUrlHigh")) {
					map.put("video_high_link", str);
				}else if(pattern.toString().contains("VideoUrlLow")) {
					map.put("video_low_link", str);
				}else if(pattern.toString().contains("VideoHLS")) {
					map.put("video_hls_link", str);
				}
			}
		}
		return map;
	}
	/**
	 * @Description: 随机获取响应时间最短前50任意代理
	 * @return   
	 * @return CraIpProxy  
	 * @throws
	 * @author JornTang
	 * @date 2018年3月8日
	 */
	public static CraIpProxy getCraIpProxy() {
		//随机获取响应时间最短前50任意代理
		int random = (int)(1+Math.random()*(50-1+1));
		return proxyController.getRandomProxy(random);
	}
}
