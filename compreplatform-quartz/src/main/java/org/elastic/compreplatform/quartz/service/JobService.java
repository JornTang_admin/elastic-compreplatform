package org.elastic.compreplatform.quartz.service;


import java.util.List;
import java.util.Map;

import org.elastic.compreplatform.common.model.PageHelper;
import org.elastic.compreplatform.common.model.RespMsg;
import org.elastic.compreplatform.quartz.model.JobGroup;
import org.elastic.compreplatform.quartz.model.JobInfo;
import org.elastic.compreplatform.quartz.model.JobRegistry;

import com.github.pagehelper.Page;
import com.minstone.quartz.core.biz.model.ReturnT;

/**
 * ClassName: JobService 
 * @Description: core job action for
 * @author JornTang
 * @email 957707261@qq.com
 * @date 2017年8月17日
 */
public interface JobService {
	
	public Map<String, Object> pageList(int start, int length, int jobGroup, String executorHandler, String filterTime);
	
	public RespMsg add(JobInfo jobInfo);
	
	public RespMsg reschedule(JobInfo jobInfo);
	
	public RespMsg remove(int id);
	
	public RespMsg pause(int id);
	
	public RespMsg resume(int id);
	
	public RespMsg triggerJob(int id);

	public Map<String,Object> dashboardInfo();
	/**
	 * @Description: 任务执行报表
	 * @return   
	 * @return RespMsg  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月9日
	 */
	public RespMsg triggerChartDate();
	/**
	 * @Description: 获取任务信息表最大ID值
	 * @return   
	 * @return ReturnT<String>  
	 * @throws
	 * @author JornTang
	 * @email 957707261@qq.com
	 * @date 2017年8月25日
	 */
	public ReturnT<Map<String,Integer>> getMaxId();
	
	/**
	 * @Description: 获取注册信息list
	 * @param start
	 * @param length
	 * @param executorClient
	 * @param clientName
	 * @return   
	 * @return Map<String,Object>  
	 * @throws
	 * @author JornTang
	 * @email 957707261@qq.com
	 * @date 2017年8月25日
	 */
	public List<JobRegistry> pageRegistryList(JobRegistry registry, PageHelper page);
	/**
	 * @Description: 生成密钥跟令牌
	 * @param id   
	 * @return void  
	 * @throws
	 * @author JornTang
	 * @email 957707261@qq.com
	 * @date 2017年8月28日
	 */
	public ReturnT<String> addSecretKey(int id)throws Exception;
	/**
	 * @Description: 客户端授权或取消授权
	 * @param id
	 * @param ifGrant   
	 * @return void  
	 * @throws
	 * @author JornTang
	 * @email 957707261@qq.com
	 * @date 2017年8月28日
	 */
	public void updateRegistGrant(JobRegistry registry);
	/**
	 * @Description: 关联分组
	 * @param id
	 * @param groupId
	 * @return   
	 * @return ReturnT<String>  
	 * @throws
	 * @author JornTang
	 * @email 957707261@qq.com
	 * @date 2017年8月29日
	 */
	public void addRelationGroup(int id, int groupId)throws Exception;
	/**
	 * @Description: 查询执行器集合
	 * @param group
	 * @param page
	 * @return   
	 * @return Page<JobGroup>  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月7日
	 */
	public Page<JobGroup> pageJobGroupList(JobGroup group, PageHelper page);
	/**
	 * @Description: 任务列表分页查询
	 * @param jobInfo
	 * @param page
	 * @return   
	 * @return Page<JobGroup>  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月8日
	 */
	public Page<JobInfo> pageJobInfoList(JobInfo jobInfo, PageHelper page);
	/**
	 * @Description: 根据ID获取任务信息
	 * @param id
	 * @return   
	 * @return JobInfo  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月8日
	 */
	public JobInfo findJobInfoById(Integer id);

}
