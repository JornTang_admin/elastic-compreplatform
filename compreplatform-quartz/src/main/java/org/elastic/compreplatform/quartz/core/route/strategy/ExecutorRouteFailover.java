package org.elastic.compreplatform.quartz.core.route.strategy;

import java.util.ArrayList;

import org.elastic.compreplatform.quartz.core.route.ExecutorRouter;
import org.elastic.compreplatform.quartz.core.schedule.JobDynamicScheduler;
import org.elastic.compreplatform.quartz.core.trigger.JobTrigger;

import com.minstone.quartz.core.biz.ExecutorBiz;
import com.minstone.quartz.core.biz.model.ReturnT;
import com.minstone.quartz.core.biz.model.TriggerParam;

/**
 * ClassName: ExecutorRouteFailover 
 * @Description: 
 * @author JornTang
 * @email 957707261@qq.com
 * @date 2017年8月17日
 */
public class ExecutorRouteFailover extends ExecutorRouter {

    public String route(int jobId, ArrayList<String> addressList) {
        return addressList.get(0);
    }

    @Override
    public ReturnT<String> routeRun(TriggerParam triggerParam, ArrayList<String> addressList) {

        StringBuffer beatResultSB = new StringBuffer();
        for (String address : addressList) {
            // beat
            ReturnT<String> beatResult = null;
            try {
                ExecutorBiz executorBiz = JobDynamicScheduler.getExecutorBiz(address);
                beatResult = executorBiz.beat();
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                beatResult = new ReturnT<String>(ReturnT.FAIL_CODE, ""+e );
            }
            beatResultSB.append( (beatResultSB.length()>0)?"<br><br>":"")
                    .append("�����⣺")
                    .append("<br>address��").append(address)
                    .append("<br>code��").append(beatResult.getCode())
                    .append("<br>msg��").append(beatResult.getMsg());

            // beat success
            if (beatResult.getCode() == ReturnT.SUCCESS_CODE) {

                ReturnT<String> runResult = JobTrigger.runExecutor(triggerParam, address);
                beatResultSB.append("<br><br>").append(runResult.getMsg());

                // result
                runResult.setMsg(beatResultSB.toString());
                runResult.setContent(address);
                return runResult;
            }
        }
        return new ReturnT<String>(ReturnT.FAIL_CODE, beatResultSB.toString());

    }
}
