package org.elastic.compreplatform.quartz.core.route.strategy;

import java.util.ArrayList;

import org.elastic.compreplatform.quartz.core.route.ExecutorRouter;
import org.elastic.compreplatform.quartz.core.trigger.JobTrigger;

import com.minstone.quartz.core.biz.model.ReturnT;
import com.minstone.quartz.core.biz.model.TriggerParam;

/**
 * 
 * ClassName: ExecutorRouteFirst 
 * @Description: 
 * @author JornTang
 * @email 957707261@qq.com
 * @date 2017年8月17日
 */
public class ExecutorRouteFirst extends ExecutorRouter {

    public String route(int jobId, ArrayList<String> addressList) {
        return addressList.get(0);
    }

    @Override
    public ReturnT<String> routeRun(TriggerParam triggerParam, ArrayList<String> addressList) {

    	// address
        String address = route(triggerParam.getJobId(), addressList);

        //run executor
        ReturnT<String> runResult = JobTrigger.runExecutor(triggerParam, address);
        runResult.setContent(address);
        return runResult;
    }
}
