package org.elastic.compreplatform.quartz.controller;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.elastic.compreplatform.common.constant.RespEnum;
import org.elastic.compreplatform.common.model.PageHelper;
import org.elastic.compreplatform.common.model.RespMsg;
import org.elastic.compreplatform.common.util.PageHelperUtil;
import org.elastic.compreplatform.common.util.RespMsgUtil;
import org.elastic.compreplatform.quartz.dao.JobGroupDao;
import org.elastic.compreplatform.quartz.dao.JobInfoDao;
import org.elastic.compreplatform.quartz.dao.JobRegistryDao;
import org.elastic.compreplatform.quartz.model.JobGroup;
import org.elastic.compreplatform.quartz.model.JobRegistry;
import org.elastic.compreplatform.quartz.service.JobService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.Page;

/**
 * ClassName: JobGroupController 
 * @Description: 
 * @author JornTang
 * @email 957707261@qq.com
 * @date 2017年8月17日
 */
@Controller
@RequestMapping("/jobgroup")
public class JobGroupController {

	@Resource
	public JobInfoDao jobInfoDao;
	@Resource
	public JobGroupDao jobGroupDao;
	@Resource
	public JobRegistryDao jobRegistryDao;
	@Resource
	private JobService jobService;

	@RequestMapping("/index.do")
	public ModelAndView index(Model model) {
		ModelAndView view = new ModelAndView();
		view.setViewName("quartz/jobgroup/jobgroup-index");
		return view;
		// job group (executor)
		//List<JobGroup> list = jobGroupDao.findAll();
		// 获取所有客户端注册信息
		//List<JobRegistry> registries= jobRegistryDao.findAllRegist();
		//model.addAttribute("list", list);
		//model.addAttribute("registries", registries);
	}
	/**
	 * @Description: 查询执行器集合
	 * @param group
	 * @param page
	 * @return   
	 * @return PageHelper  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月7日
	 */
	@RequestMapping("/pageList.do")
	@ResponseBody
	public PageHelper pageList(JobGroup group, PageHelper page){
		Page<JobGroup> list = (Page<JobGroup>) jobService.pageJobGroupList(group, page);
        return PageHelperUtil.doHandlePage(list);
	}
	/**
	 * @Description: 跳转到添加或修改页面
	 * @return   
	 * @return ModelAndView  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月8日
	 */
	@RequestMapping("/toAddOrUpdate.do")
	public ModelAndView toAddOrUpdate(JobGroup group){
		ModelAndView view = new ModelAndView();
		if(group.getId()!= null){
			group = jobGroupDao.load(group.getId());
			view.addObject("group", group);
		}
		// 获取所有客户端注册信息
		List<JobRegistry> regists= jobRegistryDao.findAllRegist();
		view.setViewName("quartz/jobgroup/addOrUpdate");
		view.addObject("regists", regists);
		return view;
	}
	/**
	 * @Description: 保存执行器配置
	 * @param jobGroup
	 * @return   
	 * @return RespMsg  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月8日
	 */
	@RequestMapping("/save")
	@ResponseBody
	public RespMsg save(JobGroup jobGroup){

		// valid
		if (jobGroup.getAppName()==null || StringUtils.isBlank(jobGroup.getAppName())) {
			return RespMsgUtil.returnMsg(RespEnum.ERROR, "请输入AppName");
		}
		if (jobGroup.getAppName().length()>64) {
			return RespMsgUtil.returnMsg(RespEnum.ERROR, "AppName长度限制为4~64");
		}
		if (jobGroup.getTitle()==null || StringUtils.isBlank(jobGroup.getTitle())) {
			return RespMsgUtil.returnMsg(RespEnum.ERROR, "请输入名称");
		}
		if (jobGroup.getAddressType()!=0) {
			if (StringUtils.isBlank(jobGroup.getAddressList())) {
				return RespMsgUtil.returnMsg(RespEnum.ERROR, "手动录入注册方式，机器地址不可为空");
			}
			String[] addresss = jobGroup.getAddressList().split(",");
			for (String item: addresss) {
				if (StringUtils.isBlank(item)) {
					return RespMsgUtil.returnMsg(RespEnum.ERROR, "机器地址非法");
				}
			}
		}

		int ret = jobGroupDao.save(jobGroup);
		return (ret>0)?RespMsgUtil.returnMsg(RespEnum.SUCCESS):RespMsgUtil.returnMsg(RespEnum.ERROR);
	}
	/**
	 * @Description: 修改执行器配置
	 * @param jobGroup
	 * @return   
	 * @return ReturnT<String>  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月8日
	 */
	@RequestMapping("/update")
	@ResponseBody
	public RespMsg update(JobGroup jobGroup){
		// valid
		if (jobGroup.getAppName()==null || StringUtils.isBlank(jobGroup.getAppName())) {
			return RespMsgUtil.returnMsg(RespEnum.ERROR, "请输入AppName");
		}
		if (jobGroup.getAppName().length()>64) {
			return RespMsgUtil.returnMsg(RespEnum.ERROR, "AppName长度限制为4~64");
		}
		if (jobGroup.getTitle()==null || StringUtils.isBlank(jobGroup.getTitle())) {
			return RespMsgUtil.returnMsg(RespEnum.ERROR, "请输入名称");
		}
		if (jobGroup.getAddressType()!=0) {
			if (StringUtils.isBlank(jobGroup.getAddressList())) {
				return RespMsgUtil.returnMsg(RespEnum.ERROR, "手动录入注册方式，机器地址不可为空");
			}
			String[] addresss = jobGroup.getAddressList().split(",");
			for (String item: addresss) {
				if (StringUtils.isBlank(item)) {
					return RespMsgUtil.returnMsg(RespEnum.ERROR, "机器地址非法");
				}
			}
		}

		int ret = jobGroupDao.update(jobGroup);
		return (ret>0)?RespMsgUtil.returnMsg(RespEnum.SUCCESS):RespMsgUtil.returnMsg(RespEnum.ERROR);
	}

	@RequestMapping("/remove")
	@ResponseBody
	public RespMsg remove(int id){

		// valid
		int count = jobInfoDao.pageListCount(0, 10, id, null);
		if (count > 0) {
			return RespMsgUtil.returnMsg(RespEnum.ERROR, "该分组使用中, 不可删除");
		}

		List<JobGroup> allList = jobGroupDao.findAll();
		if (allList.size() == 1) {
			return RespMsgUtil.returnMsg(RespEnum.ERROR, "删除失败, 系统需要至少预留一个默认分组");
		}

		int ret = jobGroupDao.remove(id);
		return (ret>0)?RespMsgUtil.returnMsg(RespEnum.SUCCESS):RespMsgUtil.returnMsg(RespEnum.ERROR);
	}

}
