package org.elastic.compreplatform.quartz.controller;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.elastic.compreplatform.common.model.PageHelper;
import org.elastic.compreplatform.common.model.RespMsg;
import org.elastic.compreplatform.common.util.PageHelperUtil;
import org.elastic.compreplatform.quartz.core.enums.ExecutorFailStrategyEnum;
import org.elastic.compreplatform.quartz.core.route.ExecutorRouteStrategyEnum;
import org.elastic.compreplatform.quartz.core.schedule.JobDynamicScheduler;
import org.elastic.compreplatform.quartz.dao.JobGroupDao;
import org.elastic.compreplatform.quartz.model.JobGroup;
import org.elastic.compreplatform.quartz.model.JobInfo;
import org.elastic.compreplatform.quartz.service.JobService;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.Page;
import com.minstone.quartz.core.biz.model.ReturnT;
import com.minstone.quartz.core.enums.ExecutorBlockStrategyEnum;
import com.minstone.quartz.core.glue.GlueTypeEnum;

/**
 * ClassName: JobInfoController 
 * @Description: 
 * @author JornTang
 * @email 957707261@qq.com
 * @date 2017年8月17日
 */
@Controller
@RequestMapping("/jobinfo")
public class JobInfoController {

	@Resource
	private JobGroupDao jobGroupDao;
	@Resource
	private JobService jobService;
	
	@RequestMapping("/index.do")
	public String index(Model model) {
		// 任务组
		List<JobGroup> jobGroupList =  jobGroupDao.findAll();
		model.addAttribute("JobGroupList", jobGroupList);
		return "quartz/jobinfo/jobinfo-index";
	}
	/**
	 * @Description: 任务列表分页查询
	 * @param jobInfo
	 * @param page
	 * @return   
	 * @return PageHelper  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月8日
	 */
	@RequestMapping("/pageList")
	@ResponseBody
	public PageHelper pageList(JobInfo jobInfo, PageHelper page) {
		Page<JobInfo> list = (Page<JobInfo>) jobService.pageJobInfoList(jobInfo, page);
		// fill job info
		if (list!=null && list.size()>0) {
			for (JobInfo info : list) {
				JobDynamicScheduler.fillJobInfo(info);
			}
		}
        return PageHelperUtil.doHandlePage(list);
	}
	/**
	 * @Description: 跳转到添加或修改页面
	 * @return   
	 * @return ModelAndView  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月8日
	 */
	@RequestMapping("/toAddOrUpdate.do")
	public ModelAndView toAddOrUpdate(JobInfo jobInfo){
		ModelAndView view = new ModelAndView();
		//枚举-字典
		view.addObject("ExecutorRouteStrategyEnum", ExecutorRouteStrategyEnum.values());	// 路由策略-列表
		view.addObject("GlueTypeEnum", GlueTypeEnum.values());								// Glue类型-字典
		view.addObject("ExecutorBlockStrategyEnum", ExecutorBlockStrategyEnum.values());	// 阻塞处理策略-字典
		view.addObject("ExecutorFailStrategyEnum", ExecutorFailStrategyEnum.values());		// 失败处理策略-字典
		if(jobInfo.getId()!= null){
			jobInfo = jobService.findJobInfoById(jobInfo.getId());
			view.addObject("jobInfo", jobInfo);
		}
		// 任务组
		List<JobGroup> jobGroupList =  jobGroupDao.findAll();
		view.addObject("groupList", jobGroupList);
		view.setViewName("quartz/jobinfo/addOrUpdate");
		return view;
	}
	/**
	 * @Description: 添加任务
	 * @param jobInfo
	 * @return   
	 * @return ReturnT<String> 以JSON格式返回 
	 * @throws
	 * @author JornTang
	 * @email 957707261@qq.com
	 * @date 2017年8月23日
	 */
	@RequestMapping(value="/add")
	@ResponseBody
	public RespMsg add(JobInfo jobInfo) {
		return jobService.add(jobInfo);
	}
	@RequestMapping(value="/jsonp",produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public void jsonp(HttpServletRequest request, HttpServletResponse response) {
		try {
			response.getWriter().write("jsonp" + "(" + "{msg:\"测试jonsp\",stat: true}" + ")");
		} catch (IOException e) {
			e.printStackTrace();
		} //返回jsonp数据  
	}
	/**
	 * @Description: 获取任务信息表最大ID值
	 * @param jobInfo
	 * @return   
	 * @return ReturnT<String>  
	 * @throws
	 * @author JornTang
	 * @email 957707261@qq.com
	 * @date 2017年8月25日
	 */
	@RequestMapping(value="/getMaxId",produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ReturnT<Map<String,Integer>> getMaxId() {
		return jobService.getMaxId();
	}
	/**
	 * @Description: 修改任务信息
	 * @param jobInfo
	 * @return   
	 * @return ReturnT<String>  
	 * @throws
	 * @author JornTang
	 * @email 957707261@qq.com
	 * @date 2017年8月25日
	 */
	@RequestMapping("/reschedule")
	@ResponseBody
	public RespMsg reschedule(JobInfo jobInfo) {
		return jobService.reschedule(jobInfo);
	}
	
	/**
	 * @Description: 删除
	 * @param id
	 * @return   
	 * @return ReturnT<String>  
	 * @throws
	 * @author JornTang
	 * @email 957707261@qq.com
	 * @date 2017年8月25日
	 */
	@RequestMapping("/remove.do")
	@ResponseBody
	public RespMsg remove(int id) {
		return jobService.remove(id);
	}
	
	/**
	 * @Description: 暂停
	 * @param id
	 * @return   
	 * @return ReturnT<String>  
	 * @throws
	 * @author JornTang
	 * @email 957707261@qq.com
	 * @date 2017年8月25日
	 */
	@RequestMapping("/pause.do")
	@ResponseBody
	public RespMsg pause(int id) {
		return jobService.pause(id);
	}
	
	/**
	 * @Description: 恢复
	 * @param id
	 * @return   
	 * @return ReturnT<String>  
	 * @throws
	 * @author JornTang
	 * @email 957707261@qq.com
	 * @date 2017年8月25日
	 */
	@RequestMapping("/resume.do")
	@ResponseBody
	public RespMsg resume(int id) {
		return jobService.resume(id);
	}
	
	/**
	 * @Description: 执行
	 * @param id
	 * @return   
	 * @return ReturnT<String>  
	 * @throws
	 * @author JornTang
	 * @email 957707261@qq.com
	 * @date 2017年8月25日
	 */
	@RequestMapping("/trigger.do")
	@ResponseBody
	public RespMsg triggerJob(int id) {
		return jobService.triggerJob(id);
	}
	
}
