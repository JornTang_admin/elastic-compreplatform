package org.elastic.compreplatform.quartz.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.elastic.compreplatform.quartz.model.JobGroup;

import com.github.pagehelper.Page;

/**
 * ClassName: JobGroupDao 
 * @Description: 
 * @author JornTang
 * @email 957707261@qq.com
 * @date 2017年8月17日
 */
public interface JobGroupDao {

	public List<JobGroup> findAll();

    public List<JobGroup> findByAddressType(@Param("addressType") int addressType);

    public int save(JobGroup jobGroup);

    public int update(JobGroup jobGroup);

    public int remove(@Param("id") int id);

    public JobGroup load(@Param("id") int id);
    /**
     * @Description: 查询执行器集合
     * @param group
     * @param offset
     * @param limit
     * @param sortColumns
     * @return   
     * @return Page<JobGroup>  
     * @throws
     * @author JornTang
     * @date 2018年2月7日
     */
	public Page<JobGroup> pageJobGroupList(@Param("group")JobGroup group, @Param("pageNum")int mysqlOffset,
			@Param("pageSize")int limit, @Param("sortColumns")String sortColumns);
}
