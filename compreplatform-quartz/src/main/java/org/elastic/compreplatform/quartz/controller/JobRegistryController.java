package org.elastic.compreplatform.quartz.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.elastic.compreplatform.common.constant.RespEnum;
import org.elastic.compreplatform.common.model.PageHelper;
import org.elastic.compreplatform.common.model.RespMsg;
import org.elastic.compreplatform.common.util.PageHelperUtil;
import org.elastic.compreplatform.common.util.RespMsgUtil;
import org.elastic.compreplatform.quartz.dao.JobGroupDao;
import org.elastic.compreplatform.quartz.dao.JobRegistryDao;
import org.elastic.compreplatform.quartz.model.JobInfo;
import org.elastic.compreplatform.quartz.model.JobRegistry;
import org.elastic.compreplatform.quartz.service.JobService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.Page;
import com.minstone.quartz.core.biz.model.ReturnT;

/**
 * ClassName: JobInfoController 
 * @Description: 
 * @author JornTang
 * @email 957707261@qq.com
 * @date 2017年8月17日
 */
@Controller
@RequestMapping("/jobregistry")
public class JobRegistryController {
	private static Logger logger = LoggerFactory.getLogger(JobRegistryController.class);
	@Resource
	private JobRegistryDao jobRegistryDao;
	@Resource
	private JobGroupDao jobGroupDao;
	@Resource
	private JobService jobService;
	
	@RequestMapping("/index.do")
	public ModelAndView index(Model model,HttpServletRequest request) {
		ModelAndView view = new ModelAndView();
		view.setViewName("quartz/jobregistry/jobregistry-index");
		return view;
	}
	
	@RequestMapping("/pageList.do")
	@ResponseBody
	public PageHelper pageList(JobRegistry registry, PageHelper page){
		Page<JobRegistry> list = (Page<JobRegistry>) this.jobService.pageRegistryList(registry, page);
        return PageHelperUtil.doHandlePage(list);
	}
	/**
	 * @Description: 关联分组
	 * @return   
	 * @return ReturnT<String>  
	 * @throws
	 * @author JornTang
	 * @email 957707261@qq.com
	 * @date 2017年8月29日
	 */
	@RequestMapping(value="/addRelationGroup",produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ReturnT<String> addRelationGroup(int id,int groupId){
		ReturnT<String> rt= new ReturnT<String>();
		rt.setMsg("关联分组成功");
		rt.setCode(ReturnT.SUCCESS_CODE);
		try {
			this.jobService.addRelationGroup(id,groupId);
		} catch (Exception e) {
			rt.setCode(ReturnT.FAIL_CODE);
			rt.setMsg("关联分组失败");
			logger.error("关联分组异常",e);
		}
		return rt;
	}
	/**
	 * @Description: 客户端授权或取消授权
	 * @param id
	 * @param ifGrant
	 * @return   
	 * @return ReturnT<String>  
	 * @throws
	 * @author JornTang
	 * @email 957707261@qq.com
	 * @date 2017年8月28日
	 */
	@RequestMapping("/updateRegistGrant.do")
	@ResponseBody
	public RespMsg updateRegistGrant(JobRegistry registry){
		RespMsg msg= RespMsgUtil.returnMsg(RespEnum.SUCCESS); 
		try {
			this.jobService.updateRegistGrant(registry);
		} catch (Exception e) {
			msg= RespMsgUtil.returnMsg(RespEnum.ERROR); 
			logger.error("生成密钥跟令牌异常",e);
		}
		return msg;
	}
	/**
	 * @Description: 生成密钥跟令牌
	 * @return   
	 * @return ReturnT<String>  
	 * @throws
	 * @author JornTang
	 * @email 957707261@qq.com
	 * @date 2017年8月28日
	 */
	@RequestMapping(value="/addSecretKey",produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ReturnT<String> addSecretKey( int id){
		ReturnT<String> rt= ReturnT.SUCCESS; 
		try {
			jobService.addSecretKey(id);
		} catch (Exception e) {
			rt.setCode(ReturnT.FAIL_CODE);
			rt.setMsg("生成密钥跟令牌异常");
			logger.error("生成密钥跟令牌异常",e);
		}
		return rt;
	}
	/**
	 * @Description: 添加任务
	 * @param jobInfo
	 * @return   
	 * @return ReturnT<String> 以JSON格式返回 
	 * @throws
	 * @author JornTang
	 * @email 957707261@qq.com
	 * @date 2017年8月23日
	 */
	@RequestMapping(value="/add")
	@ResponseBody
	public RespMsg add(JobInfo jobInfo) {
		return jobService.add(jobInfo);
	}
}
