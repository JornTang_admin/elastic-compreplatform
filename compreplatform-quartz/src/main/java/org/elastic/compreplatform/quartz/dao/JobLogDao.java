package org.elastic.compreplatform.quartz.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.elastic.compreplatform.quartz.model.JobLog;

import com.github.pagehelper.Page;

/**
 * ClassName: JobLogDao 
 * @Description: 
 * @author JornTang
 * @email 957707261@qq.com
 * @date 2017年8月17日
 */
public interface JobLogDao {
	
	public Page<JobLog> pageList(@Param("pageNum") Integer offset,
									@Param("pageSize") Integer pagesize,
									@Param("jobGroup") Integer jobGroup,
									@Param("jobId") Integer jobId,
									@Param("triggerTimeStart") Date triggerTimeStart,
									@Param("triggerTimeEnd") Date triggerTimeEnd,
									@Param("logStatus") Integer logStatus,
									@Param("sortColumns")String sortColumns);
	
	public JobLog load(@Param("id") int id);

	public int save(JobLog jobLog);

	public int updateTriggerInfo(JobLog jobLog);

	public int updateHandleInfo(JobLog jobLog);
	
	public int delete(@Param("jobId") int jobId);

	public int triggerCountByHandleCode(@Param("handleCode") int handleCode);

	public List<Map<String, Object>> triggerCountByDay(@Param("from") Date from,
													   @Param("to") Date to,
													   @Param("handleCode") int handleCode);

	public int clearLog(@Param("jobGroup") int jobGroup,
						@Param("jobId") int jobId,
						@Param("clearBeforeTime") Date clearBeforeTime,
						@Param("clearBeforeNum") int clearBeforeNum);

}
