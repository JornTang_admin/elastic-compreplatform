package org.elastic.compreplatform.quartz.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.elastic.compreplatform.quartz.model.JobInfo;

import com.github.pagehelper.Page;


/**
 * ClassName: JobInfoDao 
 * @Description: 
 * @author JornTang
 * @email 957707261@qq.com
 * @date 2017年8月17日
 */
public interface JobInfoDao {

	public List<JobInfo> pageList(@Param("offset") int offset, @Param("pagesize") int pagesize, @Param("jobGroup") int jobGroup, @Param("executorHandler") String executorHandler);
	public int pageListCount(@Param("offset") int offset, @Param("pagesize") int pagesize, @Param("jobGroup") int jobGroup, @Param("executorHandler") String executorHandler);
	
	public int save(JobInfo info);

	public JobInfo loadById(@Param("id") int id);
	
	public int update(JobInfo item);
	
	public int delete(@Param("id") int id);

	public List<JobInfo> getJobsByGroup(@Param("jobGroup") int jobGroup);

	public int findAllCount();
	/**
	 * @Description: 获取自增长id值
	 * @return   
	 * @return int  
	 * @throws
	 * @author JornTang
	 * @email 957707261@qq.com
	 * @date 2017年8月25日
	 */
	public int getMaxId();
	/**
	 * @Description: 修改自增长ID值
	 * @param maxId   
	 * @return void  
	 * @throws
	 * @author JornTang
	 * @email 957707261@qq.com
	 * @date 2017年8月25日
	 */
	public void updateMaxId(int maxId);
	/**
	 * @Description: 根据ID获取
	 * @param id
	 * @return   
	 * @return boolean  
	 * @throws
	 * @author JornTang
	 * @email 957707261@qq.com
	 * @date 2017年12月7日
	 */
	public int findById(int id);
	/**
	 * @Description: 任务列表分页查询
	 * @param jobInfo
	 * @param offset
	 * @param limit
	 * @param sortColumns
	 * @return   
	 * @return Page<JobGroup>  
	 * @throws
	 * @author JornTang
	 * @date 2018年2月8日
	 */
	public Page<JobInfo> pageJobInfoList(@Param("jobInfo")JobInfo jobInfo, @Param("pageNum")int offset,
			@Param("pageSize")int limit, @Param("sortColumns")String sortColumns);

}
